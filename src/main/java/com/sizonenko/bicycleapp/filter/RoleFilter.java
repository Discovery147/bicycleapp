package com.sizonenko.bicycleapp.filter;

import com.sizonenko.bicycleapp.command.Command;
import com.sizonenko.bicycleapp.command.CommandMap;
import com.sizonenko.bicycleapp.command.PageConstant;
import com.sizonenko.bicycleapp.entity.Member;
import com.sizonenko.bicycleapp.validator.RoleValidator;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;

@WebFilter("/*")
public class RoleFilter implements Filter {

    private static class RoleFilterWrapper extends HttpServletRequestWrapper {

        private  RoleFilterWrapper(ServletRequest request) {
            super((HttpServletRequest)request);
        }
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        RoleFilterWrapper wrapper = new RoleFilterWrapper(req);
        String command = wrapper.getParameter("command");
        if(command == null){
            chain.doFilter(req, resp);
        }else{
            Member member = (Member) wrapper.getSession().getAttribute("member");
            boolean status = RoleValidator.checkRole(command, member);
            if (status) {
                chain.doFilter(wrapper, resp);
            }
            else{
                req.setAttribute("errorStatus", "Access is denied");
                req.setAttribute("errorInfo", "You can not access this operation");
                RequestDispatcher dispatcher = req.getRequestDispatcher(PageConstant.PATH_ERROR);
                dispatcher.forward(req, resp);
            }
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
