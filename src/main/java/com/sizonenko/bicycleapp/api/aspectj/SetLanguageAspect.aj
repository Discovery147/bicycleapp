package com.sizonenko.bicycleapp.api.aspectj;

import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.command.SetLanguageCommand;

/**
 * The test aspect SetLanguageAspect.
 */
public aspect SetLanguageAspect {

    pointcut runMethod(): call(String SessionRequestContent.getRequestParameter(String)) && within (SetLanguageCommand);

    /**
     * Before getRequestParameter function in SetLanguageCommand class
     */
    before(): runMethod() {

    }

    /**
     * After getRequestParameter function in SetLanguageCommand class
     */
    after(): runMethod() {

    }
}
