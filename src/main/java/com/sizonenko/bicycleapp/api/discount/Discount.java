package com.sizonenko.bicycleapp.api.discount;

import com.sizonenko.bicycleapp.entity.Member;

/**
 * The type Discount serves to increase the size of the discount after a successful bicycle reservation.
 */
public class Discount {

    /**
     * Maximum discount amount.
     */
    private static final byte MAX = 40;
    /**
     * The increment value for each reservation.
     */
    public static final byte INCREMENT = 5;

    /**
     * Checks if the limit on the value of the discount is exceeded.
     *
     * @param member the client who makes a reservation
     * @return the boolean if the discount for this customer has been changed
     */
    public static boolean checkDiscount(Member member) {
        if (member.getDiscount() < MAX) {
            member.setDiscount((byte) (member.getDiscount() + INCREMENT));
            return true;
        }
        return false;
    }
}
