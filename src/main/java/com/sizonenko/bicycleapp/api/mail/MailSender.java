package com.sizonenko.bicycleapp.api.mail;

import com.sizonenko.bicycleapp.entity.Confirm;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * The type Mail sender sends message to confirm the e-mail to the specified mail.
 */
public class MailSender {

    private static final String PATH_MAIL_PROPERTIES = "/mail_properties/mail";

    /**
     * Sends message to confirm the e-mail to the specified mail.
     *
     * @param confirm the class that contains the login and unique code of a new user
     * @param email the e-mail to which you want to send a request for confirmation of mail
     * @throws IOException the io exception can occur if the sender's mail data is incorrect or if it is not present.
     * @throws MessagingException the messaging exception can occur if the recipient's email is not valid.
     */
    public void send(Confirm confirm, String email) throws IOException, MessagingException {
        InputStream input = getClass().getClassLoader().getResourceAsStream(PATH_MAIL_PROPERTIES + ".properties");
        Properties props = new Properties();
        props.load(input);
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(props.getProperty("mail.user.name"), props.getProperty("mail.user.password"));
                    }
                });
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(props.getProperty("mail.user.from")));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
        message.setSubject("Activate Account");
        String msg = "Your Login: <h3>" + confirm.getLogin() + "</h3><br>" +
                "<form action=\"http://localhost:8080/InvokerServlet?\" method=\"GET\">" +
                "  <input type=\"hidden\" name=\"command\" value=\"confirm\" /> " +
                "  <input type=\"hidden\" name=\"login\" value=\"" + confirm.getLogin() + "\" /> " +
                "  <input type=\"hidden\" name=\"code\" value=\"" + confirm.getCode() + "\" /> " +
                "  <input type=\"submit\" value=\"Activate\" /> " +
                "</form>";
        message.setContent(msg, "text/html; charset=utf-8");
        Transport.send(message);
    }
}
