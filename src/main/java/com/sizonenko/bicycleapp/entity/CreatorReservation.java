package com.sizonenko.bicycleapp.entity;

/**
 * The type Creator reservation.
 */
public class CreatorReservation implements EntityCreator {
    @Override
    public Reservation create() {
        Reservation reservation = new Reservation();
        reservation.setMember(new CreatorMember().create());
        reservation.setBicycle(new CreatorBicycle().create());
        return reservation;
    }
}
