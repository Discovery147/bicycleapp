package com.sizonenko.bicycleapp.entity;

/**
 * The type Creator role.
 */
public class CreatorRole implements EntityCreator {
    @Override
    public Role create() {
        return new Role();
    }
}
