package com.sizonenko.bicycleapp.entity;

/**
 * The type Creator doctype.
 */
public class CreatorDoctype implements EntityCreator {
    @Override
    public Doctype create() {
        return new Doctype();
    }
}
