package com.sizonenko.bicycleapp.entity;

/**
 * The type Creator member.
 */
public class CreatorMember implements EntityCreator {
    @Override
    public Member create() {
        Member member = new Member();
        member.setRole(new CreatorRole().create());
        return member;
    }
}
