package com.sizonenko.bicycleapp.entity;

import java.util.Objects;

/**
 * The type Confirm.
 */
public class Confirm extends Entity {
    private String login;
    private String code;

    /**
     * Instantiates a new Confirm.
     */
    Confirm() {
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code
     */
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Confirm confirm = (Confirm) o;
        return Objects.equals(login, confirm.login) &&
                Objects.equals(code, confirm.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, code);
    }
}
