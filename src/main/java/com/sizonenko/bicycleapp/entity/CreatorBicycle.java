package com.sizonenko.bicycleapp.entity;

/**
 * The type Creator bicycle.
 */
public class CreatorBicycle implements EntityCreator {

    @Override
    public Bicycle create() {
        Bicycle bicycle = new Bicycle();
        bicycle.setPlace(new CreatorPlace().create());
        return bicycle;
    }
}
