package com.sizonenko.bicycleapp.entity;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * The type Member.
 */
public class Member extends Entity {

    private long memberId;
    private Role role;
    private boolean blocked;
    private String firstname;
    private String lastname;
    private String email;
    private String phone;
    private String login;
    private String password;
    private byte discount;
    private BigDecimal amount;
    private String image;

    /**
     * Instantiates a new Member.
     */
    Member() {
    }

    /**
     * Gets member id.
     *
     * @return the member id
     */
    public long getMemberId() {
        return memberId;
    }

    /**
     * Sets member id.
     *
     * @param memberId the member id
     */
    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Is blocked boolean.
     *
     * @return the boolean
     */
    public boolean isBlocked() {
        return blocked;
    }

    /**
     * Sets blocked.
     *
     * @param blocked the blocked
     */
    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    /**
     * Gets firstname.
     *
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Sets firstname.
     *
     * @param firstname the firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Gets lastname.
     *
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Sets lastname.
     *
     * @param lastname the lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets phone.
     *
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets phone.
     *
     * @param phone the phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets discount.
     *
     * @return the discount
     */
    public byte getDiscount() {
        return discount;
    }

    /**
     * Sets discount.
     *
     * @param discount the discount
     */
    public void setDiscount(byte discount) {
        this.discount = discount;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets amount.
     *
     * @param amount the amount
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return memberId == member.memberId &&
                blocked == member.blocked &&
                discount == member.discount &&
                Objects.equals(role, member.role) &&
                Objects.equals(firstname, member.firstname) &&
                Objects.equals(lastname, member.lastname) &&
                Objects.equals(email, member.email) &&
                Objects.equals(phone, member.phone) &&
                Objects.equals(login, member.login) &&
                Objects.equals(password, member.password) &&
                Objects.equals(amount, member.amount) &&
                Objects.equals(image, member.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberId, role, blocked, firstname, lastname, email, phone, login, password, discount, amount, image);
    }

    @Override
    public String toString() {
        return new com.google.gson.Gson().toJson(this);
    }
}

