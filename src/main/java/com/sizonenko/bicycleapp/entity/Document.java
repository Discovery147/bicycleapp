package com.sizonenko.bicycleapp.entity;

import java.util.Objects;

/**
 * The type Document.
 */
public class Document extends Entity {
    private long documentId;
    private String number;
    private String other;
    private Doctype doctype;

    /**
     * Instantiates a new Document.
     */
    Document() {
    }

    /**
     * Gets document id.
     *
     * @return the document id
     */
    public long getDocumentId() {
        return documentId;
    }

    /**
     * Sets document id.
     *
     * @param documentId the document id
     */
    public void setDocumentId(long documentId) {
        this.documentId = documentId;
    }

    /**
     * Gets number.
     *
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets number.
     *
     * @param number the number
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * Gets other.
     *
     * @return the other
     */
    public String getOther() {
        return other;
    }

    /**
     * Sets other.
     *
     * @param other the other
     */
    public void setOther(String other) {
        this.other = other;
    }

    /**
     * Gets doctype.
     *
     * @return the doctype
     */
    public Doctype getDoctype() {
        return doctype;
    }

    /**
     * Sets doctype.
     *
     * @param doctype the doctype
     */
    public void setDoctype(Doctype doctype) {
        this.doctype = doctype;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return documentId == document.documentId &&
                Objects.equals(number, document.number) &&
                Objects.equals(other, document.other) &&
                Objects.equals(doctype, document.doctype);
    }

    @Override
    public int hashCode() {
        return Objects.hash(documentId, number, other, doctype);
    }
}
