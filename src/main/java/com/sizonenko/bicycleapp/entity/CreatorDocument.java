package com.sizonenko.bicycleapp.entity;

/**
 * The type Creator document.
 */
public class CreatorDocument implements EntityCreator {
    @Override
    public Document create() {
        Document document = new Document();
        document.setDoctype(new CreatorDoctype().create());
        return document;
    }
}
