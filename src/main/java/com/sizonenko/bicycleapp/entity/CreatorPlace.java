package com.sizonenko.bicycleapp.entity;

/**
 * The type Creator place.
 */
public class CreatorPlace implements EntityCreator {
    @Override
    public Place create() {
        Place place = new Place();
        place.setMember(new CreatorMember().create());
        return place;
    }
}
