package com.sizonenko.bicycleapp.entity;

/**
 * The interface Entity creator.
 */
public interface EntityCreator {
    /**
     * Create entity.
     *
     * @return the entity
     */
    Entity create();
}
