package com.sizonenko.bicycleapp.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * The type Reservation.
 */
public class Reservation extends Entity {
    private long reservationId;
    private Member member;
    private Bicycle bicycle;
    private Timestamp startTime;
    private Timestamp endTime;
    private BigDecimal amount;

    /**
     * Instantiates a new Reservation.
     */
    Reservation() {
    }

    /**
     * Gets reservation id.
     *
     * @return the reservation id
     */
    public long getReservationId() {
        return reservationId;
    }

    /**
     * Sets reservation id.
     *
     * @param reservationId the reservation id
     */
    public void setReservationId(long reservationId) {
        this.reservationId = reservationId;
    }

    /**
     * Gets member.
     *
     * @return the member
     */
    public Member getMember() {
        return member;
    }

    /**
     * Sets member.
     *
     * @param member the member
     */
    public void setMember(Member member) {
        this.member = member;
    }

    /**
     * Gets bicycle.
     *
     * @return the bicycle
     */
    public Bicycle getBicycle() {
        return bicycle;
    }

    /**
     * Sets bicycle.
     *
     * @param bicycle the bicycle
     */
    public void setBicycle(Bicycle bicycle) {
        this.bicycle = bicycle;
    }

    /**
     * Gets start time.
     *
     * @return the start time
     */
    public Timestamp getStartTime() {
        return startTime;
    }

    /**
     * Sets start time.
     *
     * @param startTime the start time
     */
    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    /**
     * Gets end time.
     *
     * @return the end time
     */
    public Timestamp getEndTime() {
        return endTime;
    }

    /**
     * Sets end time.
     *
     * @param endTime the end time
     */
    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets amount.
     *
     * @param amount the amount
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return reservationId == that.reservationId &&
                amount == that.amount &&
                Objects.equals(member, that.member) &&
                Objects.equals(bicycle, that.bicycle) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reservationId, member, bicycle, startTime, endTime, amount);
    }

    @Override
    public String toString() {
        return new com.google.gson.Gson().toJson(this);
    }
}
