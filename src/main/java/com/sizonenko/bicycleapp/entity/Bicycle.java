package com.sizonenko.bicycleapp.entity;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * The type Bicycle.
 */
public class Bicycle extends Entity {
    private long bicycleId;
    private String serialNumber;
    private String maker;
    private String model;
    private String color;
    private String size;
    private String description;
    private BigDecimal price;
    private Place place;
    private String image;

    /**
     * Instantiates a new Bicycle.
     */
    Bicycle() {
    }

    /**
     * Gets bicycle id.
     *
     * @return the bicycle id
     */
    public long getBicycleId() {
        return bicycleId;
    }

    /**
     * Sets bicycle id.
     *
     * @param bicycleId the bicycle id
     */
    public void setBicycleId(long bicycleId) {
        this.bicycleId = bicycleId;
    }

    /**
     * Gets serial number.
     *
     * @return the serial number
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets serial number.
     *
     * @param serialNumber the serial number
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * Gets maker.
     *
     * @return the maker
     */
    public String getMaker() {
        return maker;
    }

    /**
     * Sets maker.
     *
     * @param maker the maker
     */
    public void setMaker(String maker) {
        this.maker = maker;
    }

    /**
     * Gets model.
     *
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets model.
     *
     * @param model the model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Gets color.
     *
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets color.
     *
     * @param color the color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Gets size.
     *
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * Sets size.
     *
     * @param size the size
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Gets place.
     *
     * @return the place
     */
    public Place getPlace() {
        return place;
    }

    /**
     * Sets place.
     *
     * @param place the place
     */
    public void setPlace(Place place) {
        this.place = place;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bicycle bicycle = (Bicycle) o;
        return bicycleId == bicycle.bicycleId &&
                Objects.equals(serialNumber, bicycle.serialNumber) &&
                Objects.equals(maker, bicycle.maker) &&
                Objects.equals(model, bicycle.model) &&
                Objects.equals(color, bicycle.color) &&
                Objects.equals(size, bicycle.size) &&
                Objects.equals(description, bicycle.description) &&
                Objects.equals(price, bicycle.price) &&
                Objects.equals(place, bicycle.place) &&
                Objects.equals(image, bicycle.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bicycleId, serialNumber, maker, model, color, size, description, price, place, image);
    }

    @Override
    public String toString() {
        return new com.google.gson.Gson().toJson(this);
    }
}

