package com.sizonenko.bicycleapp.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * The type Transaction.
 */
public class Transaction extends Entity {
    private long transactionId;
    private Member member;
    private Bicycle bicycle;
    private Timestamp startTime;
    private Timestamp endTime;
    private float period;
    private BigDecimal expectedAmount;
    private BigDecimal amount;
    private Document document;

    /**
     * Instantiates a new Transaction.
     */
    Transaction() {
    }

    /**
     * Gets transaction id.
     *
     * @return the transaction id
     */
    public long getTransactionId() {
        return transactionId;
    }

    /**
     * Sets transaction id.
     *
     * @param transactionId the transaction id
     */
    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * Gets member.
     *
     * @return the member
     */
    public Member getMember() {
        return member;
    }

    /**
     * Sets member.
     *
     * @param member the member
     */
    public void setMember(Member member) {
        this.member = member;
    }

    /**
     * Gets bicycle.
     *
     * @return the bicycle
     */
    public Bicycle getBicycle() {
        return bicycle;
    }

    /**
     * Sets bicycle.
     *
     * @param bicycle the bicycle
     */
    public void setBicycle(Bicycle bicycle) {
        this.bicycle = bicycle;
    }

    /**
     * Gets start time.
     *
     * @return the start time
     */
    public Timestamp getStartTime() {
        return startTime;
    }

    /**
     * Sets start time.
     *
     * @param startTime the start time
     */
    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    /**
     * Gets end time.
     *
     * @return the end time
     */
    public Timestamp getEndTime() {
        return endTime;
    }

    /**
     * Sets end time.
     *
     * @param endTime the end time
     */
    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    /**
     * Gets period.
     *
     * @return the period
     */
    public float getPeriod() {
        return period;
    }

    /**
     * Sets period.
     *
     * @param period the period
     */
    public void setPeriod(float period) {
        this.period = period;
    }

    /**
     * Gets expected amount.
     *
     * @return the expected amount
     */
    public BigDecimal getExpectedAmount() {
        return expectedAmount;
    }

    /**
     * Sets expected amount.
     *
     * @param expectedAmount the expected amount
     */
    public void setExpectedAmount(BigDecimal expectedAmount) {
        this.expectedAmount = expectedAmount;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets amount.
     *
     * @param amount the amount
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Gets document.
     *
     * @return the document
     */
    public Document getDocument() {
        return document;
    }

    /**
     * Sets document.
     *
     * @param document the document
     */
    public void setDocument(Document document) {
        this.document = document;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return transactionId == that.transactionId &&
                Float.compare(that.period, period) == 0 &&
                Objects.equals(member, that.member) &&
                Objects.equals(bicycle, that.bicycle) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime) &&
                Objects.equals(expectedAmount, that.expectedAmount) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(document, that.document);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, member, bicycle, startTime, endTime, period, expectedAmount, amount, document);
    }

    @Override
    public String toString() {
        return new com.google.gson.Gson().toJson(this);
    }
}
