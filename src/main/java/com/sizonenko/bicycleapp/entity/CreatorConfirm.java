package com.sizonenko.bicycleapp.entity;

/**
 * The type Creator confirm.
 */
public class CreatorConfirm implements EntityCreator {
    @Override
    public Confirm create() {
        return new Confirm();
    }
}
