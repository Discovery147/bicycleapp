package com.sizonenko.bicycleapp.entity;

import java.util.Objects;

/**
 * The type Doctype.
 */
public class Doctype extends Entity {
    private long doctypeId;
    private String name;

    /**
     * Instantiates a new Doctype.
     */
    Doctype() {
    }

    /**
     * Gets doctype id.
     *
     * @return the doctype id
     */
    public long getDoctypeId() {
        return doctypeId;
    }

    /**
     * Sets doctype id.
     *
     * @param doctypeId the doctype id
     */
    public void setDoctypeId(long doctypeId) {
        this.doctypeId = doctypeId;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Doctype doctype = (Doctype) o;
        return doctypeId == doctype.doctypeId &&
                Objects.equals(name, doctype.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(doctypeId, name);
    }
}
