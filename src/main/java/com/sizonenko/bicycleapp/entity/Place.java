package com.sizonenko.bicycleapp.entity;

import java.util.Objects;

/**
 * The type Place.
 */
public class Place extends Entity {
    private long placeId;
    private String name;
    private String address;
    private String image;
    private Member member;

    /**
     * Instantiates a new Place.
     */
    Place() {
    }

    /**
     * Gets place id.
     *
     * @return the place id
     */
    public long getPlaceId() {
        return placeId;
    }

    /**
     * Sets place id.
     *
     * @param placeId the place id
     */
    public void setPlaceId(long placeId) {
        this.placeId = placeId;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Gets member.
     *
     * @return the member
     */
    public Member getMember() {
        return member;
    }

    /**
     * Sets member.
     *
     * @param member the member
     */
    public void setMember(Member member) {
        this.member = member;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Place place = (Place) o;
        return placeId == place.placeId &&
                Objects.equals(name, place.name) &&
                Objects.equals(address, place.address) &&
                Objects.equals(image, place.image) &&
                Objects.equals(member, place.member);
    }

    @Override
    public int hashCode() {
        return Objects.hash(placeId, name, address, image, member);
    }

    @Override
    public String toString() {
        return new com.google.gson.Gson().toJson(this);
    }
}
