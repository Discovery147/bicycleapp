package com.sizonenko.bicycleapp.entity;

/**
 * The type Creator transaction.
 */
public class CreatorTransaction implements EntityCreator {
    @Override
    public Transaction create() {
        Transaction transaction = new Transaction();
        transaction.setMember(new CreatorMember().create());
        transaction.setBicycle(new CreatorBicycle().create());
        transaction.setDocument(new CreatorDocument().create());
        return transaction;
    }
}
