package com.sizonenko.bicycleapp.receiver;

import com.sizonenko.bicycleapp.dao.AbstractDAO;
import com.sizonenko.bicycleapp.dao.DAOFactory;
import com.sizonenko.bicycleapp.dao.Table;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.exception.ReceiverException;

import java.util.List;

/**
 * The type Doctype receiver.
 */
public class DoctypeReceiver {

    private AbstractDAO dao = DAOFactory.getDAO(Table.DOCTYPE);

    /**
     * Find all list.
     *
     * @return the list
     * @throws ReceiverException the receiver exception
     */
    public List findAll() throws ReceiverException {
        try {
            return dao.findAll();
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }
}
