package com.sizonenko.bicycleapp.receiver;

import com.sizonenko.bicycleapp.dao.AbstractDAO;
import com.sizonenko.bicycleapp.dao.DAOFactory;
import com.sizonenko.bicycleapp.dao.Table;
import com.sizonenko.bicycleapp.entity.Place;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.exception.ReceiverException;

import java.util.List;

/**
 * The type Place receiver.
 */
public class PlaceReceiver {
    private AbstractDAO dao = DAOFactory.getDAO(Table.PLACE);

    /**
     * Find all list.
     *
     * @return the list
     * @throws ReceiverException the receiver exception
     */
    public List findAll() throws ReceiverException {
        try {
            return dao.findAll();
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Find by id place.
     *
     * @param id the id
     * @return the place
     * @throws ReceiverException the receiver exception
     */
    public Place findById(Long id) throws ReceiverException {
        try {
            return (Place) dao.findEntityById(id);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Update boolean.
     *
     * @param place the place
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean update(Place place) throws ReceiverException {
        try {
            return dao.update(place);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Create boolean.
     *
     * @param place the place
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean create(Place place) throws ReceiverException {
        try {
            return dao.create(place);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }
}
