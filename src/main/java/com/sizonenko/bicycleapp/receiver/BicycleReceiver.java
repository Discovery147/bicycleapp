package com.sizonenko.bicycleapp.receiver;

import com.sizonenko.bicycleapp.dao.*;
import com.sizonenko.bicycleapp.entity.Bicycle;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.parser.BicycleParser;

import java.util.*;

/**
 * The type Bicycle receiver.
 */
public class BicycleReceiver {
    private AbstractDAO dao = DAOFactory.getDAO(Table.BICYCLE);
    private BicycleParser parser = new BicycleParser();

    /**
     * Fill main page map.
     *
     * @return the map
     * @throws ReceiverException the receiver exception
     */
    public Map fillMainPage() throws ReceiverException {
        try {
            List bicycles = dao.findAll();
            return parser.transformListToMap(bicycles);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Fill reservation page map.
     *
     * @return the map
     * @throws ReceiverException the receiver exception
     */
    public Map fillReservationPage() throws ReceiverException {
        try {
            List bicycles = ((AbstractBicycleDAO) (dao)).findAllToReservation();
            return parser.transformListToMap(bicycles);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Find all by place list.
     *
     * @param memberId the member id
     * @return the list
     * @throws ReceiverException the receiver exception
     */
    public List findAllByPlace(long memberId) throws ReceiverException {
        try {
            return ((AbstractBicycleDAO) (dao)).findAllByPlace(memberId);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Is bicycle availabe by id boolean.
     *
     * @param bicycleId the bicycle id
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean isBicycleAvailabeById(Long bicycleId) throws ReceiverException {
        try {
            return ((AbstractBicycleDAO) (dao)).isBicycleAvailableById(bicycleId);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Find all list.
     *
     * @return the list
     * @throws ReceiverException the receiver exception
     */
    public List<Bicycle> findAll() throws ReceiverException {
        try {
            return dao.findAll();
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Delete by id.
     *
     * @param id the id
     * @throws ReceiverException the receiver exception
     */
    public void deleteById(long id) throws ReceiverException {
        try {
            dao.delete(id);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Find by id bicycle.
     *
     * @param id the id
     * @return the bicycle
     * @throws ReceiverException the receiver exception
     */
    public Bicycle findById(Long id) throws ReceiverException {
        try {
            return (Bicycle) dao.findEntityById(id);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Update boolean.
     *
     * @param bicycle the bicycle
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean update(Bicycle bicycle) throws ReceiverException {
        try {
            return dao.update(bicycle);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Create boolean.
     *
     * @param bicycle the bicycle
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean create(Bicycle bicycle) throws ReceiverException {
        try {
            return dao.create(bicycle);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }
}
