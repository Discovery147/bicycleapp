package com.sizonenko.bicycleapp.receiver;

import com.sizonenko.bicycleapp.dao.*;
import com.sizonenko.bicycleapp.entity.Role;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.exception.ReceiverException;

import java.util.*;

/**
 * The type Role receiver.
 */
public class RoleReceiver {
    private AbstractDAO dao = DAOFactory.getDAO(Table.ROLE);

    /**
     * Find all list.
     *
     * @return the list
     * @throws ReceiverException the receiver exception
     */
    public List<Role> findAll() throws ReceiverException {
        try {
            return dao.findAll();
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }
}
