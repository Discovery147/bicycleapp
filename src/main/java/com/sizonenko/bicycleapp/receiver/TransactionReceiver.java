package com.sizonenko.bicycleapp.receiver;

import com.sizonenko.bicycleapp.dao.*;
import com.sizonenko.bicycleapp.entity.Transaction;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.localization.Locale;
import com.sizonenko.bicycleapp.localization.TimestampLocale;

import java.util.List;

/**
 * The type Transaction receiver.
 */
public class TransactionReceiver {
    private AbstractDAO dao = DAOFactory.getDAO(Table.TRANSACTION);

    /**
     * Find all by place list.
     *
     * @param memberId the member id
     * @param locale   the locale
     * @return the list
     * @throws ReceiverException the receiver exception
     */
    public List findAllByPlace(long memberId, Locale locale) throws ReceiverException {
        try {
            List<Transaction> transactions = ((AbstractTransactionDAO) (dao)).findAllByPlace(memberId);
            transactions.forEach(transaction -> {
                transaction.setStartTime(new TimestampLocale(transaction.getStartTime(), locale));
                transaction.setEndTime(new TimestampLocale(transaction.getEndTime(), locale));
            });
            return transactions;
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Find not finished transaction by id transaction.
     *
     * @param id the id
     * @return the transaction
     * @throws ReceiverException the receiver exception
     */
    public Transaction findNotFinishedTransactionById(Long id) throws ReceiverException {
        try {
            return (Transaction) ((AbstractTransactionDAO) (dao)).findNotFinishedTransactionById(id);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Complete transaction boolean.
     *
     * @param transaction the transaction
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean completeTransaction(Transaction transaction) throws ReceiverException {
        try {
            return ((AbstractTransactionDAO) (dao)).completeTransaction(transaction);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Create by reservation boolean.
     *
     * @param transaction the transaction
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean createByReservation(Transaction transaction) throws ReceiverException {
        try {
            return ((AbstractTransactionDAO) (dao)).createByReservation(transaction);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Create boolean.
     *
     * @param transaction the transaction
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean create(Transaction transaction) throws ReceiverException {
        try {
            return dao.create(transaction);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }
}
