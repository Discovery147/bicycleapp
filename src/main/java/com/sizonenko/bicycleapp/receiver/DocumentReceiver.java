package com.sizonenko.bicycleapp.receiver;

import com.sizonenko.bicycleapp.dao.AbstractDAO;
import com.sizonenko.bicycleapp.dao.AbstractDocumentDAO;
import com.sizonenko.bicycleapp.dao.DAOFactory;
import com.sizonenko.bicycleapp.dao.Table;
import com.sizonenko.bicycleapp.entity.Document;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.exception.ReceiverException;

/**
 * The type Document receiver.
 */
public class DocumentReceiver {
    private AbstractDAO dao = DAOFactory.getDAO(Table.DOCUMENT);

    /**
     * Create boolean.
     *
     * @param document the document
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean create(Document document) throws ReceiverException {
        try {
            return dao.create(document);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Find id by entity long.
     *
     * @param document the document
     * @return the long
     * @throws ReceiverException the receiver exception
     */
    public Long findIdByEntity(Document document) throws ReceiverException {
        try {
            return (Long) ((AbstractDocumentDAO) dao).findIdByEntity(document);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Delete document.
     *
     * @param document the document
     * @throws ReceiverException the receiver exception
     */
    public void deleteDocument(Document document) throws ReceiverException {
        try {
            dao.delete(document);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }
}
