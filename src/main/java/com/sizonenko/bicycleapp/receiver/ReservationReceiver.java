package com.sizonenko.bicycleapp.receiver;

import com.sizonenko.bicycleapp.dao.AbstractDAO;
import com.sizonenko.bicycleapp.dao.AbstractReservationDAO;
import com.sizonenko.bicycleapp.dao.DAOFactory;
import com.sizonenko.bicycleapp.dao.Table;
import com.sizonenko.bicycleapp.entity.CreatorBicycle;
import com.sizonenko.bicycleapp.entity.CreatorReservation;
import com.sizonenko.bicycleapp.entity.Member;
import com.sizonenko.bicycleapp.entity.Reservation;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.localization.Locale;
import com.sizonenko.bicycleapp.localization.TimestampLocale;
import com.sizonenko.bicycleapp.parser.TimeStampParser;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

/**
 * The type Reservation receiver.
 */
public class ReservationReceiver {

    private AbstractReservationDAO dao = (AbstractReservationDAO)DAOFactory.getDAO(Table.RESERVATION);

    /**
     * Fill calendar page list.
     *
     * @param locale the locale
     * @return the list
     * @throws ReceiverException the receiver exception
     */
    public List fillCalendarPage(Locale locale) throws ReceiverException {
        try {
            List<Reservation> allReservations = dao.findAllToCalendar();
            allReservations.forEach(reservation -> {
                reservation.setStartTime(new TimestampLocale(reservation.getStartTime(), locale));
                reservation.setEndTime(new TimestampLocale(reservation.getEndTime(), locale));
            });
            return allReservations;
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Gets user reservations.
     *
     * @param memberId the member id
     * @param locale   the locale
     * @return the user reservations
     * @throws ReceiverException the receiver exception
     */
    public List getUserReservations(long memberId, Locale locale) throws ReceiverException {
        try {
            List<Reservation> userReservations = dao.findAllByMemberId(memberId);
            userReservations.forEach(reservation -> {
                reservation.setStartTime(new TimestampLocale(reservation.getStartTime(), locale));
                reservation.setEndTime(new TimestampLocale(reservation.getEndTime(), locale));
            });
            return userReservations;
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Create reservation boolean.
     *
     * @param bicycleId the bicycle id
     * @param startTime the start time
     * @param endTime   the end time
     * @param amount    the amount
     * @param member    the member
     * @param locale    the locale
     * @return the boolean
     * @throws ParseException    the parse exception
     * @throws ReceiverException the receiver exception
     */
    public boolean createReservation(String bicycleId, String startTime, String endTime, String amount, Member member, Locale locale) throws ParseException, ReceiverException {
        Reservation reservation = new CreatorReservation().create();
        reservation.getBicycle().setBicycleId(Long.parseLong(bicycleId));
        reservation.setAmount(new BigDecimal(amount));
        reservation.setStartTime(TimeStampParser.parseStringToDateByLocale(startTime, locale));
        reservation.setEndTime(TimeStampParser.parseStringToDateByLocale(endTime, locale));
        reservation.setMember(member);
        try {
            return dao.create(new CreatorReservation().create());
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Find all by place list.
     *
     * @param memberId the member id
     * @param locale   the locale
     * @return the list
     * @throws ReceiverException the receiver exception
     */
    public List findAllByPlace(long memberId, Locale locale) throws ReceiverException {
        try {
            List<Reservation> reservations = dao.findAllByPlace(memberId);
            reservations.forEach(reservation -> {
                reservation.setStartTime(new TimestampLocale(reservation.getStartTime(), locale));
                reservation.setEndTime(new TimestampLocale(reservation.getEndTime(), locale));
            });
            return reservations;
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Find reservation by id reservation.
     *
     * @param id the id
     * @return the reservation
     * @throws ReceiverException the receiver exception
     */
    public Reservation findReservationById(Long id) throws ReceiverException {
        try {
            return (Reservation) dao.findEntityById(id);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Delete by id.
     *
     * @param reservation the reservation
     * @throws ReceiverException the receiver exception
     */
    public void deleteById(Long reservation) throws ReceiverException {
        try {
            dao.delete(reservation);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Is bicycle can start transaction boolean.
     *
     * @param entity the entity
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean isBicycleCanStartTransaction(Reservation entity) throws ReceiverException {
        try {
            return dao.isBicycleCanStartTransaction(entity);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }
}
