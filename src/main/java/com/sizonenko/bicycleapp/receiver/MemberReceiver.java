package com.sizonenko.bicycleapp.receiver;

import com.sizonenko.bicycleapp.api.mail.MailSender;
import com.sizonenko.bicycleapp.dao.AbstractDAO;
import com.sizonenko.bicycleapp.dao.AbstractMemberDAO;
import com.sizonenko.bicycleapp.dao.DAOFactory;
import com.sizonenko.bicycleapp.dao.Table;
import com.sizonenko.bicycleapp.entity.Confirm;
import com.sizonenko.bicycleapp.entity.CreatorConfirm;
import com.sizonenko.bicycleapp.entity.Member;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import org.apache.commons.lang3.RandomStringUtils;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

/**
 * The type Member receiver.
 */
public class MemberReceiver {
    private AbstractDAO dao = DAOFactory.getDAO(Table.MEMBER);

    /**
     * Log in boolean.
     *
     * @param login    the login
     * @param password the password
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean logIn(String login, String password) throws ReceiverException {
        try {
            return ((AbstractMemberDAO) (dao)).validateMember(login, password);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Gets member.
     *
     * @param login the login
     * @return the member
     * @throws ReceiverException the receiver exception
     */
    public Member getMember(String login) throws ReceiverException {
        try {
            return ((AbstractMemberDAO) (dao)).findEntityByLogin(login);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Register boolean.
     *
     * @param member the member
     * @return the boolean
     * @throws ReceiverException  the receiver exception
     * @throws IOException        the io exception
     * @throws MessagingException the messaging exception
     */
    public boolean register(Member member) throws ReceiverException, IOException, MessagingException {
        try {
            if(dao.create(member)){
                String uniqueCode = RandomStringUtils.randomAlphanumeric(5).toUpperCase();
                Confirm confirm = new CreatorConfirm().create();
                confirm.setLogin(member.getLogin());
                confirm.setCode(uniqueCode);
                ConfirmReceiver confirmReceiver = new ConfirmReceiver();
                if (confirmReceiver.createConfirm(confirm)) {
                    // SmsSender.send(confirm, member.getPhone()); not supported
                    new MailSender().send(confirm, member.getEmail());
                }
                return true;
            }else{
                return false;
            }
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Edit profile boolean.
     *
     * @param member the member
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean editProfile(Member member) throws ReceiverException {
        try {
            return dao.update(member);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Unblock user by login boolean.
     *
     * @param login the login
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean unblockUserByLogin(String login) throws ReceiverException {
        try {
            return ((AbstractMemberDAO) (dao)).unblockUserByLogin(login);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Find all list.
     *
     * @return the list
     * @throws ReceiverException the receiver exception
     */
    public List findAll() throws ReceiverException {
        try {
            return dao.findAll();
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Find by id member.
     *
     * @param id the id
     * @return the member
     * @throws ReceiverException the receiver exception
     */
    public Member findById(Long id) throws ReceiverException {
        try {
            return (Member) dao.findEntityById(id);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Update by admin boolean.
     *
     * @param member the member
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean updateByAdmin(Member member) throws ReceiverException {
        try {
            return ((AbstractMemberDAO) (dao)).updateMemberByAdmin(member);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }

    /**
     * Increment discount boolean.
     *
     * @param memberId  the member id
     * @param increment the increment
     * @return the boolean
     * @throws ReceiverException the receiver exception
     */
    public boolean incrementDiscount(long memberId, byte increment) throws ReceiverException {
        try {
            return ((AbstractMemberDAO) (dao)).incrementDiscount(memberId, increment);
        } catch (DAOException e) {
            throw new ReceiverException(e);
        }
    }
}
