package com.sizonenko.bicycleapp.controller;

import com.sizonenko.bicycleapp.command.Command;
import com.sizonenko.bicycleapp.command.CommandMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 * The type Upload servlet handles queries that contain one additional attachment.
 * The maximum size of an attachment is 10 megabytes.
 */
@WebServlet("/UploadServlet")
@MultipartConfig(maxFileSize = 1024 * 1024 * 10)
public class UploadServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Command command = CommandMap.getInstance().get(request);
        SessionRequestContent requestContent = new SessionRequestContent();
        requestContent.insertValues(request);
        Part file = request.getPart("fileName");
        requestContent.setFile(file);
        Router router = command.execute(requestContent);
        requestContent.extractValues(request);
        router.buildResponse(request, response);
    }
}