package com.sizonenko.bicycleapp.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 * The type Error servlet catches erroneous requests and sends them to the error page.
 */
@WebServlet("/ErrorServlet")
public class ErrorServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processError(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processError(request, response);
    }

    private void processError(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer code = (Integer) request.getAttribute("javax.servlet.error.status_code");
        String errorInfo = "";
        switch (code) {
            case 404:
                errorInfo = "Page Not Found";
                break;
            case 500:
                errorInfo = (request.getAttribute("javax.servlet.forward.servlet_path").equals("/UploadServlet"))
                        ? "The uploaded file is incorrect"
                        : "Internal Server Error";
        }
        request.setAttribute("errorStatus", code);
        request.setAttribute("errorInfo", errorInfo);
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/pages/error/error.jsp");
        dispatcher.forward(request, response);
    }
}
