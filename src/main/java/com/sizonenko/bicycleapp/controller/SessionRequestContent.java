package com.sizonenko.bicycleapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * The class replaces the request object and provides methods for working with data.
 */
public class SessionRequestContent {

    /**
     * Attributes of the request.
     */
    private Map<String, Object> requestArrtibutes;
    /**
     * Parameters of the request.
     */
    private Map<String, String> requestParameters;
    /**
     * Attributes of the session.
     */
    private Map<String, Object> sessionAttributes;
    /**
     * File
     */
    private Part file;

    /**
     * Copies all the data received from the client and provides an object to work with this data.
     *
     * @param request the request
     */
    void insertValues(HttpServletRequest request) {
        requestArrtibutes = new HashMap<>();
        Enumeration<String> attributeNames = request.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String attributeName = attributeNames.nextElement();
            requestArrtibutes.put(attributeName, request.getAttribute(attributeName));
        }
        requestParameters = new HashMap<>();
        Enumeration<String> parametersNames = request.getParameterNames();
        while (parametersNames.hasMoreElements()) {
            String parameterName = parametersNames.nextElement();
            requestParameters.put(parameterName, request.getParameter(parameterName));
        }
        sessionAttributes = new HashMap<>();
        Enumeration<String> sessionAttrNames = request.getSession().getAttributeNames();
        while (sessionAttrNames.hasMoreElements()) {
            String sessionAttrName = sessionAttrNames.nextElement();
            sessionAttributes.put(sessionAttrName, request.getSession().getAttribute(sessionAttrName));
        }
    }

    /**
     * Returns the processed data to the request.
     *
     * @param request the request
     */
    void extractValues(HttpServletRequest request) {
        requestArrtibutes.forEach(request::setAttribute);
        if (sessionAttributes.containsKey("invalidate")) {
            request.getSession().invalidate();
        } else {
            sessionAttributes.forEach((k, v) -> request.getSession().setAttribute(k, v));
        }
    }

    /**
     * Gets request parameter.
     *
     * @param name the name
     * @return the request parameter
     */
    public String getRequestParameter(String name) {
        return requestParameters.get(name);
    }

    /**
     * Gets session attribute.
     *
     * @param name the name
     * @return the session attribute
     */
    public Object getSessionAttribute(String name) {
        return sessionAttributes.get(name);
    }

    /**
     * Sets request attribute.
     *
     * @param name  the name
     * @param value the value
     */
    public void setRequestAttribute(String name, Object value) {
        requestArrtibutes.put(name, value);
    }

    /**
     * Sets session attribute.
     *
     * @param name  the name
     * @param value the value
     */
    public void setSessionAttribute(String name, Object value) {
        sessionAttributes.put(name, value);
    }

    /**
     * Gets file.
     *
     * @return the file
     */
    public Part getFile() {
        return file;
    }

    /**
     * Sets file.
     *
     * @param file the file
     */
    public void setFile(Part file) {
        this.file = file;
    }
}
