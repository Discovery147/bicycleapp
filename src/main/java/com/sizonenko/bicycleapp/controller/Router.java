package com.sizonenko.bicycleapp.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The type Router defines the page and the type of transition.
 * Can not return anything if it's an Ajax request.
 */
public class Router {
    /**
     * The enum Router type defines the type of transition.
     */
    public enum RouterType {
        /**
         * Forward router type.
         */
        FORWARD,
        /**
         * Redirect router type.
         */
        REDIRECT,
        /**
         * Ajax request.
         */
        NOTHING
    }

    /**
     * Page URI
     */
    private String pagePath;
    /**
     * Type of transition
     */
    private RouterType route = RouterType.FORWARD;

    /**
     * Gets page path.
     *
     * @return the page path
     */
    public String getPagePath() {
        return pagePath;
    }

    /**
     * Sets page path.
     *
     * @param pagePath the page path
     */
    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    /**
     * Gets route.
     *
     * @return the route
     */
    public RouterType getRoute() {
        return route;
    }

    /**
     * Sets route.
     *
     * @param route the route
     */
    public void setRoute(RouterType route) {
        this.route = (route != null) ? route : RouterType.FORWARD;
    }

    /**
     * Returns the response to the client, depending on the type of transfer.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException      the io exception
     * @throws ServletException the servlet exception
     */
    void buildResponse(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        switch (route) {
            case FORWARD:
                RequestDispatcher dispatcher = request.getRequestDispatcher(pagePath);
                dispatcher.forward(request, response);
                break;
            case REDIRECT:
                response.sendRedirect(pagePath);
                break;
            case NOTHING:
                response.getWriter().write(String.valueOf(request.getAttribute("content")));
        }
    }
}
