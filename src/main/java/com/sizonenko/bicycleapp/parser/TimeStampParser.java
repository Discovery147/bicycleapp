package com.sizonenko.bicycleapp.parser;

import com.sizonenko.bicycleapp.localization.DatePattern;
import com.sizonenko.bicycleapp.localization.Locale;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * The type Time stamp parser.
 */
public class TimeStampParser {

    /**
     * Parse string to date by locale timestamp.
     *
     * @param date   the date
     * @param locale the locale
     * @return the timestamp
     * @throws ParseException the parse exception
     */
    public static Timestamp parseStringToDateByLocale(String date, Locale locale) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(DatePattern.getInstance().get(locale));
        return new Timestamp(formatter.parse(date).getTime());
    }

    /**
     * Parse date to string by locale string.
     *
     * @param date   the date
     * @param locale the locale
     * @return the string
     */
    public static String parseDateToStringByLocale(Timestamp date, Locale locale) {
        SimpleDateFormat formatter = new SimpleDateFormat(DatePattern.getInstance().get(locale));
        return formatter.format(date);
    }
}
