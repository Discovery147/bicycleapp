package com.sizonenko.bicycleapp.parser;

import javax.servlet.http.Part;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * The type File parser.
 */
public class FileParser {
    /**
     * Parser part to base 64 string string.
     *
     * @param file the file
     * @return the string
     * @throws IOException the io exception
     */
    public String parserPartToBase64String(Part file) throws IOException {
        InputStream input = file.getInputStream();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] byteImage = new byte[1000000];
        for (int length = 0; (length = input.read(byteImage)) > 0; ) output.write(byteImage, 0, length);
        return org.apache.commons.codec.binary.Base64.encodeBase64String(byteImage);
    }
}
