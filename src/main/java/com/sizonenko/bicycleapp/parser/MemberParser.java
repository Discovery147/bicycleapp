package com.sizonenko.bicycleapp.parser;

import com.sizonenko.bicycleapp.entity.CreatorMember;
import com.sizonenko.bicycleapp.entity.Member;

import javax.servlet.http.Part;
import java.io.IOException;

/**
 * The type Member parser.
 */
public class MemberParser {
    /**
     * Edit profile member.
     *
     * @param login the login
     * @param phone the phone
     * @param file  the file
     * @return the member
     * @throws IOException the io exception
     */
    public Member editProfile(String login, String phone, Part file) throws IOException {
        Member member = new CreatorMember().create();
        member.setLogin(login);
        member.setPhone(phone);
        member.setImage(new FileParser().parserPartToBase64String(file));
        return member;
    }
}
