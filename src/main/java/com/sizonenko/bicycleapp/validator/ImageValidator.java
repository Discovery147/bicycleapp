package com.sizonenko.bicycleapp.validator;

import javax.servlet.http.Part;

/**
 * The type Image validator.
 */
public class ImageValidator {

    /**
     * The maximum size of the picture is 1 megabyte.
     */
    private static final int SIZE = 1_048_576;

    /**
     * Validate the size of the image.
     *
     * @param file the image
     * @return the boolean
     */
    public boolean validateImageSize(Part file) {
        return file.getSize() <= SIZE;
    }
}
