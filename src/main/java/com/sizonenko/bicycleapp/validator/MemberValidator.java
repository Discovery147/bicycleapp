package com.sizonenko.bicycleapp.validator;

import com.sizonenko.bicycleapp.entity.Member;

import javax.servlet.http.Part;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Member validator.
 */
public class MemberValidator {

    /**
     * Only english letters and digits
     * Example of correct Login: "Petr01".
     */
    private static final String REGEX_LOGIN = "[A-Za-z0-9]+";
    /**
     * Only english letters and digits
     * Example of correct Password: "01Petr01".
     */
    private static final String REGEX_PASSWORD = "[A-Za-z0-9]+";
    /**
     * Only english or russian letters
     * Example of correct Firstname or Lastname: "Petrovich".
     */
    private static final String REGEX_NAME = "[A-Za-zА-Яа-я]+";
    /**
     * Example of correct Email: "petr@gmail.com".
     */
    private static final String REGEX_EMAIL = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
    /**
     * Only 12 digits are required
     * Example of correct Phone: "375291112233".
     */
    private static final String REGEX_PHONE = "\\d{12}";

    /**
     * Validates data at registration.
     *
     * @param member the member
     * @return the boolean
     */
    public boolean validateRegister(Member member) {
        Matcher mather = Pattern.compile(REGEX_LOGIN).matcher(member.getLogin());
        if (!mather.matches()) {
            return false;
        }
        mather = Pattern.compile(REGEX_PASSWORD).matcher(member.getPassword());
        if (!mather.matches()) {
            return false;
        }
        mather = Pattern.compile(REGEX_NAME).matcher(member.getFirstname());
        if (!mather.matches()) {
            return false;
        }
        mather = Pattern.compile(REGEX_NAME).matcher(member.getLastname());
        if (!mather.matches()) {
            return false;
        }
        mather = Pattern.compile(REGEX_EMAIL).matcher(member.getEmail());
        if (!mather.matches()) {
            return false;
        }
        mather = Pattern.compile(REGEX_PHONE).matcher(member.getPhone());
        if (!mather.matches()) {
            return false;
        }
        return true;
    }

    /**
     * Validates data when changing personal data.
     *
     * @param login the login
     * @param phone the phone
     * @param file  the file
     * @return the boolean
     */
    public boolean validateEditProfile(String login, String phone, Part file) {
        Matcher mather = Pattern.compile(REGEX_LOGIN).matcher(login);
        if (!mather.matches()) {
            return false;
        }
        mather = Pattern.compile(REGEX_PHONE).matcher(phone);
        if (!mather.matches()) {
            return false;
        }
        return new ImageValidator().validateImageSize(file);
    }
}
