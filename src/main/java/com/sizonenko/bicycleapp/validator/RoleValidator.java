package com.sizonenko.bicycleapp.validator;

import com.sizonenko.bicycleapp.command.CommandType;
import com.sizonenko.bicycleapp.entity.Member;
import com.sizonenko.bicycleapp.entity.Role;
import org.apache.commons.lang3.EnumUtils;

import java.util.stream.LongStream;

public class RoleValidator {
    public static boolean checkRole(String command, Member member){
        long role = (member == null) ? 0 : member.getRole().getRoleId();
        command = command.toUpperCase();
        CommandType type = null;
        if (EnumUtils.isValidEnum(CommandType.class, command)) {
            type = CommandType.valueOf(command);
        } else {
            type = CommandType.UNKNOWN;
        }
        return LongStream.of(type.getRoles()).anyMatch(x -> x == role);
    }
}
