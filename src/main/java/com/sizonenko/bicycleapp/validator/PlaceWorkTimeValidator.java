package com.sizonenko.bicycleapp.validator;

import org.joda.time.DateTime;

import java.sql.Timestamp;

/**
 * The type Place work time validator.
 */
public class PlaceWorkTimeValidator {

    /**
     * Closing time.
     */
    private static final int CLOSE_HOUR = 21;
    /**
     * Opening time.
     */
    private static final int OPEN_HOUR = 8;

    /**
     * Checks the time regarding the schedule of rental places
     *
     * @param endDate the end date
     * @return the boolean
     */
    public boolean validateDateForOpenClosePlace(Timestamp endDate) {
        DateTime dateTime = new DateTime(endDate.getTime());
        int hourOfDay = dateTime.getHourOfDay();
        return (hourOfDay < CLOSE_HOUR && hourOfDay > OPEN_HOUR);
    }
}
