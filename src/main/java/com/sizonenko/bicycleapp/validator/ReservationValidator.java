package com.sizonenko.bicycleapp.validator;

import com.sizonenko.bicycleapp.entity.Member;
import com.sizonenko.bicycleapp.localization.Locale;
import com.sizonenko.bicycleapp.parser.TimeStampParser;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The type Reservation validator.
 */
public class ReservationValidator {
    /**
     * Validate reservation.
     *
     * @param bicycleId the bicycle id
     * @param startTime the start time
     *                  At least 1 hour from the current time until the bicycle is received
     * @param endTime   the end time
     *                  Rental period is at least 1 hour
     * @param amount    the advance reservation amount
     *                  Should be less than or equal to the balance of the client
     * @param member    the client
     *                  Should not be Null
     *                  Should not be blocked
     * @param locale    the locale
     * @return the boolean
     */
    public boolean validateReservation(String bicycleId, String startTime, String endTime, String amount, Member member, Locale locale) {
        try {
            if (member == null || member.isBlocked()) {
                return false;
            }
            BigDecimal amountBig = new BigDecimal(amount);
            if (amountBig.compareTo(member.getAmount()) > 0) {
                return false;
            }
            Timestamp timestampStart = TimeStampParser.parseStringToDateByLocale(startTime, locale);
            Timestamp timestampEnd = TimeStampParser.parseStringToDateByLocale(endTime, locale);
            Timestamp timestampCurrent = new Timestamp(System.currentTimeMillis());
            return ((timestampEnd.getTime() - timestampStart.getTime()) >= 60 * 60 * 1000 &&
                    (timestampStart.getTime() - timestampCurrent.getTime()) >= 60 * 60 * 1000);
        } catch (Exception e) {
            return false;
        }
    }
}
