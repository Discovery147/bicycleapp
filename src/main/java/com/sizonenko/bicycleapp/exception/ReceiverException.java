package com.sizonenko.bicycleapp.exception;

/**
 * The type Receiver exception.
 */
public class ReceiverException extends Exception {
    /**
     * Instantiates a new Receiver exception.
     */
    public ReceiverException() {
        super();
    }

    /**
     * Instantiates a new Receiver exception.
     *
     * @param message the message
     */
    public ReceiverException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Receiver exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public ReceiverException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Receiver exception.
     *
     * @param cause the cause
     */
    public ReceiverException(Throwable cause) {
        super(cause);
    }
}
