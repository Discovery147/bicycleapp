package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.entity.CreatorPlace;
import com.sizonenko.bicycleapp.entity.Place;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.parser.FileParser;
import com.sizonenko.bicycleapp.receiver.PlaceReceiver;
import com.sizonenko.bicycleapp.validator.ImageValidator;
import org.apache.log4j.Level;

import javax.servlet.http.Part;
import java.io.IOException;

/**
 * The type Edit place by admin command.
 */
public class EditPlaceByAdminCommand implements Command {

    private static final String PLACE_ID = "placeId";
    private static final String MEMBER_ID = "member";
    private static final String NAME = "name";
    private static final String ADDRESS = "address";

    private PlaceReceiver receiver = new PlaceReceiver();
    private ImageValidator validator = new ImageValidator();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        Place place = new CreatorPlace().create();
        place.setPlaceId(Long.valueOf(requestContent.getRequestParameter(PLACE_ID)));
        place.getMember().setMemberId(Long.valueOf(requestContent.getRequestParameter(MEMBER_ID)));
        place.setName(requestContent.getRequestParameter(NAME));
        place.setAddress(requestContent.getRequestParameter(ADDRESS));
        place.setAddress(requestContent.getRequestParameter(ADDRESS));
        Part image = requestContent.getFile();
        if (validator.validateImageSize(image)) {
            try {
                place.setImage(new FileParser().parserPartToBase64String(image));
                receiver.update(place);
                router.setPagePath(PageConstant.PATH_ADMIN_PLACES_FILL);
            } catch (IOException | ReceiverException e) {
                LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
                router.setPagePath(PageConstant.PATH_ERROR);
                requestContent.setRequestAttribute("errorStatus", e.toString());
                requestContent.setRequestAttribute("errorInfo", e.getMessage());
            }
        } else {
            router.setPagePath(PageConstant.PATH_ADMIN_BICYCLES_FILL);
            requestContent.setRequestAttribute("exception", "exception");
        }
        return router;
    }
}
