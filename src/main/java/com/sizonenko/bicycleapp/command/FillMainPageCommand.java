package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.receiver.BicycleReceiver;
import org.apache.log4j.Level;

import java.util.Map;

/**
 * The type Fill main page command.
 */
public class FillMainPageCommand implements Command {

    private BicycleReceiver receiver = new BicycleReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        try {
            Map bicycles = receiver.fillMainPage();
            requestContent.setRequestAttribute("data", bicycles);
            router.setPagePath(PageConstant.PATH_MAIN);
        } catch (ReceiverException e) {
            LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
            router.setPagePath(PageConstant.PATH_ERROR);
            requestContent.setRequestAttribute("errorStatus", e.toString());
            requestContent.setRequestAttribute("errorInfo", e.getMessage());
        }
        return router;
    }
}
