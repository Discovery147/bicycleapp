package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.entity.*;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.receiver.*;
import org.apache.log4j.Level;

import java.util.List;

/**
 * The type Fill admin bicycles command.
 */
public class FillAdminBicyclesCommand implements Command {

    private BicycleReceiver bicycleReceiver = new BicycleReceiver();
    private PlaceReceiver placeReceiver = new PlaceReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        List bicycles;
        List places;
        try {
            bicycles = bicycleReceiver.findAll();
            places = placeReceiver.findAll();
            requestContent.setRequestAttribute("bicycles", bicycles);
            requestContent.setRequestAttribute("places", places);
            router.setPagePath(PageConstant.PATH_ADMIN_BICYCLES);
        } catch (ReceiverException e) {
            LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
            router.setPagePath(PageConstant.PATH_ERROR);
            requestContent.setRequestAttribute("errorStatus", e.toString());
            requestContent.setRequestAttribute("errorInfo", e.getMessage());
        }
        return router;
    }
}
