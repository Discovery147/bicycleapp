package com.sizonenko.bicycleapp.command;

/**
 * The enum command types and role identifiers to which this command is available.
 */
public enum CommandType {
    LOG_IN(0),
    LOG_OUT(1,2,3),
    LANGUAGE(0,1,2,3),
    REGISTER(0),
    FILL_MAIN(0,1,2,3),
    FILL_CALENDAR(0,1,2,3),
    FILL_RESERVATION(1),
    EDIT_PROFILE(1,2,3),
    CONFIRM(0,1,2,3),
    FILL_CASHIER(2),
    FIND_NOT_FINISHED_TRANSACTION(2),
    COMPLETE_TRANSACTION(2),
    FIND_RESERVATION(2),
    CREATE_TRANSACTION_BY_RESERVATION(2),
    CREATE_TRANSACTION(2),
    FILL_ADMIN_BICYCLES(3),
    DELETE_BICYCLE_BY_ID(3),
    FIND_BICYCLE_BY_ID(3),
    EDIT_BICYCLE_BY_ADMIN(3),
    CREATE_BICYCLE(3),
    FILL_ADMIN_PLACES(3),
    UNKNOWN(0,1,2,3),
    FIND_PLACE_BY_ID(1,2,3),
    EDIT_PLACE_BY_ADMIN(3),
    CREATE_PLACE(3),
    FILL_ADMIN_MEMBERS(3),
    FIND_MEMBER_BY_ID(3),
    EDIT_MEMBER_BY_ADMIN(3),
    FILL_PROFILE(1,2,3),
    RESERVATION(1);

    private long roles[];

    CommandType(long... roles) {
        this.roles = roles;
    }

    public long[] getRoles() {
        return roles;
    }
}
