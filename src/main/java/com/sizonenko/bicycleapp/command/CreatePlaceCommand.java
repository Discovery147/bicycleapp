package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.entity.CreatorPlace;
import com.sizonenko.bicycleapp.entity.Place;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.parser.FileParser;
import com.sizonenko.bicycleapp.receiver.PlaceReceiver;
import com.sizonenko.bicycleapp.validator.ImageValidator;
import org.apache.log4j.Level;
import javax.servlet.http.Part;
import java.io.IOException;

public class CreatePlaceCommand implements Command {

    private static final String NAME = "name";
    private static final String ADDRESS = "address";
    private static final String MEMBER = "member";

    private PlaceReceiver receiver = new PlaceReceiver();
    private ImageValidator validator = new ImageValidator();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        Place place = new CreatorPlace().create();
        place.setName(requestContent.getRequestParameter(NAME));
        place.setAddress(requestContent.getRequestParameter(ADDRESS));
        place.getMember().setMemberId(Long.valueOf(requestContent.getRequestParameter(MEMBER)));
        Part image = requestContent.getFile();
        if (validator.validateImageSize(image)) {
            try {
                place.setImage(new FileParser().parserPartToBase64String(image));
                receiver.create(place);
                router.setPagePath(PageConstant.PATH_ADMIN_PLACES_FILL);
            } catch (IOException | ReceiverException e) {
                LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
                router.setPagePath(PageConstant.PATH_ERROR);
                requestContent.setRequestAttribute("errorStatus", e.toString());
                requestContent.setRequestAttribute("errorInfo", e.getMessage());
            }
        } else {
            router.setPagePath(PageConstant.PATH_ADMIN_BICYCLES_FILL);
            requestContent.setRequestAttribute("exception", "exception");
        }
        return router;
    }
}
