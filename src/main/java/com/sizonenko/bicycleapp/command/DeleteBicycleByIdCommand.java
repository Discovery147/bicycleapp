package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.receiver.BicycleReceiver;
import org.apache.log4j.Level;

/**
 * The type Delete bicycle by id command.
 */
public class DeleteBicycleByIdCommand implements Command {

    private static final String BICYCLE_ID = "id";
    private BicycleReceiver bicycleReceiver = new BicycleReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        try {
            Long id = Long.valueOf(requestContent.getRequestParameter(BICYCLE_ID));
            bicycleReceiver.deleteById(id);
            router.setRoute(Router.RouterType.NOTHING);
        } catch (ReceiverException e) {
            LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
            router.setPagePath(PageConstant.PATH_ERROR);
            requestContent.setRequestAttribute("errorStatus", e.toString());
            requestContent.setRequestAttribute("errorInfo", e.getMessage());
        }
        return router;
    }
}
