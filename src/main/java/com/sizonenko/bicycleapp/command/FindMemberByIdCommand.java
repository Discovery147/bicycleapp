package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.entity.Member;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.receiver.MemberReceiver;
import org.apache.log4j.Level;

/**
 * The type Find member by id command.
 */
public class FindMemberByIdCommand implements Command {

    private static final String ID = "id";
    private MemberReceiver receiver = new MemberReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        Member member = null;
        try {
            Long id = Long.valueOf(requestContent.getRequestParameter(ID));
            member = receiver.findById(id);
            router.setRoute(Router.RouterType.NOTHING);
        } catch (ReceiverException e) {
            LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
            router.setPagePath(PageConstant.PATH_ERROR);
            requestContent.setRequestAttribute("errorStatus", e);
            requestContent.setRequestAttribute("errorInfo", e.getMessage());
        }
        requestContent.setRequestAttribute("content", member.toString());
        return router;
    }
}
