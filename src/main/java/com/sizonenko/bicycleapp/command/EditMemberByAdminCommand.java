package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.entity.CreatorMember;
import com.sizonenko.bicycleapp.entity.Member;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.receiver.MemberReceiver;
import org.apache.log4j.Level;

/**
 * The type Edit member by admin command.
 */
public class EditMemberByAdminCommand implements Command {

    private static final String MEMBER_ID = "memberId";
    private static final String DISCOUNT = "discount";
    private static final String ROLE_ID = "role";
    private static final String BLOCKED = "blocked";

    private MemberReceiver receiver = new MemberReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        Member member = new CreatorMember().create();
        member.setMemberId(Long.valueOf(requestContent.getRequestParameter(MEMBER_ID)));
        member.setDiscount(Byte.parseByte(requestContent.getRequestParameter(DISCOUNT)));
        member.getRole().setRoleId(Long.valueOf(requestContent.getRequestParameter(ROLE_ID)));
        member.setBlocked(requestContent.getRequestParameter(BLOCKED).equals("1"));
        try {
            receiver.updateByAdmin(member);
            router.setPagePath(PageConstant.PATH_ADMIN_MEMBERS_FILL);
        } catch (ReceiverException e) {
            LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
            router.setPagePath(PageConstant.PATH_ERROR);
            requestContent.setRequestAttribute("errorStatus", e.toString());
            requestContent.setRequestAttribute("errorInfo", e.getMessage());
        }
        return router;
    }
}
