package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;

/**
 * The type Fill profile command.
 */
public class FillProfileCommand implements Command {

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        router.setPagePath(PageConstant.PATH_PROFILE);
        return router;
    }
}
