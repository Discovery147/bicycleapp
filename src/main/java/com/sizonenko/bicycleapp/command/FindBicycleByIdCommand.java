package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.entity.Bicycle;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.receiver.BicycleReceiver;
import org.apache.log4j.Level;

/**
 * The type Find bicycle by id command.
 */
public class FindBicycleByIdCommand implements Command {

    private static final String ID = "id";
    private BicycleReceiver bicycleReceiver = new BicycleReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        Bicycle bicycle = null;
        try {
            Long id = Long.valueOf(requestContent.getRequestParameter(ID));
            bicycle = bicycleReceiver.findById(id);
            router.setRoute(Router.RouterType.NOTHING);
        } catch (ReceiverException e) {
            LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
            router.setPagePath(PageConstant.PATH_ERROR);
            requestContent.setRequestAttribute("errorStatus", e);
            requestContent.setRequestAttribute("errorInfo", e.getMessage());
        }
        requestContent.setRequestAttribute("content", bicycle.toString());
        return router;
    }
}
