package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.entity.Bicycle;
import com.sizonenko.bicycleapp.entity.CreatorBicycle;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.parser.FileParser;
import com.sizonenko.bicycleapp.receiver.BicycleReceiver;
import com.sizonenko.bicycleapp.validator.ImageValidator;
import org.apache.log4j.Level;
import javax.servlet.http.Part;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * The type Create bicycle command.
 */
public class CreateBicycleCommand implements Command {
    private static final String PLACE_ID = "place";
    private static final String SERIAL_NUMBER = "serial";
    private static final String MAKER = "maker";
    private static final String MODEL = "model";
    private static final String COLOR = "color";
    private static final String SIZE = "size";
    private static final String PRICE = "price";

    private BicycleReceiver receiver = new BicycleReceiver();
    private ImageValidator validator = new ImageValidator();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        Bicycle bicycle = new CreatorBicycle().create();
        bicycle.getPlace().setPlaceId(Long.valueOf(requestContent.getRequestParameter(PLACE_ID)));
        bicycle.setSerialNumber(requestContent.getRequestParameter(SERIAL_NUMBER));
        bicycle.setMaker(requestContent.getRequestParameter(MAKER));
        bicycle.setModel(requestContent.getRequestParameter(MODEL));
        bicycle.setColor(requestContent.getRequestParameter(COLOR));
        bicycle.setSize(requestContent.getRequestParameter(SIZE));
        bicycle.setPrice(new BigDecimal(requestContent.getRequestParameter(PRICE)));
        Part image = requestContent.getFile();
        if(validator.validateImageSize(image)){
            try {
                bicycle.setImage(new FileParser().parserPartToBase64String(image));
                receiver.create(bicycle);
                router.setPagePath(PageConstant.PATH_ADMIN_BICYCLES_FILL);
            } catch (IOException | ReceiverException e) {
                LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
                router.setPagePath(PageConstant.PATH_ERROR);
                requestContent.setRequestAttribute("errorStatus", e.toString());
                requestContent.setRequestAttribute("errorInfo", e.getMessage());
            }
        }else{
            router.setPagePath(PageConstant.PATH_ADMIN_BICYCLES_FILL);
            requestContent.setRequestAttribute("exception","exception");
        }
        return router;
    }
}
