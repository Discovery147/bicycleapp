package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.receiver.MemberReceiver;
import com.sizonenko.bicycleapp.receiver.RoleReceiver;
import org.apache.log4j.Level;

import java.util.List;

/**
 * The type Fill admin members command.
 */
public class FillAdminMembersCommand implements Command {

    private MemberReceiver memberReceiver = new MemberReceiver();
    private RoleReceiver roleReceiver = new RoleReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        List members;
        List roles;
        try {
            members = memberReceiver.findAll();
            roles = roleReceiver.findAll();
            requestContent.setRequestAttribute("members", members);
            requestContent.setRequestAttribute("roles", roles);
            router.setPagePath(PageConstant.PATH_ADMIN_MEMBERS);
        } catch (ReceiverException e) {
            LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
            router.setPagePath(PageConstant.PATH_ERROR);
            requestContent.setRequestAttribute("errorStatus", e.toString());
            requestContent.setRequestAttribute("errorInfo", e.getMessage());
        }
        return router;
    }
}
