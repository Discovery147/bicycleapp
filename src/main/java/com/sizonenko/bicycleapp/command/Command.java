package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;

import org.apache.log4j.Logger;

/**
 * The interface Command is a pattern of a command.
 */
public interface Command {

    /**
     * Logging object for the entire command hierarchy.
     */
    Logger LOGGER = Logger.getLogger(Command.class);

    /**
     * The method defines different functional for each child class
     *
     * @param requestContent Contains all the data received by the servlet.
     * @return An object that defines the output page and the method of transition.
     */
    Router execute(SessionRequestContent requestContent);
}
