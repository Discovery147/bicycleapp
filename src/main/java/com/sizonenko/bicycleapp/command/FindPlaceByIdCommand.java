package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.entity.Place;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.receiver.PlaceReceiver;
import org.apache.log4j.Level;

/**
 * The type Find place by id command.
 */
public class FindPlaceByIdCommand implements Command {

    private static final String ID = "id";
    private PlaceReceiver placeReceiver = new PlaceReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        Place place = null;
        try {
            Long id = Long.valueOf(requestContent.getRequestParameter(ID));
            place = placeReceiver.findById(id);
            router.setRoute(Router.RouterType.NOTHING);
        } catch (ReceiverException e) {
            LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
            router.setPagePath(PageConstant.PATH_ERROR);
            requestContent.setRequestAttribute("errorStatus", e);
            requestContent.setRequestAttribute("errorInfo", e.getMessage());
        }
        requestContent.setRequestAttribute("content", place.toString());
        return router;
    }
}
