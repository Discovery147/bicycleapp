package com.sizonenko.bicycleapp.command;

/**
 * The type Page constant.
 */
public class PageConstant {
    /**
     * The Path error.
     */
    public static final String PATH_ERROR = "WEB-INF/pages/error/error.jsp";
    /**
     * The Path index.
     */
    static final String PATH_INDEX = "index.jsp";
    /**
     * The Path main.
     */
    static final String PATH_MAIN = "WEB-INF/pages/main.jsp";
    /**
     * The Path calendar.
     */
    static final String PATH_CALENDAR = "pages/calendar.jsp";
    /**
     * The Path reservation.
     */
    static final String PATH_RESERVATION = "WEB-INF/pages/user/reservation.jsp";
    /**
     * The Path profile.
     */
    static final String PATH_PROFILE = "WEB-INF/pages/user/user.jsp";
    /**
     * The Path profile fill.
     */
    static final String PATH_PROFILE_FILL = "/InvokerServlet?command=fill_profile";
    /**
     * The Path success confirm.
     */
    static final String PATH_SUCCESS_CONFIRM = "pages/confirm.jsp";
    /**
     * The Path cashier.
     */
    static final String PATH_CASHIER = "WEB-INF/pages/cashier/cashier.jsp";
    /**
     * The Path cashier fill.
     */
    static final String PATH_CASHIER_FILL = "/InvokerServlet?command=fill_cashier";
    /**
     * The Path admin bicycles.
     */
    static final String PATH_ADMIN_BICYCLES = "WEB-INF/pages/admin/bicycles.jsp";
    /**
     * The Path admin bicycles fill.
     */
    static final String PATH_ADMIN_BICYCLES_FILL = "/InvokerServlet?command=fill_admin_bicycles";
    /**
     * The Path admin places.
     */
    static final String PATH_ADMIN_PLACES = "WEB-INF/pages/admin/places.jsp";
    /**
     * The Path admin places fill.
     */
    static final String PATH_ADMIN_PLACES_FILL = "/InvokerServlet?command=fill_admin_places";
    /**
     * The Path admin members.
     */
    static final String PATH_ADMIN_MEMBERS = "WEB-INF/pages/admin/members.jsp";
    /**
     * The Path admin members fill.
     */
    static final String PATH_ADMIN_MEMBERS_FILL = "/InvokerServlet?command=fill_admin_members";

    private PageConstant() {
    }
}
