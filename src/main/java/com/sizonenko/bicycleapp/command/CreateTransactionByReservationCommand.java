package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.entity.*;
import com.sizonenko.bicycleapp.entity.CreatorTransaction;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.localization.Locale;
import com.sizonenko.bicycleapp.parser.TimeStampParser;
import com.sizonenko.bicycleapp.receiver.BicycleReceiver;
import com.sizonenko.bicycleapp.receiver.DocumentReceiver;
import com.sizonenko.bicycleapp.receiver.ReservationReceiver;
import com.sizonenko.bicycleapp.receiver.TransactionReceiver;
import org.apache.log4j.Level;
import java.math.BigDecimal;
import java.text.ParseException;

/**
 * The type Create transaction by reservation command.
 */
public class CreateTransactionByReservationCommand implements Command {

    private static final String BICYCLE_ID = "bicycleId";
    private static final String MEMBER_ID = "memberId";
    private static final String START_TIME = "startTime";
    private static final String PERIOD = "period";
    private static final String EXPECTED_AMOUNT = "expectedAmount";
    private static final String DOCTYPE_ID = "docTypeId";
    private static final String DOCUMENT_NUMBER = "documentNumber";
    private static final String DOCUMENT_OTHER = "documentOther";
    private static final String RESERVATION_ID = "reservationId";

    private DocumentReceiver documentReceiver = new DocumentReceiver();
    private BicycleReceiver bicycleReceiver = new BicycleReceiver();
    private TransactionReceiver transactionReceiver = new TransactionReceiver();
    private ReservationReceiver reservationReceiver = new ReservationReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        Locale locale = (Locale) (requestContent.getSessionAttribute("languageAttr"));
        String content = null;
        Transaction transaction = new CreatorTransaction().create();
        transaction.getDocument().getDoctype().setDoctypeId(Long.valueOf(requestContent.getRequestParameter(DOCTYPE_ID)));
        transaction.getDocument().setNumber(requestContent.getRequestParameter(DOCUMENT_NUMBER));
        transaction.getDocument().setOther(requestContent.getRequestParameter(DOCUMENT_OTHER));
        Long bicycleId = Long.valueOf(requestContent.getRequestParameter(BICYCLE_ID));
        try {
            if (bicycleReceiver.isBicycleAvailabeById(bicycleId)) { // isAvailableBicycle
                if (documentReceiver.create(transaction.getDocument())) {
                    Long documentId = documentReceiver.findIdByEntity(transaction.getDocument());
                    transaction.getDocument().setDocumentId(documentId);
                    transaction.getBicycle().setBicycleId(Long.valueOf(requestContent.getRequestParameter(BICYCLE_ID)));
                    transaction.getMember().setMemberId(Long.valueOf(requestContent.getRequestParameter(MEMBER_ID)));
                    transaction.setStartTime(TimeStampParser.parseStringToDateByLocale(requestContent.getRequestParameter(START_TIME), locale));
                    transaction.setPeriod(Float.valueOf(requestContent.getRequestParameter(PERIOD)));
                    transaction.setExpectedAmount(new BigDecimal(requestContent.getRequestParameter(EXPECTED_AMOUNT)));
                    if (transactionReceiver.createByReservation(transaction)) { // открытие поездки
                        reservationReceiver.deleteById(Long.valueOf(requestContent.getRequestParameter(RESERVATION_ID)));    // закрытие брони
                        router.setRoute(Router.RouterType.NOTHING);
                        content = "forward";
                    } else {
                        router.setRoute(Router.RouterType.NOTHING);
                        content = "Create transaction is not success!";
                    }
                } else {
                    router.setRoute(Router.RouterType.NOTHING);
                    content = "Dublicate document!";
                }
            } else {
                router.setRoute(Router.RouterType.NOTHING);
                content = "Bicycle is not available!";
            }
        } catch (ParseException | NumberFormatException | ReceiverException e) {
            try {
                router.setRoute(Router.RouterType.NOTHING);
                documentReceiver.deleteDocument(transaction.getDocument()); // Удаляем возможно созданный документ, если транзакция не удалась
                LOGGER.log(Level.ERROR, "Exception: " + e.getCause());
                content = "Data format is unvailable";
            } catch (ReceiverException deleteDocumentException) {
                LOGGER.log(Level.ERROR, "Exception: " + deleteDocumentException.getCause());
                content = "Delete document Exception";
            }
        }
        requestContent.setRequestAttribute("content", content);
        return router;
    }
}
