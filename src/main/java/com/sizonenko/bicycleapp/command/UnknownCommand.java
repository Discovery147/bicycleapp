package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;

/**
 * The type Unknown command.
 */
public class UnknownCommand implements Command {
    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        router.setPagePath(PageConstant.PATH_ERROR);
        requestContent.setRequestAttribute("errorStatus", "Unknown command");
        requestContent.setRequestAttribute("errorInfo", "Command: " + requestContent.getRequestParameter("command"));
        return router;
    }
}
