package com.sizonenko.bicycleapp.command;

import org.apache.commons.lang3.EnumUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.EnumMap;

/**
 * The type is singleton.
 * Defines a set of commands and their corresponding methods.
 */
public class CommandMap {
    private EnumMap<CommandType, Command> map = new EnumMap<CommandType, Command>(CommandType.class) {{
        this.put(CommandType.UNKNOWN, new UnknownCommand());
        this.put(CommandType.LOG_IN, new LogInCommand());
        this.put(CommandType.LOG_OUT, new LogOutCommand());
        this.put(CommandType.LANGUAGE, new SetLanguageCommand());
        this.put(CommandType.REGISTER, new RegisterCommand());
        this.put(CommandType.FILL_MAIN, new FillMainPageCommand());
        this.put(CommandType.FILL_CALENDAR, new FillCalendarPageCommand());
        this.put(CommandType.FILL_RESERVATION, new FillReservationPageCommand());
        this.put(CommandType.RESERVATION, new ReserveCommand());
        this.put(CommandType.EDIT_PROFILE, new EditProfileCommand());
        this.put(CommandType.CONFIRM, new ConfirmCommand());
        this.put(CommandType.FILL_CASHIER, new FillCashierCommand());
        this.put(CommandType.FIND_NOT_FINISHED_TRANSACTION, new FindNotFinishedTransactionCommand());
        this.put(CommandType.FIND_RESERVATION, new FindReservationCommand());
        this.put(CommandType.FIND_BICYCLE_BY_ID, new FindBicycleByIdCommand());
        this.put(CommandType.COMPLETE_TRANSACTION, new CompleteTransactionCommand());
        this.put(CommandType.CREATE_TRANSACTION_BY_RESERVATION, new CreateTransactionByReservationCommand());
        this.put(CommandType.CREATE_TRANSACTION, new CreateTransactionCommand());
        this.put(CommandType.FILL_ADMIN_BICYCLES, new FillAdminBicyclesCommand());
        this.put(CommandType.DELETE_BICYCLE_BY_ID, new DeleteBicycleByIdCommand());
        this.put(CommandType.EDIT_BICYCLE_BY_ADMIN, new EditBicycleByAdminCommand());
        this.put(CommandType.CREATE_BICYCLE, new CreateBicycleCommand());
        this.put(CommandType.FILL_ADMIN_PLACES, new FillAdminPlacesCommand());
        this.put(CommandType.FIND_PLACE_BY_ID, new FindPlaceByIdCommand());
        this.put(CommandType.EDIT_PLACE_BY_ADMIN, new EditPlaceByAdminCommand());
        this.put(CommandType.CREATE_PLACE, new CreatePlaceCommand());
        this.put(CommandType.FILL_ADMIN_MEMBERS, new FillAdminMembersCommand());
        this.put(CommandType.FIND_MEMBER_BY_ID, new FindMemberByIdCommand());
        this.put(CommandType.EDIT_MEMBER_BY_ADMIN, new EditMemberByAdminCommand());
        this.put(CommandType.FILL_PROFILE, new FillProfileCommand());
    }};

    private final static CommandMap instance = new CommandMap();

    private CommandMap() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static CommandMap getInstance() {
        return instance;
    }

    /**
     * Get command returns the command object based on the value of the string.
     *
     * @param request contains in text form the value of the command
     * @return the specific command object
     */
    public Command get(HttpServletRequest request) {
        return initCommand(request.getParameter("command"));
    }

    /**
     * Get command.
     *
     * @param command the command
     * @return the command
     */
    public Command get(String command){
        return initCommand(command);
    }

    private Command initCommand(String command){
        command = command.toUpperCase();
        CommandType key = null;
        if (EnumUtils.isValidEnum(CommandType.class, command)) {
            key = CommandType.valueOf(command);
        } else {
            key = CommandType.UNKNOWN;
        }
        return map.get(key);
    }

}
