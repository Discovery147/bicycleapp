package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.entity.*;
import com.sizonenko.bicycleapp.entity.CreatorReservation;
import com.sizonenko.bicycleapp.entity.CreatorTransaction;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.localization.Locale;
import com.sizonenko.bicycleapp.parser.TimeStampParser;
import com.sizonenko.bicycleapp.receiver.*;
import com.sizonenko.bicycleapp.validator.PlaceWorkTimeValidator;
import org.apache.log4j.Level;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;

/**
 * The type Create transaction command.
 */
public class CreateTransactionCommand implements Command {

    private static final String BICYCLE_ID = "bicycleId";
    private static final String DOCTYPE_ID = "docTypeId";
    private static final String DOCUMENT_NUMBER = "documentNumber";
    private static final String DOCUMENT_OTHER = "documentOther";
    private static final String START_TIME = "startTime";
    private static final String PERIOD = "period";
    private static final String EXPECTED_AMOUNT = "expectedAmount";

    private BicycleReceiver bicycleReceiver = new BicycleReceiver();
    private ReservationReceiver reservationReceiver = new ReservationReceiver();
    private TransactionReceiver transactionReceiver = new TransactionReceiver();
    private DocumentReceiver documentReceiver = new DocumentReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        Locale locale = (Locale) (requestContent.getSessionAttribute("languageAttr"));
        String content = null;
        Transaction transaction = new CreatorTransaction().create();
        Long bicycleId = Long.valueOf(requestContent.getRequestParameter(BICYCLE_ID));
        try {
            if (bicycleReceiver.isBicycleAvailabeById(bicycleId)) {
                float period = Float.valueOf(requestContent.getRequestParameter(PERIOD));
                Timestamp startTime = TimeStampParser.parseStringToDateByLocale(requestContent.getRequestParameter(START_TIME), locale);
                Timestamp endTime = new Timestamp((long) (startTime.getTime() + (1000 * 60 * 60 * period)));
                Reservation reservation = new CreatorReservation().create();
                reservation.getBicycle().setBicycleId(bicycleId);
                reservation.setStartTime(startTime);
                reservation.setEndTime(endTime);
                if (new PlaceWorkTimeValidator().validateDateForOpenClosePlace(endTime) && reservationReceiver.isBicycleCanStartTransaction(reservation)) {
                    transaction.getDocument().getDoctype().setDoctypeId(Long.valueOf(requestContent.getRequestParameter(DOCTYPE_ID)));
                    transaction.getDocument().setNumber(requestContent.getRequestParameter(DOCUMENT_NUMBER));
                    transaction.getDocument().setOther(requestContent.getRequestParameter(DOCUMENT_OTHER));
                    if (documentReceiver.create(transaction.getDocument())) {
                        Long documentId = documentReceiver.findIdByEntity(transaction.getDocument());
                        transaction.getDocument().setDocumentId(documentId);
                        transaction.setStartTime(startTime);
                        transaction.setPeriod(period);
                        transaction.setExpectedAmount(new BigDecimal(requestContent.getRequestParameter(EXPECTED_AMOUNT)));
                        if (transactionReceiver.create(transaction)) {
                            router.setRoute(Router.RouterType.NOTHING);
                            content = "forward";
                        } else {
                            router.setRoute(Router.RouterType.NOTHING);
                            content = "Create transaction is not success!";
                        }
                    } else {
                        router.setRoute(Router.RouterType.NOTHING);
                        content = "Dublicate document!";
                    }
                } else {
                    router.setRoute(Router.RouterType.NOTHING);
                    content = "Bicycle is not available or incorrect dates";
                }
            }
        } catch (ParseException | NumberFormatException | ReceiverException e) {
            try {
                router.setRoute(Router.RouterType.NOTHING);
                documentReceiver.deleteDocument(transaction.getDocument()); // Удаляем возможно созданный документ, если транзакция не удалась
                LOGGER.log(Level.ERROR, "Exception: " + e.getCause());
                content = "Data format is unvailable";
            } catch (ReceiverException deleteDocumentException) {
                LOGGER.log(Level.ERROR, "Exception: " + deleteDocumentException.getCause());
                content = "Delete document Exception";
            }
        }
        requestContent.setRequestAttribute("content", content);
        return router;
    }
}
