package com.sizonenko.bicycleapp.command;

import com.sizonenko.bicycleapp.controller.Router;
import com.sizonenko.bicycleapp.controller.SessionRequestContent;
import com.sizonenko.bicycleapp.exception.ReceiverException;
import com.sizonenko.bicycleapp.receiver.MemberReceiver;
import com.sizonenko.bicycleapp.receiver.PlaceReceiver;
import org.apache.log4j.Level;

import java.util.List;

/**
 * The type Fill admin places command.
 */
public class FillAdminPlacesCommand implements Command {

    private PlaceReceiver placeReceiver = new PlaceReceiver();
    private MemberReceiver memberReceiver = new MemberReceiver();

    @Override
    public Router execute(SessionRequestContent requestContent) {
        Router router = new Router();
        List places;
        List members;
        try {
            places = placeReceiver.findAll();
            members = memberReceiver.findAll();
            requestContent.setRequestAttribute("places", places);
            requestContent.setRequestAttribute("members", members);
            router.setPagePath(PageConstant.PATH_ADMIN_PLACES);
        } catch (ReceiverException e) {
            LOGGER.log(Level.ERROR, "SQLException: " + e.getCause());
            router.setPagePath(PageConstant.PATH_ERROR);
            requestContent.setRequestAttribute("errorStatus", e.toString());
            requestContent.setRequestAttribute("errorInfo", e.getMessage());
        }
        return router;
    }
}
