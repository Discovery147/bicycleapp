package com.sizonenko.bicycleapp.tag;

import com.sizonenko.bicycleapp.localization.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * The type Blocked member tag.
 */
public class BlockedMemberTag extends TagSupport {

    private String blocked = "true";
    private Locale language = Locale.ru_RU;

    /**
     * Gets blocked.
     *
     * @return the blocked
     */
    public String getBlocked() {
        return blocked;
    }

    /**
     * Sets blocked.
     *
     * @param blocked the blocked
     */
    public void setBlocked(String blocked) {
        this.blocked = blocked;
    }

    /**
     * Gets language.
     *
     * @return the language
     */
    public Locale getLanguage() {
        return language;
    }

    /**
     * Sets language.
     *
     * @param locale the locale
     */
    public void setLanguage(Locale locale) {
        if(locale != null) {
            this.language = locale;
        }
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            String status = null;
            if(blocked.equals("true")){
                switch (language){
                    case by_BY:
                        status = "Заблякаваны";
                        break;
                    case ru_RU:
                        status = "Заблокирован";
                        break;
                    case en_US:
                        status = "Blocked";
                }
            }else {
                switch (language) {
                    case by_BY:
                        status = "Не";
                        break;
                    case ru_RU:
                        status = "Нет";
                        break;
                    case en_US:
                        status = "No";
                }
            }
            pageContext.getOut().write(status);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
