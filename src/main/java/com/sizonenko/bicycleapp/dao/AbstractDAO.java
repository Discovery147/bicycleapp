package com.sizonenko.bicycleapp.dao;

import com.sizonenko.bicycleapp.entity.Entity;
import com.sizonenko.bicycleapp.exception.DAOException;

import java.util.List;

/**
 * The interface Abstract dao.
 *
 * @param <K> the type parameter
 * @param <T> the type parameter
 */
public interface AbstractDAO<K, T extends Entity> {
    /**
     * Find all list.
     *
     * @return the list
     * @throws DAOException the dao exception
     */
    List<T> findAll() throws DAOException;

    /**
     * Find entity by id t.
     *
     * @param id the id
     * @return the t
     * @throws DAOException the dao exception
     */
    T findEntityById(K id) throws DAOException;

    /**
     * Delete boolean.
     *
     * @param id the id
     * @return the boolean
     * @throws DAOException the dao exception
     */
    boolean delete(K id) throws DAOException;

    /**
     * Delete boolean.
     *
     * @param entity the entity
     * @return the boolean
     * @throws DAOException the dao exception
     */
    boolean delete(T entity) throws DAOException;

    /**
     * Create boolean.
     *
     * @param entity the entity
     * @return the boolean
     * @throws DAOException the dao exception
     */
    boolean create(T entity) throws DAOException;

    /**
     * Update boolean.
     *
     * @param entity the entity
     * @return the boolean
     * @throws DAOException the dao exception
     */
    boolean update(T entity) throws DAOException;
}
