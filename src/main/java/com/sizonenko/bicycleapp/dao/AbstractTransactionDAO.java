package com.sizonenko.bicycleapp.dao;

import com.sizonenko.bicycleapp.entity.Transaction;
import com.sizonenko.bicycleapp.exception.DAOException;

import java.util.List;

/**
 * The interface Abstract transaction dao.
 *
 * @param <K> the type parameter
 */
public interface AbstractTransactionDAO<K> extends AbstractDAO<K, Transaction> {
    /**
     * Find all by place list.
     *
     * @param memberId the member id
     * @return the list
     * @throws DAOException the dao exception
     */
    List<Transaction> findAllByPlace(Long memberId) throws DAOException;

    /**
     * Find not finished transaction by id transaction.
     *
     * @param id the id
     * @return the transaction
     * @throws DAOException the dao exception
     */
    Transaction findNotFinishedTransactionById(Long id) throws DAOException;

    /**
     * Complete transaction boolean.
     *
     * @param transaction the transaction
     * @return the boolean
     * @throws DAOException the dao exception
     */
    boolean completeTransaction(Transaction transaction) throws DAOException;

    /**
     * Create by reservation boolean.
     *
     * @param transaction the transaction
     * @return the boolean
     * @throws DAOException the dao exception
     */
    boolean createByReservation(Transaction transaction) throws DAOException;
}
