package com.sizonenko.bicycleapp.dao;

import com.sizonenko.bicycleapp.entity.Member;
import com.sizonenko.bicycleapp.exception.DAOException;

/**
 * The interface Abstract member dao.
 *
 * @param <K> the type parameter
 */
public interface AbstractMemberDAO<K> extends AbstractDAO<K, Member> {

    /**
     * Validate member boolean.
     *
     * @param login    the login
     * @param password the password
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean validateMember(String login, String password) throws DAOException;

    /**
     * Find entity by login member.
     *
     * @param login the login
     * @return the member
     * @throws DAOException the dao exception
     */
    public Member findEntityByLogin(String login) throws DAOException;

    /**
     * Unblock user by login boolean.
     *
     * @param login the login
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean unblockUserByLogin(String login) throws DAOException;

    /**
     * Update member by admin boolean.
     *
     * @param member the member
     * @return the boolean
     * @throws DAOException the dao exception
     */
    public boolean updateMemberByAdmin(Member member) throws DAOException;

    /**
     * Increment discount boolean.
     *
     * @param memberId  the member id
     * @param increment the increment
     * @return the boolean
     * @throws DAOException the dao exception
     */
    boolean incrementDiscount(long memberId, byte increment) throws DAOException;
}
