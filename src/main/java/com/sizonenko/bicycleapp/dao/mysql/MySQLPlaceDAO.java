package com.sizonenko.bicycleapp.dao.mysql;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.sizonenko.bicycleapp.dao.AbstractDAO;
import com.sizonenko.bicycleapp.entity.CreatorPlace;
import com.sizonenko.bicycleapp.entity.Place;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.pool.ConnectionPool;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type My sql place dao.
 */
public class MySQLPlaceDAO implements AbstractDAO<Long, Place>, MySQLDAO {

    private final String SQL_UPDATE_PLACE = "UPDATE place SET name = ?, address = ?, image = ?, member_id = ? WHERE place_id = ?";
    private final String SQL_CREATE_PLACE = "INSERT INTO place (name, address, image, member_id) VALUES (?,?,?,?)";

    @Override
    public List<Place> findAll() throws DAOException {
        List<Place> list = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM place LEFT JOIN member ON place.member_id = member.member_id");
            while (resultSet.next()) {
                Place place = new CreatorPlace().create();
                place.setPlaceId(resultSet.getLong("place_id"));
                place.setAddress(resultSet.getString("address"));
                place.setName(resultSet.getString("name"));
                place.getMember().setMemberId(resultSet.getLong("member_id"));
                place.getMember().setLastname(resultSet.getString("last_name"));
                place.getMember().setFirstname(resultSet.getString("first_name"));
                place.getMember().setLogin(resultSet.getString("login"));
                place.getMember().setImage(resultSet.getString("image"));
                list.add(place);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return list;
    }

    @Override
    public Place findEntityById(Long id) throws DAOException {
        Place place = new CreatorPlace().create();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM place as p WHERE p.place_id = " + id);
            while (resultSet.next()) {
                place.setPlaceId(resultSet.getInt("place_id"));
                place.setName(resultSet.getString("name"));
                place.setAddress(resultSet.getString("address"));
                place.getMember().setMemberId(resultSet.getLong("member_id"));
                place.setImage(resultSet.getString("image"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return place;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(Place entity) throws DAOException {
        return false;
    }

    @Override
    public boolean create(Place entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_CREATE_PLACE);
            pr.setString(1, entity.getName());
            pr.setString(2, entity.getAddress());
            byte[] byteContent = entity.getImage().getBytes();
            pr.setBlob(3, new SerialBlob(byteContent));
            pr.setLong(4, entity.getMember().getMemberId());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean update(Place entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_UPDATE_PLACE);
            pr.setString(1, entity.getName());
            pr.setString(2, entity.getAddress());
            byte[] image = entity.getImage().getBytes();
            pr.setBlob(3, new SerialBlob(image));
            pr.setLong(4, entity.getMember().getMemberId());
            pr.setLong(5, entity.getPlaceId());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }
}
