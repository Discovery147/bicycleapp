package com.sizonenko.bicycleapp.dao.mysql;

import com.mysql.jdbc.Blob;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.sizonenko.bicycleapp.dao.AbstractBicycleDAO;
import com.sizonenko.bicycleapp.entity.Bicycle;
import com.sizonenko.bicycleapp.entity.CreatorBicycle;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.pool.ConnectionPool;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * The type My sql bicycle dao.
 */
public class MySQLBicycleDAO implements AbstractBicycleDAO<Long>, MySQLDAO {

    private static final String SQL_DELETE_BICYCLE = "UPDATE bicycle SET deleted = 1 WHERE bicycle_id = ?";
    private final String SQL_UPDATE_BICYCLE = "UPDATE bicycle SET serial_number = ?, maker = ?, model = ?, color = ?, size = ?, price = ?, place_id = ?, image = ? WHERE bicycle_id = ?";
    private final String SQL_CREATE_BICYCLE = "INSERT INTO bicycle (place_id, serial_number, maker, model, color, size, image, price) VALUES (?,?,?,?,?,?,?,?)";

    @Override
    public List<Bicycle> findAll() throws DAOException {
        List<Bicycle> list = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM bicycle as b INNER JOIN place as p ON b.place_id = p.place_id WHERE b.deleted = 0 AND b.place_id IS NOT NULL");
            while (resultSet.next()) {
                Bicycle bicycle = new CreatorBicycle().create();
                bicycle.setBicycleId(resultSet.getInt("bicycle_id"));
                bicycle.setSerialNumber(resultSet.getString("serial_number"));
                bicycle.setMaker(resultSet.getString("maker"));
                bicycle.setModel(resultSet.getString("model"));
                bicycle.setColor(resultSet.getString("color"));
                bicycle.setSize(resultSet.getString("size"));
                bicycle.setPrice(resultSet.getBigDecimal("price"));
                bicycle.getPlace().setPlaceId(resultSet.getInt("place_id"));
                bicycle.getPlace().setName(resultSet.getString("name"));
                bicycle.getPlace().setAddress(resultSet.getString("address"));
                list.add(bicycle);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return list;
    }

    @Override
    public Bicycle findEntityById(Long id) throws DAOException {
        Bicycle bicycle = new CreatorBicycle().create();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM bicycle as b WHERE b.bicycle_id = " + id);
            while (resultSet.next()) {
                bicycle.setBicycleId(resultSet.getInt("bicycle_id"));
                bicycle.setSerialNumber(resultSet.getString("serial_number"));
                bicycle.setMaker(resultSet.getString("maker"));
                bicycle.setModel(resultSet.getString("model"));
                bicycle.setColor(resultSet.getString("color"));
                bicycle.setSize(resultSet.getString("size"));
                bicycle.setPrice(resultSet.getBigDecimal("price"));
                bicycle.getPlace().setPlaceId(resultSet.getInt("place_id"));
                bicycle.setImage(resultSet.getString("image"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return bicycle;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_DELETE_BICYCLE);
            pr.setLong(1, id);
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean delete(Bicycle entity) {
        return false;
    }

    @Override
    public boolean create(Bicycle entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_CREATE_BICYCLE);
            pr.setLong(1, entity.getPlace().getPlaceId());
            pr.setString(2, entity.getSerialNumber());
            pr.setString(3, entity.getMaker());
            pr.setString(4, entity.getModel());
            pr.setString(5, entity.getColor());
            pr.setString(6, entity.getSize());
            if (entity.getImage() != null) {
                byte[] byteImage = entity.getImage().getBytes();
                pr.setBlob(7, new SerialBlob(byteImage));
            }else{
                pr.setNull(7, Types.BLOB);
            }
            pr.setBigDecimal(8, entity.getPrice());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            if (e.getErrorCode() == 2627 || e.getErrorCode() == 2601){
                return false;
            }else{
                throw new DAOException(e);
            }
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean update(Bicycle entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_UPDATE_BICYCLE);
            pr.setString(1, entity.getSerialNumber());
            pr.setString(2, entity.getMaker());
            pr.setString(3, entity.getModel());
            pr.setString(4, entity.getColor());
            pr.setString(5, entity.getSize());
            pr.setBigDecimal(6, entity.getPrice());
            pr.setLong(7, entity.getPlace().getPlaceId());
            byte[] byteImage = entity.getImage().getBytes();
            pr.setBlob(8, new SerialBlob(byteImage));
            pr.setLong(9, entity.getBicycleId());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public List<Bicycle> findAllToReservation() throws DAOException {
        List<Bicycle> list = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery(
                    "SELECT b.bicycle_id, b.maker, b.model, b.color, b.size, b.price, p.name, p.address " +
                            "FROM bicycle as b " +
                            "INNER JOIN place AS p ON b.place_id = p.place_id " +
                            "WHERE b.bstatus = 1 AND p.pstatus_id = 1 AND b.deleted = 0"
            );
            while (resultSet.next()) {
                Bicycle bicycle = new CreatorBicycle().create();
                bicycle.setBicycleId(resultSet.getInt("b.bicycle_id"));
                bicycle.setMaker(resultSet.getString("b.maker"));
                bicycle.setModel(resultSet.getString("b.model"));
                bicycle.setColor(resultSet.getString("b.color"));
                bicycle.setSize(resultSet.getString("b.size"));
                bicycle.setPrice(resultSet.getBigDecimal("b.price"));
                bicycle.getPlace().setAddress(resultSet.getString("p.address"));
                bicycle.getPlace().setName(resultSet.getString("p.name"));
                list.add(bicycle);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return list;
    }

    @Override
    public List findAllByPlace(long memberId) throws DAOException {
        List<Bicycle> list = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery(
                    "SELECT b.bicycle_id, b.maker, b.model, b.color, b.size, b.price " +
                            "FROM bicycle as b " +
                            "INNER JOIN place AS p ON b.place_id = p.place_id " +
                            "WHERE b.bstatus = 1 AND p.pstatus_id = 1 AND b.deleted = 0 AND p.member_id = " + memberId +
                            " ORDER BY b.maker, b.model ASC"
            );
            while (resultSet.next()) {
                Bicycle bicycle = new CreatorBicycle().create();
                bicycle.setBicycleId(resultSet.getInt("b.bicycle_id"));
                bicycle.setMaker(resultSet.getString("b.maker"));
                bicycle.setModel(resultSet.getString("b.model"));
                bicycle.setColor(resultSet.getString("b.color"));
                bicycle.setSize(resultSet.getString("b.size"));
                bicycle.setPrice(resultSet.getBigDecimal("b.price"));
                list.add(bicycle);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return list;
    }

    @Override
    public boolean isBicycleAvailableById(Long bicycleId) throws DAOException {
        Connection cn = null;
        Statement st = null;
        boolean bstatus = false;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT bstatus FROM bicycle WHERE bicycle_id = " + bicycleId);
            while (resultSet.next()) {
                byte status = resultSet.getByte("bstatus");
                bstatus = status == 1;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return bstatus;
    }
}
