package com.sizonenko.bicycleapp.dao.mysql;

import com.mysql.jdbc.Connection;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.pool.ConnectionPool;

import java.sql.SQLException;

import com.mysql.jdbc.Statement;

/**
 * The interface MySQLDAO.
 */
public interface MySQLDAO {
    /**
     * Close com.mysql.jdbc.Statement.
     *
     * @param st the com.mysql.jdbc.Statement
     * @throws DAOException the dao exception
     */
    default void close(Statement st) throws DAOException {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            throw new DAOException();
        }
    }

    /**
     * Returns connection to connection pool.
     *
     * @param connection the com.mysql.jdbc.Connection
     */
    default void returnConnection(Connection connection) {
        if (connection != null) {
            ConnectionPool.getInstance().returnConnection(connection);
        }
    }
}
