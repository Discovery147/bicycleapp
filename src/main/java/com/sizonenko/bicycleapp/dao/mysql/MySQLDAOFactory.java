package com.sizonenko.bicycleapp.dao.mysql;

import com.sizonenko.bicycleapp.dao.AbstractDAO;
import com.sizonenko.bicycleapp.dao.Table;

/**
 * The type My sqldao factory.
 */
public abstract class MySQLDAOFactory {
    /**
     * Gets dao.
     *
     * @param name the name
     * @return the dao
     */
    public static AbstractDAO getDAO(Table name) {
        switch (name) {
            case MEMBER:
                return new MySQLMemberDAO();
            case BICYCLE:
                return new MySQLBicycleDAO();
            case RESERVATION:
                return new MySQLReservationDAO();
            case CONFIRM:
                return new MySQLConfirmDAO();
            case TRANSACTION:
                return new MySQLTransactionDAO();
            case DOCTYPE:
                return new MySQLDoctypeDAO();
            case DOCUMENT:
                return new MySQLDocumentDAO();
            case PLACE:
                return new MySQLPlaceDAO();
            case ROLE:
                return new MySQLRoleDAO();
            default:
                throw new UnsupportedOperationException();
        }
    }
}
