package com.sizonenko.bicycleapp.dao.mysql;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.sizonenko.bicycleapp.dao.AbstractMemberDAO;
import com.sizonenko.bicycleapp.entity.CreatorMember;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.pool.ConnectionPool;
import com.sizonenko.bicycleapp.entity.Member;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type My sql member dao.
 */
public class MySQLMemberDAO implements AbstractMemberDAO<Long>, MySQLDAO {

    private final String SQL_CHECK_MEMBER_BY_LOGIN_AND_PASSWORD = "SELECT member_id FROM member WHERE login = ? AND password = MD5(?)";
    private final String SQL_SELECT_MEMBER_BY_LOGIN = "SELECT * FROM member WHERE login=?";
    private final String SQL_INSERT_MEMBER = "INSERT INTO member (first_name, last_name, phone, email, login, password) VALUES (?,?,?,?,?,MD5(?))";
    private final String SQL_UPDATE_PROFILE = "UPDATE member SET login = ?, phone = ?, image = ? WHERE member_id = ?";
    private final String SQL_UPDATE_PROFILE_BY_ADMIN = "UPDATE member SET discount = ?, role_id = ?, blocked = ? WHERE member_id = ?";
    private final String SQL_UNBLOCK_USER = "UPDATE member SET blocked = 0 WHERE login = ?";
    private final String SQL_INCREMENT_DISCOUNT = "UPDATE member SET discount = discount + ? WHERE member_id = ?";

    @Override
    public List<Member> findAll() throws DAOException {
        List<Member> list = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM member INNER JOIN role ON member.role_id = role.role_id");
            while (resultSet.next()) {
                Member member = new CreatorMember().create();
                member.setMemberId(resultSet.getLong("member_id"));
                member.setFirstname(resultSet.getString("first_name"));
                member.setLastname(resultSet.getString("last_name"));
                member.setPhone(resultSet.getString("phone"));
                member.setEmail(resultSet.getString("email"));
                member.setBlocked(resultSet.getBoolean("blocked"));
                member.getRole().setRoleId(resultSet.getLong("role_id"));
                member.getRole().setName(resultSet.getString("name"));
                member.setLogin(resultSet.getString("login"));
                list.add(member);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return list;
    }

    @Override
    public boolean validateMember(String login, String password) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_CHECK_MEMBER_BY_LOGIN_AND_PASSWORD);
            pr.setString(1, login);
            pr.setString(2, password);
            ResultSet resultSet = pr.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public Member findEntityByLogin(String login) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        Member member = new CreatorMember().create();
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_SELECT_MEMBER_BY_LOGIN);
            pr.setString(1, login);
            ResultSet resultSet = pr.executeQuery();
            while (resultSet.next()) {
                member.getRole().setRoleId(resultSet.getByte("role_id"));
                member.setMemberId(resultSet.getLong("member_id"));
                member.setDiscount(resultSet.getByte("discount"));
                member.setFirstname(resultSet.getString("first_name"));
                member.setLastname(resultSet.getString("last_name"));
                member.setPhone(resultSet.getString("phone"));
                member.setEmail(resultSet.getString("email"));
                member.setLogin(resultSet.getString("login"));
                member.setAmount(resultSet.getBigDecimal("amount"));
                member.setBlocked(resultSet.getBoolean("blocked"));
                member.setImage(resultSet.getString("image"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
        return member;
    }

    @Override
    public boolean unblockUserByLogin(String login) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_UNBLOCK_USER);
            pr.setString(1, login);
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean updateMemberByAdmin(Member member) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_UPDATE_PROFILE_BY_ADMIN);
            pr.setByte(1, member.getDiscount());
            pr.setLong(2, member.getRole().getRoleId());
            pr.setBoolean(3, member.isBlocked());
            pr.setLong(4, member.getMemberId());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean incrementDiscount(long memberId, byte increment) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_INCREMENT_DISCOUNT);
            pr.setByte(1, increment);
            pr.setLong(2, memberId);
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public Member findEntityById(Long id) throws DAOException {
        Member member = new CreatorMember().create();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM member INNER JOIN role ON member.role_id = role.role_id WHERE member_id = " + id);
            while (resultSet.next()) {
                member.setMemberId(resultSet.getLong("member_id"));
                member.setFirstname(resultSet.getString("first_name"));
                member.setLastname(resultSet.getString("last_name"));
                member.setPhone(resultSet.getString("phone"));
                member.setEmail(resultSet.getString("email"));
                member.setBlocked(resultSet.getBoolean("blocked"));
                member.getRole().setRoleId(resultSet.getLong("role_id"));
                member.getRole().setName(resultSet.getString("name"));
                member.setLogin(resultSet.getString("login"));
                member.setDiscount(resultSet.getByte("discount"));
                member.setImage(resultSet.getString("image"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return member;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public boolean delete(Member entity) {
        return false;
    }

    @Override
    public boolean create(Member entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_INSERT_MEMBER);
            pr.setString(1, entity.getFirstname());
            pr.setString(2, entity.getLastname());
            pr.setString(3, entity.getPhone());
            pr.setString(4, entity.getEmail());
            pr.setString(5, entity.getLogin());
            pr.setString(6, entity.getPassword());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            if (e.getErrorCode() == 2627 || e.getErrorCode() == 2601){
                return false;
            }else{
                throw new DAOException(e);
            }
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean update(Member entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_UPDATE_PROFILE);
            pr.setString(1, entity.getLogin());
            pr.setString(2, entity.getPhone());
            byte[] byteImage = entity.getImage().getBytes();
            pr.setBlob(3, new SerialBlob(byteImage));
            pr.setLong(4, entity.getMemberId());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }
}
