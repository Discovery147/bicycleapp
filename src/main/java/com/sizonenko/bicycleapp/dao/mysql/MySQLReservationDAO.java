package com.sizonenko.bicycleapp.dao.mysql;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.sizonenko.bicycleapp.dao.AbstractReservationDAO;
import com.sizonenko.bicycleapp.entity.Reservation;
import com.sizonenko.bicycleapp.entity.CreatorReservation;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.pool.ConnectionPool;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import java.util.ArrayList;
import java.util.List;

/**
 * The type My sql reservation dao.
 */
public class MySQLReservationDAO implements AbstractReservationDAO<Long>, MySQLDAO {

    private final String SQL_INSERT_RESERVATION = "INSERT INTO reservation (member_id, bicycle_id, start_time, end_time, amount) VALUES (?,?,?,?,?)";
    private final String SQL_CHECK_RESERVATION = "SELECT * FROM reservation WHERE bicycle_id = ? AND NOT((start_time > ? AND start_time > ?) OR (end_time < ? AND end_time < ?))";
    private final String SQL_CHECK_TRANSACTION = "SELECT transaction_id FROM transaction WHERE bicycle_id = ? AND ? < DATE_ADD(start_time, INTERVAL period+1 HOUR) AND end_time IS NULL";
    private final String SQL_DELETE_RESERVATION = "DELETE FROM reservation WHERE request_id = ?";

    @Override
    public List<Reservation> findAll() {
        return null;
    }

    @Override
    public List<Reservation> findAllToCalendar() throws DAOException {
        List<Reservation> list = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery(
                    "SELECT p.name as name, p.address as address, b.maker as maker, b.model as model, b.color as color, b.size as size, r.start_time as start_time, r.end_time as end_time " +
                            "FROM reservation AS r " +
                            "INNER JOIN bicycle AS b ON r.bicycle_id = b.bicycle_id " +
                            "INNER JOIN place AS p ON b.place_id = p.place_id WHERE r.end_time > NOW()" +
                            "UNION " +
                            "SELECT p.name as name, p.address as address, b.maker as maker, b.model as model, b.color as color, b.size as size, t.start_time as start_time, DATE_ADD(t.start_time, INTERVAL t.period HOUR) as end_time " +
                            "FROM transaction AS t " +
                            "INNER JOIN bicycle AS b ON t.bicycle_id = b.bicycle_id " +
                            "INNER JOIN place AS p ON b.place_id = p.place_id WHERE DATE_ADD(t.start_time, INTERVAL t.period HOUR) > NOW() AND t.end_time IS NULL");
            while (resultSet.next()) {
                Reservation reservation = new CreatorReservation().create();
                reservation.getBicycle().setMaker(resultSet.getString("maker"));
                reservation.getBicycle().setModel(resultSet.getString("model"));
                reservation.getBicycle().setColor(resultSet.getString("color"));
                reservation.getBicycle().setSize(resultSet.getString("size"));
                reservation.getBicycle().getPlace().setName(resultSet.getString("name"));
                reservation.getBicycle().getPlace().setAddress(resultSet.getString("address"));
                reservation.setStartTime(resultSet.getTimestamp("start_time"));
                reservation.setEndTime(resultSet.getTimestamp("end_time"));
                list.add(reservation);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return list;
    }

    @Override
    public List<Reservation> findAllByMemberId(long memberId) throws DAOException {
        List<Reservation> list = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery(
                    "SELECT p.address, b.maker, b.model, b.size, b.color, r.start_time, r.end_time, r.amount FROM reservation AS r " +
                            "INNER JOIN bicycle AS b ON r.bicycle_id = b.bicycle_id " +
                            "INNER JOIN place AS p ON b.place_id = p.place_id " +
                            "WHERE r.end_time > NOW() AND r.member_id = " + memberId);
            while (resultSet.next()) {
                Reservation reservation = new CreatorReservation().create();
                reservation.getBicycle().setMaker(resultSet.getString("b.maker"));
                reservation.getBicycle().setModel(resultSet.getString("b.model"));
                reservation.getBicycle().setColor(resultSet.getString("b.color"));
                reservation.getBicycle().setSize(resultSet.getString("b.size"));
                reservation.getBicycle().getPlace().setAddress(resultSet.getString("p.address"));
                reservation.setStartTime(resultSet.getTimestamp("r.start_time"));
                reservation.setEndTime(resultSet.getTimestamp("r.end_time"));
                reservation.setAmount(resultSet.getBigDecimal("r.amount"));
                list.add(reservation);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return list;
    }

    @Override
    public List<Reservation> findAllByPlace(long memberId) throws DAOException {
        List<Reservation> list = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery(
                    "SELECT m.member_id, m.first_name, m.last_name, m.phone, b.bicycle_id, b.maker, b.model, b.size, b.color, r.request_id, r.start_time, r.end_time, r.amount " +
                            "FROM reservation AS r " +
                            "INNER JOIN member AS m ON r.member_id = m.member_id " +
                            "INNER JOIN bicycle AS b ON r.bicycle_id = b.bicycle_id " +
                            "INNER JOIN place AS p ON b.place_id = p.place_id " +
                            "WHERE r.end_time > NOW() AND p.member_id = " + memberId);
            while (resultSet.next()) {
                Reservation reservation = new CreatorReservation().create();
                reservation.getMember().setMemberId(resultSet.getLong("m.member_id"));
                reservation.getMember().setFirstname(resultSet.getString("m.first_name"));
                reservation.getMember().setLastname(resultSet.getString("m.last_name"));
                reservation.getMember().setPhone(resultSet.getString("m.phone"));
                reservation.getBicycle().setBicycleId(resultSet.getLong("b.bicycle_id"));
                reservation.getBicycle().setMaker(resultSet.getString("b.maker"));
                reservation.getBicycle().setModel(resultSet.getString("b.model"));
                reservation.getBicycle().setColor(resultSet.getString("b.color"));
                reservation.getBicycle().setSize(resultSet.getString("b.size"));
                reservation.setReservationId(resultSet.getLong("r.request_id"));
                reservation.setStartTime(resultSet.getTimestamp("r.start_time"));
                reservation.setEndTime(resultSet.getTimestamp("r.end_time"));
                reservation.setAmount(resultSet.getBigDecimal("r.amount"));
                list.add(reservation);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return list;
    }

    @Override
    public boolean isBicycleCanStartTransaction(Reservation entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_CHECK_RESERVATION);
            pr.setLong(1, entity.getBicycle().getBicycleId());
            pr.setTimestamp(2, entity.getStartTime());
            pr.setTimestamp(3, entity.getEndTime());
            pr.setTimestamp(4, entity.getStartTime());
            pr.setTimestamp(5, entity.getEndTime());
            ResultSet resultSet = pr.executeQuery();
            return !resultSet.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public Reservation findEntityById(Long id) throws DAOException {
        Reservation reservation = new CreatorReservation().create();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery(
                    "SELECT m.*, b.*, r.*" +
                            "FROM reservation AS r " +
                            "INNER JOIN member AS m ON r.member_id = m.member_id " +
                            "INNER JOIN bicycle AS b ON r.bicycle_id = b.bicycle_id " +
                            "WHERE r.request_id = " + id);
            while (resultSet.next()) {
                reservation.getMember().setMemberId(resultSet.getLong("m.member_id"));
                reservation.getMember().setFirstname(resultSet.getString("m.first_name"));
                reservation.getMember().setLastname(resultSet.getString("m.last_name"));
                reservation.getMember().setPhone(resultSet.getString("m.phone"));
                reservation.getBicycle().setBicycleId(resultSet.getLong("b.bicycle_id"));
                reservation.getBicycle().setMaker(resultSet.getString("b.maker"));
                reservation.getBicycle().setModel(resultSet.getString("b.model"));
                reservation.getBicycle().setColor(resultSet.getString("b.color"));
                reservation.getBicycle().setSize(resultSet.getString("b.size"));
                reservation.setReservationId(resultSet.getLong("r.request_id"));
                reservation.setStartTime(resultSet.getTimestamp("r.start_time"));
                reservation.setEndTime(resultSet.getTimestamp("r.end_time"));
                reservation.setAmount(resultSet.getBigDecimal("r.amount"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return reservation;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_DELETE_RESERVATION);
            pr.setLong(1, id);
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean delete(Reservation entity) {
        return false;
    }

    @Override
    public boolean create(Reservation entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_CHECK_RESERVATION);
            pr.setLong(1, entity.getBicycle().getBicycleId());
            pr.setTimestamp(2, entity.getStartTime());
            pr.setTimestamp(3, entity.getEndTime());
            pr.setTimestamp(4, entity.getStartTime());
            pr.setTimestamp(5, entity.getEndTime());
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()) {
                return false;
            }
            pr = (PreparedStatement) cn.prepareStatement(SQL_CHECK_TRANSACTION);
            pr.setLong(1, entity.getBicycle().getBicycleId());
            pr.setTimestamp(2, entity.getStartTime());
            resultSet = pr.executeQuery();
            if (resultSet.next()) {
                return false;
            }
            pr = (PreparedStatement) cn.prepareStatement(SQL_INSERT_RESERVATION);
            pr.setLong(1, entity.getMember().getMemberId());
            pr.setLong(2, entity.getBicycle().getBicycleId());
            pr.setTimestamp(3, entity.getStartTime());
            pr.setTimestamp(4, entity.getEndTime());
            pr.setBigDecimal(5, entity.getAmount());
            return pr.executeUpdate() > 0;  // ЕСЛИ ID ВЕЛОСИПЕДА НАЙДЕН, ТО ДАТЫ СТАРТА И ФИНИША ДОЛЖНЫ БЫТЬ ОБЕ РАНЬШЕ НАЙДЕННЫХ СТАРТА И ФИНИША ЛИБО ПОЗЖЕ
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean update(Reservation entity) {
        return false;
    }
}
