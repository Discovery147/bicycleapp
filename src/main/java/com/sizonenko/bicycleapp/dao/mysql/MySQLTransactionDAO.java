package com.sizonenko.bicycleapp.dao.mysql;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.sizonenko.bicycleapp.dao.AbstractTransactionDAO;
import com.sizonenko.bicycleapp.entity.*;
import com.sizonenko.bicycleapp.entity.CreatorTransaction;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.pool.ConnectionPool;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type My sql transaction dao.
 */
public class MySQLTransactionDAO implements AbstractTransactionDAO<Long>, MySQLDAO {

    private final String SQL_COMPLETE_TRANSACTION = "UPDATE transaction SET end_time = CURRENT_TIMESTAMP, amount = ? WHERE transaction_id = ?";
    private final String SQL_INSERT_TRANSACTION_BY_RESERVATION = "INSERT INTO transaction (member_id, bicycle_id, start_time, period, expected_amount, document_id) VALUES (?,?,?,?,?,?)";
    private final String SQL_INSERT_TRANSACTION = "INSERT INTO transaction (bicycle_id, start_time, period, expected_amount, document_id) VALUES (?,?,?,?,?)";

    @Override
    public List<Transaction> findAll() throws DAOException {
        return null;
    }

    @Override
    public Transaction findEntityById(Long id) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public boolean delete(Transaction entity) throws DAOException {
        return false;
    }

    @Override
    public boolean create(Transaction entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_INSERT_TRANSACTION);
            pr.setLong(1, entity.getBicycle().getBicycleId());
            pr.setTimestamp(2, entity.getStartTime());
            pr.setFloat(3, entity.getPeriod());
            pr.setBigDecimal(4, entity.getExpectedAmount());
            pr.setLong(5, entity.getDocument().getDocumentId());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean update(Transaction entity) throws DAOException {
        return false;
    }

    @Override
    public List findAllByPlace(Long memberId) throws DAOException {
        List<Transaction> list = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement(); // Данные человека (если резерв), документ - залог, данные велосипеда, дата начала, примерная дата окончания и ожидаемая сумма.
            ResultSet rs = st.executeQuery("SELECT m.member_id, m.first_name, m.last_name, m.phone, d.document_id, d.number, d.other, dtype.name, " +
                    "b.bicycle_id, b.maker, b.model, b.color, b.size, t.transaction_id, t.start_time, DATE_ADD(t.start_time, INTERVAL t.period HOUR) AS end_time, t.expected_amount " +
                    "FROM transaction AS t " +
                    "LEFT JOIN member AS m ON t.member_id = m.member_id " +
                    "INNER JOIN document AS d ON t.document_id = d.document_id " +
                    "INNER JOIN doctype AS dtype ON d.doctype_id = dtype.doctype_id " +
                    "INNER JOIN bicycle AS b ON t.bicycle_id = b.bicycle_id " +
                    "INNER JOIN place AS p ON b.place_id = p.place_id WHERE t.end_time IS NULL AND p.member_id = " + memberId);
            while (rs.next()) {
                Transaction transaction = new CreatorTransaction().create();
                transaction.getMember().setMemberId(rs.getLong("m.member_id"));
                transaction.getMember().setFirstname(rs.getString("m.first_name"));
                transaction.getMember().setLastname(rs.getString("m.last_name"));
                transaction.getMember().setPhone(rs.getString("m.phone"));
                transaction.getDocument().getDoctype().setName(rs.getString("dtype.name"));
                transaction.getDocument().setDocumentId(rs.getLong("d.document_id"));
                transaction.getDocument().setNumber(rs.getString("d.number"));
                transaction.getDocument().setOther(rs.getString("d.other"));
                transaction.getBicycle().setBicycleId(rs.getLong("b.bicycle_id"));
                transaction.getBicycle().setMaker(rs.getString("b.maker"));
                transaction.getBicycle().setModel(rs.getString("b.model"));
                transaction.getBicycle().setColor(rs.getString("b.color"));
                transaction.getBicycle().setSize(rs.getString("b.size"));
                transaction.setTransactionId(rs.getLong("t.transaction_id"));
                transaction.setStartTime(rs.getTimestamp("t.start_time"));
                transaction.setEndTime(rs.getTimestamp("end_time"));
                transaction.setExpectedAmount(rs.getBigDecimal("t.expected_amount"));
                list.add(transaction);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return list;
    }

    @Override
    public Transaction findNotFinishedTransactionById(Long id) throws DAOException {
        Transaction transaction = new CreatorTransaction().create();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT m.member_id, m.first_name, m.last_name, m.phone, d.document_id, d.number, d.other, dtype.name, " +
                    "b.bicycle_id, b.maker, b.model, b.color, b.size, t.transaction_id, t.start_time, DATE_ADD(t.start_time, INTERVAL t.period HOUR) AS end_time, t.expected_amount " +
                    "FROM transaction AS t " +
                    "LEFT JOIN member AS m ON t.member_id = m.member_id " +
                    "INNER JOIN document AS d ON t.document_id = d.document_id " +
                    "INNER JOIN doctype AS dtype ON d.doctype_id = dtype.doctype_id " +
                    "INNER JOIN bicycle AS b ON t.bicycle_id = b.bicycle_id " +
                    "INNER JOIN place AS p ON b.place_id = p.place_id WHERE t.transaction_id = " + id);
            while (rs.next()) {
                transaction.getMember().setMemberId(rs.getLong("m.member_id"));
                transaction.getMember().setFirstname(rs.getString("m.first_name"));
                transaction.getMember().setLastname(rs.getString("m.last_name"));
                transaction.getMember().setPhone(rs.getString("m.phone"));
                transaction.getDocument().getDoctype().setName(rs.getString("dtype.name"));
                transaction.getDocument().setDocumentId(rs.getLong("d.document_id"));
                transaction.getDocument().setNumber(rs.getString("d.number"));
                transaction.getDocument().setOther(rs.getString("d.other"));
                transaction.getBicycle().setBicycleId(rs.getLong("b.bicycle_id"));
                transaction.getBicycle().setMaker(rs.getString("b.maker"));
                transaction.getBicycle().setModel(rs.getString("b.model"));
                transaction.getBicycle().setColor(rs.getString("b.color"));
                transaction.getBicycle().setSize(rs.getString("b.size"));
                transaction.setTransactionId(rs.getLong("t.transaction_id"));
                transaction.setStartTime(rs.getTimestamp("t.start_time"));
                transaction.setEndTime(rs.getTimestamp("end_time"));
                transaction.setExpectedAmount(rs.getBigDecimal("t.expected_amount"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return transaction;
    }

    @Override
    public boolean completeTransaction(Transaction transaction) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_COMPLETE_TRANSACTION);
            pr.setBigDecimal(1, transaction.getAmount());
            pr.setLong(2, transaction.getTransactionId());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean createByReservation(Transaction entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_INSERT_TRANSACTION_BY_RESERVATION);
            pr.setLong(1, entity.getMember().getMemberId());
            pr.setLong(2, entity.getBicycle().getBicycleId());
            pr.setTimestamp(3, entity.getStartTime());
            pr.setFloat(4, entity.getPeriod());
            pr.setBigDecimal(5, entity.getExpectedAmount());
            pr.setLong(6, entity.getDocument().getDocumentId());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }
}
