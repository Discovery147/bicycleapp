package com.sizonenko.bicycleapp.dao.mysql;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.sizonenko.bicycleapp.dao.AbstractDAO;
import com.sizonenko.bicycleapp.entity.CreatorRole;
import com.sizonenko.bicycleapp.entity.Role;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.pool.ConnectionPool;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type My sql role dao.
 */
public class MySQLRoleDAO implements AbstractDAO<Long, Role>, MySQLDAO {
    @Override
    public List<Role> findAll() throws DAOException {
        List<Role> list = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM role");
            while (resultSet.next()) {
                Role role = new CreatorRole().create();
                role.setRoleId(resultSet.getLong("role_id"));
                role.setName(resultSet.getString("name"));
                list.add(role);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return list;
    }

    @Override
    public Role findEntityById(Long id) throws DAOException {
        return null;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(Role entity) throws DAOException {
        return false;
    }

    @Override
    public boolean create(Role entity) throws DAOException {
        return false;
    }

    @Override
    public boolean update(Role entity) throws DAOException {
        return false;
    }
}
