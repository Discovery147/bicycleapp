package com.sizonenko.bicycleapp.dao.mysql;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.sizonenko.bicycleapp.dao.AbstractDocumentDAO;
import com.sizonenko.bicycleapp.entity.Document;
import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.pool.ConnectionPool;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The type My sql document dao.
 */
public class MySQLDocumentDAO implements AbstractDocumentDAO<Long>, MySQLDAO {

    private final String SQL_INSERT_DOCUMENT = "INSERT INTO document (number, other, doctype_id) VALUES (?,?,?)";
    private final String SQL_DELETE_DOCUMENT = "DELETE FROM document WHERE number = ?";

    @Override
    public List<Document> findAll() throws DAOException {
        return null;
    }

    @Override
    public Document findEntityById(Long id) throws DAOException {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public boolean delete(Document entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_DELETE_DOCUMENT);
            pr.setString(1, entity.getNumber());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean create(Document entity) throws DAOException {
        Connection cn = null;
        PreparedStatement pr = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            pr = (PreparedStatement) cn.prepareStatement(SQL_INSERT_DOCUMENT);
            pr.setString(1, entity.getNumber());
            pr.setString(2, entity.getOther());
            pr.setLong(3, entity.getDoctype().getDoctypeId());
            return pr.executeUpdate() > 0;
        } catch (SQLException e) {
            if (e.getErrorCode() == 2627 || e.getErrorCode() == 2601){
                return false;
            }else{
                throw new DAOException(e);
            }
        } finally {
            close(pr);
            returnConnection(cn);
        }
    }

    @Override
    public boolean update(Document entity) throws DAOException {
        return false;
    }

    @Override
    public Long findIdByEntity(Document entity) throws DAOException {
        Long id = null;
        Connection cn = null;
        Statement st = null;
        try {
            cn = (Connection) ConnectionPool.getInstance().getConnection();
            st = (Statement) cn.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT document_id FROM document WHERE number = " + entity.getNumber());
            while (resultSet.next()) {
                id = resultSet.getLong("document_id");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(st);
            returnConnection(cn);
        }
        return id;
    }
}
