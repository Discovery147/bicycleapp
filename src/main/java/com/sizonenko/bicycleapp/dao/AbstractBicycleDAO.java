package com.sizonenko.bicycleapp.dao;

import com.sizonenko.bicycleapp.entity.Bicycle;
import com.sizonenko.bicycleapp.exception.DAOException;

import java.util.List;

/**
 * The interface Abstract bicycle dao.
 *
 * @param <K> the type parameter
 */
public interface AbstractBicycleDAO<K> extends AbstractDAO<K, Bicycle> {
    /**
     * Find all to reservation list.
     *
     * @return the list
     * @throws DAOException the dao exception
     */
    List<Bicycle> findAllToReservation() throws DAOException;

    /**
     * Find all by place list.
     *
     * @param memberId the member id
     * @return the list
     * @throws DAOException the dao exception
     */
    List<Bicycle> findAllByPlace(long memberId) throws DAOException;

    /**
     * Is bicycle available by id boolean.
     *
     * @param bicycleId the bicycle id
     * @return the boolean
     * @throws DAOException the dao exception
     */
    boolean isBicycleAvailableById(Long bicycleId) throws DAOException;
}