package com.sizonenko.bicycleapp.dao;

import com.sizonenko.bicycleapp.entity.Document;
import com.sizonenko.bicycleapp.exception.DAOException;

/**
 * The interface Abstract document dao.
 *
 * @param <K> the type parameter
 */
public interface AbstractDocumentDAO<K> extends AbstractDAO<K, Document> {
    /**
     * Find id by entity k.
     *
     * @param entity the entity
     * @return the k
     * @throws DAOException the dao exception
     */
    K findIdByEntity(Document entity) throws DAOException;
}