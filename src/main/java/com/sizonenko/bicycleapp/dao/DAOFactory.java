package com.sizonenko.bicycleapp.dao;

import com.sizonenko.bicycleapp.dao.mysql.MySQLDAOFactory;
import com.sizonenko.bicycleapp.pool.ConnectionConfig;

/**
 * The type Dao factory.
 */
public abstract class DAOFactory {
    /**
     * Gets dao.
     *
     * @param name the name
     * @return the dao
     */
    public static AbstractDAO getDAO(Table name) {
        switch (ConnectionConfig.getType()) {
            case MYSQL:
                return MySQLDAOFactory.getDAO(name);
            case ORACLE:
                throw new UnsupportedOperationException();
            case POSTGRESQL:
                throw new UnsupportedOperationException();
            default:
                throw new UnsupportedOperationException();
        }

    }
}
