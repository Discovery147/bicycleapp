package com.sizonenko.bicycleapp.dao;

import com.sizonenko.bicycleapp.entity.Reservation;
import com.sizonenko.bicycleapp.exception.DAOException;

import java.util.List;

/**
 * The interface Abstract reservation dao.
 *
 * @param <K> the type parameter
 */
public interface AbstractReservationDAO<K> extends AbstractDAO<K, Reservation> {
    /**
     * Find all to calendar list.
     *
     * @return the list
     * @throws DAOException the dao exception
     */
    public List<Reservation> findAllToCalendar() throws DAOException;

    /**
     * Find all by member id list.
     *
     * @param memberId the member id
     * @return the list
     * @throws DAOException the dao exception
     */
    public List<Reservation> findAllByMemberId(long memberId) throws DAOException;

    /**
     * Find all by place list.
     *
     * @param memberId the member id
     * @return the list
     * @throws DAOException the dao exception
     */
    public List<Reservation> findAllByPlace(long memberId) throws DAOException;

    /**
     * Is bicycle can start transaction boolean.
     *
     * @param entity the entity
     * @return the boolean
     * @throws DAOException the dao exception
     */
    boolean isBicycleCanStartTransaction(Reservation entity) throws DAOException;

    public boolean create(Reservation reservation) throws DAOException;
}
