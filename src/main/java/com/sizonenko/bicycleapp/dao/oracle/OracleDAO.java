package com.sizonenko.bicycleapp.dao.oracle;

import com.sizonenko.bicycleapp.exception.DAOException;
import com.sizonenko.bicycleapp.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * The interface Oracle dao.
 */
public interface OracleDAO {
    /**
     * Close.
     *
     * @param st the Statement
     * @throws DAOException the dao exception
     */
    default void close(Statement st) throws DAOException {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            throw new DAOException();
        }
    }

    /**
     * Close.
     *
     * @param connection the Connection
     */
    default void close(Connection connection) {
        if (connection != null) {
            ConnectionPool.getInstance().returnConnection(connection);
        }
    }
}
