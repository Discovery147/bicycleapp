package com.sizonenko.bicycleapp.dao;

/**
 * The enum Table.
 */
public enum Table {
    /**
     * Member table.
     */
    MEMBER,
    /**
     * Confirm table.
     */
    CONFIRM,
    /**
     * Bicycle table.
     */
    BICYCLE,
    /**
     * Transaction table.
     */
    TRANSACTION,
    /**
     * Doctype table.
     */
    DOCTYPE,
    /**
     * Document table.
     */
    DOCUMENT,
    /**
     * Place table.
     */
    PLACE,
    /**
     * Role table.
     */
    ROLE,
    /**
     * Reservation table.
     */
    RESERVATION
}
