package com.sizonenko.bicycleapp.pool;

import java.util.Properties;

/**
 * The type Connection config.
 */
public abstract class ConnectionConfig {
    /**
     * The enum Connection type.
     */
    public enum ConnectionType {
        /**
         * Mysql connection type.
         */
        MYSQL, /**
         * Oracle connection type.
         */
        ORACLE, /**
         * Postgresql connection type.
         */
        POSTGRESQL;
    }

    private static final ConnectionType type = ConnectionType.MYSQL;
    private static Properties properties;

    /**
     * Sets properties.
     *
     * @param properties the properties
     */
    public static void setProperties(Properties properties) {
        ConnectionConfig.properties = properties;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public static ConnectionType getType() {
        return type;
    }

    /**
     * Gets properties.
     *
     * @return the properties
     */
    public static Properties getProperties() {
        return properties;
    }
}
