package com.sizonenko.bicycleapp.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * The type Connection pool.
 */
public class ConnectionPool {

    private static int poolSize;
    private static AtomicBoolean flag = new AtomicBoolean(true);
    private static final ReentrantLock lock = new ReentrantLock();
    private static ConnectionPool instance;
    private ConcurrentLinkedDeque<Connection> queue;
    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);

    private ConnectionPool() {
        poolSize = DefaultConnectionConfig.DEFAULT_POOL_SIZE;
        queue = new ConcurrentLinkedDeque();
        for (int i = 0; i < poolSize; i++) {
            queue.offer(createConnection());
        }
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static ConnectionPool getInstance() {
        if (flag.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    flag.set(false);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Gets pool size.
     *
     * @return the pool size
     */
    public static int getPoolSize() {
        return poolSize;
    }

    /**
     * Gets current pool size.
     *
     * @return the current pool size
     */
    public int getCurrentPoolSize() {
        return queue.size();
    }

    /**
     * Sets pool size.
     *
     * @param poolSize the pool size
     */
    public static void setPoolSize(int poolSize) {
        ConnectionPool.poolSize = poolSize;
    }

    private static Connection createConnection() {
        Connection connection = null;
        try {
            if (ConnectionConfig.getProperties() != null) {
                connection = DriverManager.getConnection(
                        ConnectionConfig.getProperties().getProperty("url"),
                        ConnectionConfig.getProperties().getProperty("user"),
                        ConnectionConfig.getProperties().getProperty("password")
                );
            } else {
                connection = new DefaultConnectionConfig().getConnection();
            }
        } catch (NullPointerException | SQLException e) {
            LOGGER.log(Level.ERROR, "connection properties is not available");
            connection = new DefaultConnectionConfig().getConnection();
        }
        return connection;
    }

    /**
     * Gets connection.
     *
     * @return the connection
     */
    public Connection getConnection() {
        Supplier<Connection> supplier = queue::poll;
        return supplier.get();
    }

    /**
     * Return connection.
     *
     * @param connection the connection
     */
    public void returnConnection(Connection connection) {
        Predicate<Connection> isNotNull = Objects::nonNull;
        if (isNotNull.test(connection)) {
            queue.offer(connection);
        } else {
            LOGGER.log(Level.ERROR, "returned connection is null");
        }
    }
}
