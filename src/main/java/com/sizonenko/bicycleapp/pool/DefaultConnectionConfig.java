package com.sizonenko.bicycleapp.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * The type Default connection config.
 */
public class DefaultConnectionConfig {

    private static final Logger LOGGER = Logger.getLogger(DefaultConnectionConfig.class);
    /**
     * The Default pool size.
     */
    static final int DEFAULT_POOL_SIZE = 10;
    private static String URL = "jdbc:mysql://localhost:3306/bicycle_db";
    private static final String USER = "root";
    private static final String PASSWORD = "8774460";

    /**
     * Sets url.
     *
     * @param URL the url
     */
    public static void setURL(String URL) {
        DefaultConnectionConfig.URL = URL;
    }

    /**
     * Gets connection.
     *
     * @return the connection
     */
    public Connection getConnection() {
        try {
            return DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e) {
            LOGGER.log(Level.FATAL, "default connection properties is not available");
            throw new RuntimeException();
        }
    }
}
