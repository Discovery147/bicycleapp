package com.sizonenko.bicycleapp.localization;

/**
 * The enum Locale.
 */
public enum Locale {
    /**
     * Russia locale.
     */
    ru_RU, /**
     * USA locale.
     */
    en_US, /**
     * Belarus locale.
     */
    by_BY
}
