package com.sizonenko.bicycleapp.localization;

import java.util.EnumMap;

/**
 * The type Date pattern.
 */
public class DatePattern {
    private EnumMap<Locale, String> map = new EnumMap<Locale, String>(Locale.class) {{
        this.put(Locale.ru_RU, "dd.MM.yyyy HH:mm:ss");
        this.put(Locale.en_US, "M/d/yyyy HH:mm:ss");
        this.put(Locale.by_BY, "d.M.yyyy HH:mm:ss");
    }};
    private final static DatePattern instance = new DatePattern();

    private DatePattern() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static DatePattern getInstance() {
        return instance;
    }

    /**
     * Get string.
     *
     * @param locale the locale
     * @return the string
     */
    public String get(Locale locale) {
        return locale != null ? map.get(locale) : map.get(Locale.ru_RU);
    }
}
