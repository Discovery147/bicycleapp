package com.sizonenko.bicycleapp.localization;

import com.sizonenko.bicycleapp.parser.TimeStampParser;

import java.sql.Timestamp;

/**
 * The type Timestamp locale.
 */
public class TimestampLocale extends Timestamp {

    private Locale locale = Locale.ru_RU;

    /**
     * Instantiates a new Timestamp locale.
     *
     * @param time   the time
     * @param locale the locale
     */
    public TimestampLocale(Timestamp time, Locale locale) {
        super(time.getTime());
        this.locale = locale;
    }

    @Override
    public String toString() {
        return TimeStampParser.parseDateToStringByLocale(this, locale);
    }
}
