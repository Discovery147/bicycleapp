<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.languageAttr.toString()}" scope="session" />
<fmt:setBundle basename="resource.localization.auth_modal.pagecontent" var="localeAuth" />

<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/register/style.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>
<body>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="container">
                    <section>
                        <div id="container_demo" >
                            <a class="hiddenanchor" id="toregister"></a>
                            <a class="hiddenanchor" id="tologin"></a>
                            <div id="wrapper">
                                <div id="login" class="animate form">
                                    <form id="loginForm" method="post" onsubmit="signIn(); return false;">
                                        <input type="hidden" name="command" value="LOG_IN">
                                        <h1><fmt:message key="log.in" bundle="${localeAuth}" /></h1>
                                        <p>
                                            <label for="loginsignin" class="uname" > <fmt:message key="login" bundle="${localeAuth}" /> </label>
                                            <input id="loginsignin" required="required" type="text" placeholder="Username"/>
                                        </p>
                                        <p>
                                            <label for="passwordsignin" class="youpasswd"> <fmt:message key="password" bundle="${localeAuth}" /> </label>
                                            <input id="passwordsignin" required="required" type="password" placeholder="Password" />
                                        </p>
                                        <div class="alert alert-danger" style="display: none" id="signInError">
                                            <strong><fmt:message key="error" bundle="${localeAuth}" /></strong> <fmt:message key="data.error" bundle="${localeAuth}" />
                                        </div>
                                        <input type="submit" class="btn btn-success" value="<fmt:message key="log.in" bundle="${localeAuth}" />"/>
                                        <p class="change_link">
                                            <fmt:message key="account.no" bundle="${localeAuth}" />
                                            <a href="#toregister" class="to_register"><fmt:message key="join" bundle="${localeAuth}" /></a>
                                            <a href="#myModal" class="to_register" data-dismiss="modal"><fmt:message key="close" bundle="${localeAuth}" /></a>
                                        </p>
                                    </form>
                                </div>
                                <div id="register" class="animate form" style="width: 100%; margin-bottom: 10%">
                                    <form  name="registerForm" method = "post" id="registerForm" onsubmit="signUp(); return false;">
                                        <input type="hidden" name="command" value="register">
                                        <h1> <fmt:message key="sign.up" bundle="${localeAuth}" /> </h1>
                                        <p>
                                            <label for="firstnamesignup"><fmt:message key="firstname" bundle="${localeAuth}" /></label><span style="color: red"> *</span>
                                            <input id="firstnamesignup" name="firstname" required="required" type="text" minlength="2" maxlength="10" pattern="[A-Za-zА-Яа-я]+" title="<fmt:message key="input.title.name" bundle="${localeAuth}" />" autofocus/>
                                        </p>
                                        <p>
                                            <label for="lastnamesignup"><fmt:message key="lastname" bundle="${localeAuth}" /></label><span style="color: red"> *</span>
                                            <input id="lastnamesignup" name="lastname" required="required" type="text" minlength="2" maxlength="10" pattern="[A-Za-zА-Яа-я]+" title="<fmt:message key="input.title.name" bundle="${localeAuth}" />"/>
                                        </p>
                                        <p>
                                            <label for="loginsignup"><fmt:message key="login" bundle="${localeAuth}" /></label><span style="color: red"> *</span>
                                            <input id="loginsignup" name="login" required="required" type="text" minlength="8" maxlength="12" pattern="[A-Za-z0-9]+" title="<fmt:message key="input.title.login" bundle="${localeAuth}" />"/>
                                        </p>
                                        <p>
                                            <label for="passwordsignup"><fmt:message key="password" bundle="${localeAuth}" /></label><span style="color: red"> *</span>
                                            <input id="passwordsignup" name="password" required="required" type="password" minlength="8" maxlength="20" pattern="[A-Za-z0-9]+" title="<fmt:message key="input.title.password" bundle="${localeAuth}" />"/>
                                        </p>
                                        <p>
                                            <label for="emailsignup"><fmt:message key="email" bundle="${localeAuth}" /></label><span style="color: red"> *</span>
                                            <input id="emailsignup" name="email" required="required" type="email" pattern="^[\w-\+]+(\.[\w]+)*@[\w-]+(\.[\w]+)*(\.[a-z]{2,})$" title="<fmt:message key="input.title.email" bundle="${localeAuth}" />"/>
                                        </p>
                                        <p>
                                            <label for="phonesignup"><fmt:message key="phone" bundle="${localeAuth}" /> </label><span style="color: red"> *</span>
                                            <input id="phonesignup" name="phone" required="required" type="text" pattern="\d{12}" title="<fmt:message key="input.title.phone" bundle="${localeAuth}" />"/>
                                        </p>
                                        <div class="alert alert-danger" style="display: none" id="signUpError">
                                            <strong><fmt:message key="error" bundle="${localeAuth}" /></strong> <fmt:message key="member.error" bundle="${localeAuth}" />
                                        </div>
                                        <div class="alert alert-success" style="display: none" id="signUpInfo">
                                            <fmt:message key="confirm" bundle="${localeAuth}" />
                                        </div>
                                        <input type="submit" class="btn btn-success" value="<fmt:message key="sign.up" bundle="${localeAuth}" />"/>
                                        <p class="change_link" style="width: 100%">
                                            <fmt:message key="account.yes" bundle="${localeAuth}" />
                                            <a href="#tologin" class="to_register"> <fmt:message key="account.yes" bundle="${localeAuth}" /> </a>
                                            <a href="#myModal" class="to_register" data-dismiss="modal"><fmt:message key="close" bundle="${localeAuth}" /></a>
                                        </p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/js/header.js"  type="text/javascript"></script>
</body>
</html>
