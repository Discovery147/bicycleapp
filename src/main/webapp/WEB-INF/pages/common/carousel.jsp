<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>
    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="opacity: 0.8">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item">
                <img src="img/main/rent_1.jpg" width = 100% alt="Los Angeles">
            </div>
            <div class="item active">
                <img src="img/main/rent_2.jpg" width = 100% alt="Los Angeles">
            </div>
            <div class="item">
                <img src="img/main/rent_3.jpg" width = 100% alt="Los Angeles">
            </div>
            <div class="item">
                <img src="img/main/rent_4.jpg" width = 100% alt="Los Angeles">
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</body>
</html>
