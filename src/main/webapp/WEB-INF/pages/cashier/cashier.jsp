<%@page import = "java.util.*" session="true"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.languageAttr.toString()}" scope="session" />
<fmt:setBundle basename="resource.localization.cashier.pagecontent" var="localeCashier" />

<html>
<head>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/icons/reservationPageIcon.ico" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cashier/cashier.css">
    <link href="https://fonts.googleapis.com/css?family=Play" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <title><fmt:message key="title" bundle="${localeCashier}" /></title>
</head>
<body>
    <c:import url="../common/header.jsp"/>
    <input type="hidden" id="locale" value="${sessionScope.languageAttr}" />
    <div id="background"></div>
        <div class="container" style="font-family: 'Play', sans-serif">
            <div class="row">
                <h2><fmt:message key="work.place" bundle="${localeCashier}" /></h2>
                <hr color="red" />
                <div class="panel panel-success">
                    <div class="panel-heading" onclick="showTransactionBlock()">
                        <fmt:message key="make.trip" bundle="${localeCashier}" />
                        <span class="glyphicon glyphicon-chevron-down" aria-hidden="false"></span>
                    </div>
                    <div class="panel-body" style="border-radius: 2px; display: none" id="transactionBlock">
                        <div class="container">
                            <form class="form-inline" action="InvokerServlet" method="post" onsubmit="createTransaction(); return false;">
                                <div class="btn-group">
                                    <select class="selectpicker show-tick form-control" data-live-search="true" data-style="btn-primary" id="bicycles" onchange="fillExpectedAmountNewTransaction()">
                                        <c:forEach items="${bicycles}" var="bicycle">
                                            <option value="${bicycle.bicycleId}">${bicycle.maker} ${bicycle.model} ${bicycle.color} ${bicycle.size}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="period"><fmt:message key="period" bundle="${localeCashier}" /></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><fmt:message key="hour" bundle="${localeCashier}" /></div>
                                        <input type="number" class="form-control" id="period" placeholder="Period" min="1" step="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="AmountNewTransaction"><fmt:message key="amount" bundle="${localeCashier}" /></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">BYN</div>
                                        <input type="number" class="form-control" id="AmountNewTransaction" placeholder="Amount" min="0" step="1">
                                        <div class="input-group-addon">.00</div>
                                    </div>
                                </div>
                                <p><fmt:message key="doc.type" bundle="${localeCashier}" /></p>
                                <select  class="selectpicker" data-style="btn-danger" id="doctypes">
                                    <c:forEach items="${doctypes}" var="doctype">
                                        <option value="${doctype.doctypeId}">${doctype.name}</option>
                                    </c:forEach>
                                </select>
                                <div class="form-group">
                                    <label class="sr-only"><fmt:message key="doc.number" bundle="${localeCashier}" /></label>
                                    <input type="text" class="form-control" placeholder="Number" id="docNumberNewTransaction">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><fmt:message key="doc.addition" bundle="${localeCashier}" /></label>
                                    <input type="text" class="form-control" placeholder="Other" id="docOtherNewTransaction">
                                </div>
                                <button type="submit" class="btn btn-default"><fmt:message key="add" bundle="${localeCashier}" /></button>
                            </form>
                            <div class="alert alert-success" style="display: none; margin-right: 2%" id="newTransactionSuccess">
                                <strong><fmt:message key="success" bundle="${localeCashier}" /></strong>
                                <fmt:message key="page.reload" bundle="${localeCashier}" />
                            </div>
                            <div class="alert alert-danger" style="display: none; margin-right: 2%" id="newTransactionError">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading"><fmt:message key="bicycle.busy" bundle="${localeCashier}" /></div>
                    <div class="panel-body" style="border-radius: 2px;">
                        <div class="table-responsive">
                            <table class="data_table table table-striped table-bordered">
                                <thead>
                                <tr class="default">
                                    <th style="font-weight:600" ><fmt:message key="bicycle" bundle="${localeCashier}" /></th>
                                    <th style="font-weight:600"><fmt:message key="fio" bundle="${localeCashier}" /></th>
                                    <th style="font-weight:600"><fmt:message key="document" bundle="${localeCashier}" /></th>
                                    <th style="font-weight:600"><fmt:message key="date.start" bundle="${localeCashier}" /></th>
                                    <th style="font-weight:600"><fmt:message key="date.end" bundle="${localeCashier}" /></th>
                                    <th style="font-weight:600"><fmt:message key="amount.expected" bundle="${localeCashier}" /> (BYN)</th>
                                    <th style="font-weight:600"><fmt:message key="operations" bundle="${localeCashier}" /></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${transactions}" var="transaction">
                                    <tr>
                                        <td>${transaction.bicycle.maker} ${transaction.bicycle.model} ${transaction.bicycle.color} ${transaction.bicycle.size}</td>
                                        <td>${transaction.member.firstname} ${transaction.member.lastname}</td>
                                        <td>${transaction.document.doctype.name} ${transaction.document.number} ${transaction.document.other}</td>
                                        <td>${transaction.startTime}</td>
                                        <td>${transaction.endTime}</td>
                                        <td>${transaction.expectedAmount}</td>
                                        <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#notFinishedTransactionModal"
                                                    onclick="fillModalWindowByNotFinishedTransaction('${transaction.transactionId}')"><fmt:message key="complete" bundle="${localeCashier}" /></button></td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Modal bicycle finish-->
                <div class="modal fade" id="notFinishedTransactionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form action="InvokerServlet" method="post">
                                    <input type="hidden" name="command" value="complete_transaction">
                                    <input type="hidden" name="transactionId" id="transactionId">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <div class="form-group">
                                                <label for="modalClient"><fmt:message key="member" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control" id="modalClient" disabled placeholder="Client">
                                            </div>
                                            <div class="form-group">
                                                <label for="modalDocument"><fmt:message key="document" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control" id="modalDocument" disabled placeholder="Document">
                                            </div>
                                            <div class="form-group">
                                                <label for="modalBicycle"><fmt:message key="bicycle" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control" id="modalBicycle" disabled placeholder="Bicycle">
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="modalStartTime"><fmt:message key="date.start" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control" id="modalStartTime" disabled placeholder="Start time">
                                            </div>
                                            <div class="form-group">
                                                <label for="modalEndTime"><fmt:message key="date.end" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control disabled" id="modalEndTime" disabled placeholder="End time">
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="form-row">
                                                        <label for="modalExpectedAmount"><fmt:message key="amount.expected" bundle="${localeCashier}" /></label>
                                                        <input type="number" class="form-control" id="modalExpectedAmount" placeholder="Expected amount" name="amount">
                                                    </div>
                                                    <div class="form-row">
                                                        <label for="modalAmount"><fmt:message key="amount.end" bundle="${localeCashier}" /></label>
                                                        <input type="number" step="0.01" min="0" class="form-control" id="modalAmount" name="addedAmount" placeholder="Amount">
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-primary" style="width: 100%" value="<fmt:message key="complete" bundle="${localeCashier}" />" />
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100%"><fmt:message key="close" bundle="${localeCashier}" /></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading"><fmt:message key="reservations" bundle="${localeCashier}" /></div>
                    <div class="panel-body" style="border-radius: 2px;">
                        <div class="table-responsive">
                            <table class="data_table table table-striped table-bordered">
                                <thead>
                                <tr class="default">
                                    <th style="font-weight:600" ><fmt:message key="bicycle" bundle="${localeCashier}" /></th>
                                    <th style="font-weight:600"><fmt:message key="fio" bundle="${localeCashier}" /></th>
                                    <th style="font-weight:600"><fmt:message key="date.start" bundle="${localeCashier}" /></th>
                                    <th style="font-weight:600"><fmt:message key="date.end" bundle="${localeCashier}" /></th>
                                    <th style="font-weight:600"><fmt:message key="amount.expected" bundle="${localeCashier}" /> (BYN)</th>
                                    <th style="font-weight:600"><fmt:message key="operations" bundle="${localeCashier}" /></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${reservations}" var="reservation">
                                    <tr>
                                        <td>${reservation.bicycle.maker} ${reservation.bicycle.model} ${reservation.bicycle.color} ${reservation.bicycle.size}</td>
                                        <td>${reservation.member.firstname} ${reservation.member.lastname}</td>
                                        <td>${reservation.startTime}</td>
                                        <td>${reservation.endTime}</td>
                                        <td>${reservation.amount}</td>
                                        <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#notFinishedTransactionModalByReservation"
                                                    onclick="fillModalWindowByNotFinishedReservation('${reservation.reservationId}')"><fmt:message key="trip.open" bundle="${localeCashier}" /></button>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Modal open transaction by reservation-->
                <div class="modal fade" id="notFinishedTransactionModalByReservation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form action="InvokerServlet" method="post" onsubmit="createTransactionByReservation(); return false;">
                                    <input type="hidden" name="command" value="create_transaction_by_reservation">
                                    <input type="hidden" name="memberId" id="ModalmemberIdByReservation">
                                    <input type="hidden" name="bicycleId" id="ModalbicycleIdByReservation">
                                    <input type="hidden" name="recervationId" id="ModalReservationId">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <div class="form-group">
                                                <label for="modalClient"><fmt:message key="member" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control" id="modalClientByReservation" disabled placeholder="Client">
                                            </div>
                                            <label for="documentByReservation"><fmt:message key="document" bundle="${localeCashier}" /></label>
                                            <div class="form-group">
                                                <select  class="selectpicker" data-style="btn-danger" id="documentByReservation">
                                                    <c:forEach items="${doctypes}" var="doctype">
                                                        <option value="${doctype.doctypeId}">${doctype.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="sr-only"><fmt:message key="doc.number" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control"id="docNumber" placeholder="Number">
                                            </div>
                                            <div class="form-group">
                                                <label class="sr-only"><fmt:message key="doc.addition" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control" id="docOther" placeholder="Other">
                                            </div>
                                            <div class="form-group">
                                                <label for="modalBicycle"><fmt:message key="bicycle" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control" id="modalBicycleByReservation" disabled placeholder="Bicycle">
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="modalStartTime"><fmt:message key="date.start" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control" id="modalStartTimeByReservation" disabled placeholder="Start time">
                                            </div>
                                            <div class="form-group">
                                                <label for="modalEndTime"><fmt:message key="date.end" bundle="${localeCashier}" /></label>
                                                <input type="text" class="form-control disabled" id="modalEndTimeByReservation" disabled placeholder="End time">
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <div class="form-row">
                                                        <label for="modalExpectedAmount"><fmt:message key="amount.expected" bundle="${localeCashier}" /></label>
                                                        <input type="number" class="form-control" id="modalExpectedAmountByReservation" disabled placeholder="Expected amount">
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-primary" style="width: 100%" value="Завершить" />
                                            <div class="alert alert-success" style="display: none; margin-right: 2%" id="newTransactionByReservationSuccess">
                                                <strong><fmt:message key="success" bundle="${localeCashier}" /></strong>
                                                <fmt:message key="page.reload" bundle="${localeCashier}" />
                                            </div>
                                            <div class="alert alert-danger" style="display: none" id="modalReservationError">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100%"><fmt:message key="close" bundle="${localeCashier}" /></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <footer>
        <c:import url="${pageContext.request.contextPath}/WEB-INF/pages/common/footer.jsp"/>
    </footer>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="http://momentjs.com/downloads/moment.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/cashier/cashier.js"></script>
</body>
</html>