<%@page import = "java.util.*" session="true"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.languageAttr.toString()}" scope="session" />
<fmt:setBundle basename="resource.localization.admin.bicycles.pagecontent" var="localeAdminBicycles" />

<html>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Play" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <title><fmt:message key="title" bundle="${localeAdminBicycles}" /></title>
</head>
<body style="font-family: 'Play', sans-serif">
<c:import url="${pageContext.request.contextPath}/WEB-INF/pages/common/header.jsp"/>
<div class="panel panel-info">
    <div class="panel-heading"><fmt:message key="bicycles" bundle="${localeAdminBicycles}" /></div>
    <div class="panel-body" style="border-radius: 2px;">
        <c:if test="${not empty exception}">
            <div class="alert alert-danger" style="margin-top: 1%">
                <fmt:message key="error.image" bundle="${localeAdminBicycles}" />
            </div>
        </c:if>
        <div class="panel panel-success">
            <div class="panel-heading" onclick="showCreateBicycleBlock()">
                <fmt:message key="bicycle.add" bundle="${localeAdminBicycles}" />
                <span class="glyphicon glyphicon-chevron-down" aria-hidden="false"></span>
            </div>
            <div class="panel-body" style="border-radius: 2px; display: none" id="createBicycleBlock">
                <div class="container">
                    <form role="form" method="post" class="form-group" enctype="multipart/form-data" action="http://localhost:8080/UploadServlet">
                        <input type="hidden" name="command" value="create_bicycle">
                        <div class="form-group">
                            <label for="ModalBicycleMaker"><fmt:message key="maker" bundle="${localeAdminBicycles}" /></label>
                            <input type="text" class="form-control" placeholder="Maker" name="maker" required>
                        </div>
                        <div class="form-group">
                            <label for="ModalBicycleModel"><fmt:message key="model" bundle="${localeAdminBicycles}" /></label>
                            <input type="text" class="form-control" placeholder="Model" name="model" required>
                        </div>
                        <div class="form-group">
                            <label for="ModalBicycleColor"><fmt:message key="color" bundle="${localeAdminBicycles}" /></label>
                            <input type="text" class="form-control" placeholder="Color" name="color" required>
                        </div>
                        <div class="form-group">
                            <label for="ModalBicycleSize"><fmt:message key="size" bundle="${localeAdminBicycles}" /></label>
                            <input type="text" class="form-control" placeholder="Size" name="size" required>
                        </div>
                        <div class="form-group">
                            <label for="ModalBicycleSerial"><fmt:message key="serial" bundle="${localeAdminBicycles}" /></label>
                            <input type="text" class="form-control" placeholder="Serial number" name="serial" required>
                        </div>
                        <div class="form-group">
                            <label for="ModalBicyclePrice"><fmt:message key="price" bundle="${localeAdminBicycles}" /></label>
                            <input type="number" min="0" step="0.01" class="form-control" placeholder="Price for 1 hour" name="price" required>
                        </div>
                        <label><fmt:message key="address" bundle="${localeAdminBicycles}" /></label>
                        <br>
                        <div class="btn-group">
                            <select class="selectpicker show-tick form-control" data-live-search="true" name="place" required>
                                <c:forEach items="${places}" var="place">
                                    <option value="${place.placeId}">${place.address}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><fmt:message key="image" bundle="${localeAdminBicycles}" /></label>
                            <br>
                            <label class="btn btn-block btn-primary">
                                <fmt:message key="choose" bundle="${localeAdminBicycles}" />  <input type="file" class="form-control" name="fileName" />
                            </label>
                        </div>
                        <button type="submit" class="btn btn-success"><fmt:message key="add" bundle="${localeAdminBicycles}" /></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="data_table table table-striped table-bordered">
                <thead>
                    <tr class="default">
                        <th style="font-weight:600" ><fmt:message key="bicycle" bundle="${localeAdminBicycles}" /></th>
                        <th style="font-weight:600"><fmt:message key="serial" bundle="${localeAdminBicycles}" /></th>
                        <th style="font-weight:600"><fmt:message key="address" bundle="${localeAdminBicycles}" /></th>
                        <th style="font-weight:600"><fmt:message key="operations" bundle="${localeAdminBicycles}" /></th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${bicycles}" var="bicycle">
                    <tr>
                        <td>${bicycle.maker} ${bicycle.model} ${bicycle.color} ${bicycle.size}</td>
                        <td>${bicycle.serialNumber}</td>
                        <td>${bicycle.place.address}</td>
                       <td>
                           <div align="right">
                           <button type="button" style="width:50%" class="btn btn-warning" data-toggle="modal" data-target="#editBicycle"
                                    onclick="fillModalWindowByBicycle('${bicycle.bicycleId}')"><fmt:message key="change" bundle="${localeAdminBicycles}" /></button>
                           <button type="button" class="btn btn-danger" onclick="deleteBicycleById('${bicycle.bicycleId}')"><fmt:message key="delete" bundle="${localeAdminBicycles}" /></button>
                           </div>
                       </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal edit bicycle-->
<div class="modal fade" id="editBicycle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form role="form" method="post" enctype="multipart/form-data" action="http://localhost:8080/UploadServlet">
                    <input type="hidden" name="command" value="edit_bicycle_by_admin">
                    <input type="hidden" name="bicycleId" id="modalBicycleId">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="form-group">
                                <div style="text-align: center;">
                                    <label><fmt:message key="image" bundle="${localeAdminBicycles}" /></label>
                                    <br>
                                    <img src="" id="ModalBicycleImage" alt="Bicycle Image" style="width: 30%; border-radius: 10%"/>
                                    <h6><fmt:message key="add.image.other" bundle="${localeAdminBicycles}" /></h6>
                                </div>
                                <label class="btn btn-block btn-primary">
                                    <fmt:message key="choose" bundle="${localeAdminBicycles}" />  <input type="file" class="form-control" name="fileName" style="display: none" />
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="ModalBicycleMaker"><fmt:message key="maker" bundle="${localeAdminBicycles}" /></label>
                                <input type="text" class="form-control" id="ModalBicycleMaker" name="maker" placeholder="Maker">
                            </div>
                            <div class="form-group">
                                <label for="ModalBicycleModel"><fmt:message key="model" bundle="${localeAdminBicycles}" /></label>
                                <input type="text" class="form-control" id="ModalBicycleModel" name="model" placeholder="Model">
                            </div>
                            <div class="form-group">
                                <label for="ModalBicycleSize"><fmt:message key="size" bundle="${localeAdminBicycles}" /></label>
                                <input type="text" class="form-control" id="ModalBicycleSize" name="size" placeholder="Size">
                                <label for="ModalBicycleColor"><fmt:message key="color" bundle="${localeAdminBicycles}" /></label>
                                <input type="text" class="form-control" id="ModalBicycleColor" name="color" placeholder="Color">
                            </div>
                            <div class="form-group">
                                <label for="ModalBicycleSerial"><fmt:message key="serial" bundle="${localeAdminBicycles}" /></label>
                                <input type="text" class="form-control" id="ModalBicycleSerial" name="serial" placeholder="Serial">
                            </div>
                            <label for="ModalPlaces"><fmt:message key="address" bundle="${localeAdminBicycles}" /></label>
                            <div class="form-group">
                                <select class="selectpicker show-tick form-control" data-live-search="true" data-style="btn-primary" id="ModalPlaces" name="place">
                                    <c:forEach items="${places}" var="place">
                                        <option value="${place.placeId}">${place.address}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="ModalBicyclePrice"><fmt:message key="price" bundle="${localeAdminBicycles}" /></label>
                                <input type="number" class="form-control" id="ModalBicyclePrice" min=0 step="0.01" name="price" placeholder="Price">
                            </div>
                            <input type="submit" class="btn btn-success" style="width: 100%" value="<fmt:message key="change" bundle="${localeAdminBicycles}" />" />
                            </div>
                        </div>
                    </form>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100%; margin-bottom: 2%">
                    <fmt:message key="close" bundle="${localeAdminBicycles}" />
                </button>
            </div>
        </div>
    </div>

<footer>
    <c:import url="${pageContext.request.contextPath}/WEB-INF/pages/common/footer.jsp"/>
</footer>
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="http://momentjs.com/downloads/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/js/admin/bicycles.js"></script>
</body>


</html>