<%@page import = "java.util.*" session="true"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.languageAttr.toString()}" scope="session" />
<fmt:setBundle basename="resource.localization.admin.places.pagecontent" var="localeAdminPlaces" />

<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Play" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <title><fmt:message key="title" bundle="${localeAdminPlaces}" /></title>
</head>
<body style="font-family: 'Play', sans-serif">
<c:import url="${pageContext.request.contextPath}/WEB-INF/pages/common/header.jsp"/>
<div class="panel panel-info">
    <div class="panel-heading"><fmt:message key="places" bundle="${localeAdminPlaces}" /></div>
    <div class="panel-body" style="border-radius: 2px;">
        <c:if test="${not empty exception}">
            <div class="alert alert-danger" style="margin-top: 1%">
                <fmt:message key="error.image" bundle="${localeAdminPlaces}" />
            </div>
        </c:if>
        <div class="panel panel-success">
            <div class="panel-heading" onclick="showCreatePlaceBlock()">
                <fmt:message key="place.add" bundle="${localeAdminPlaces}" />
                <span class="glyphicon glyphicon-chevron-down" aria-hidden="false"></span>
            </div>
            <div class="panel-body" style="border-radius: 2px; display: none" id="createPlaceBlock">
                <div class="container">
                    <form role="form" method="post" class="form-group" enctype="multipart/form-data" action="http://localhost:8080/UploadServlet">
                        <input type="hidden" name="command" value="create_place">
                        <div class="form-group">
                            <label><fmt:message key="name" bundle="${localeAdminPlaces}" /></label>
                            <input type="text" class="form-control" placeholder="Name" name="name" required>
                        </div>
                        <div class="form-group">
                            <label><fmt:message key="address" bundle="${localeAdminPlaces}" /></label>
                            <input type="text" class="form-control" placeholder="Address" name="address" required>
                        </div>
                        <label><fmt:message key="cashier" bundle="${localeAdminPlaces}" /></label>
                        <br>
                        <div class="btn-group">
                            <select class="selectpicker show-tick form-control" data-live-search="true" name="member" required>
                                <c:forEach items="${members}" var="member">
                                    <option value="${member.memberId}">${member.firstname} ${member.lastname} (${member.login})</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><fmt:message key="image" bundle="${localeAdminPlaces}" /></label>
                            <br>
                            <label class="btn btn-block btn-primary">
                                <fmt:message key="choose" bundle="${localeAdminPlaces}" />  <input type="file" class="form-control" name="fileName" />
                            </label>
                        </div>
                        <button type="submit" class="btn btn-success"><fmt:message key="add" bundle="${localeAdminPlaces}" /></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="data_table table table-striped table-bordered">
                <thead>
                <tr class="default">
                    <th style="font-weight:600" ><fmt:message key="address" bundle="${localeAdminPlaces}" /></th>
                    <th style="font-weight:600"><fmt:message key="name" bundle="${localeAdminPlaces}" /></th>
                    <th style="font-weight:600"><fmt:message key="cashier" bundle="${localeAdminPlaces}" /></th>
                    <th style="font-weight:600"><fmt:message key="operations" bundle="${localeAdminPlaces}" /></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${places}" var="place">
                    <tr>
                        <td>${place.address}</td>
                        <td>${place.name}</td>
                        <td>${place.member.firstname} ${place.member.lastname} (${place.member.login})</td>
                        <td>
                            <div align="right">
                                <button type="button" style="width: 100%" class="btn btn-warning" data-toggle="modal" data-target="#editPlace"
                                        onclick="fillModalWindowByPlace('${place.placeId}')"><fmt:message key="change" bundle="${localeAdminPlaces}" /></button>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal edit place-->
<div class="modal fade" id="editPlace" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form role="form" method="post" enctype="multipart/form-data" action="http://localhost:8080/UploadServlet">
                    <input type="hidden" name="command" value="edit_place_by_admin">
                    <input type="hidden" name="placeId" id="modalPlaceId">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="form-group">
                                <div style="text-align: center;">
                                    <label><fmt:message key="image" bundle="${localeAdminPlaces}" /></label>
                                    <br>
                                    <img src="" id="ModalPlaceImage" alt="Place image" style="width: 30%; border-radius: 10%"/>
                                    <h6><fmt:message key="add.image.other" bundle="${localeAdminPlaces}" /></h6>
                                </div>
                                <label class="btn btn-block btn-primary">
                                    <fmt:message key="choose" bundle="${localeAdminPlaces}" />  <input type="file" class="form-control" name="fileName" style="display: none" />
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="ModalPlaceName"><fmt:message key="name" bundle="${localeAdminPlaces}" /></label>
                                <input type="text" class="form-control" id="ModalPlaceName" name="name" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <label for="ModalPlaceAddress"><fmt:message key="address" bundle="${localeAdminPlaces}" /></label>
                                <input type="text" class="form-control" id="ModalPlaceAddress" name="address" placeholder="Name" required>
                            </div>
                            <label for="ModalMembers"><fmt:message key="cashier" bundle="${localeAdminPlaces}" /></label>
                            <div class="form-group">
                                <select class="selectpicker show-tick form-control" data-live-search="true" data-style="btn-primary" id="ModalMembers" name="member">
                                    <c:forEach items="${members}" var="member">
                                        <option value="${member.memberId}">${member.firstname} ${member.lastname} (${member.login})</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <input type="submit" class="btn btn-success" style="width: 100%" value="<fmt:message key="change" bundle="${localeAdminPlaces}" />" />
                        </div>
                    </div>
                </form>
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100%; margin-bottom: 2%"><fmt:message key="close" bundle="${localeAdminPlaces}" /></button>
        </div>
    </div>
</div>

<footer>
    <c:import url="${pageContext.request.contextPath}/WEB-INF/pages/common/footer.jsp"/>
</footer>
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="http://momentjs.com/downloads/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/js/admin/places.js"></script>
</body>


</html>