<%@page import = "java.util.*" session="true"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix = "ctg" uri = "customtags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.languageAttr.toString()}" scope="session" />
<fmt:setBundle basename="resource.localization.admin.members.pagecontent" var="localeAdminMembers" />

<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Play" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <title><fmt:message key="title" bundle="${localeAdminMembers}" /></title>
</head>
<body style="font-family: 'Play', sans-serif">
<c:import url="${pageContext.request.contextPath}/WEB-INF/pages/common/header.jsp"/>
<div class="panel panel-info">
    <div class="panel-heading"><fmt:message key="members" bundle="${localeAdminMembers}" /></div>
    <div class="panel-body" style="border-radius: 2px;">
        <div class="table-responsive">
            <table class="data_table table table-striped table-bordered">
                <thead>
                <tr class="default">
                    <th style="font-weight:600" ><fmt:message key="login" bundle="${localeAdminMembers}" /></th>
                    <th style="font-weight:600"><fmt:message key="fio" bundle="${localeAdminMembers}" /></th>
                    <th style="font-weight:600"><fmt:message key="phone" bundle="${localeAdminMembers}" /></th>
                    <th style="font-weight:600"><fmt:message key="email" bundle="${localeAdminMembers}" /></th>
                    <th style="font-weight:600"><fmt:message key="role" bundle="${localeAdminMembers}" /></th>
                    <th style="font-weight:600"><fmt:message key="blocked" bundle="${localeAdminMembers}" /></th>
                    <th style="font-weight:600"><fmt:message key="operations" bundle="${localeAdminMembers}" /></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${members}" var="member">
                    <tr>
                        <td>${member.login}</td>
                        <td>${member.firstname} ${member.lastname}</td>
                        <td>${member.phone}</td>
                        <td>${member.email}</td>
                        <td>${member.role.name}</td>
                        <td>
                            <ctg:statusBlocked blocked="${member.blocked}" language="${sessionScope.languageAttr}"/>
                        </td>
                        <td>
                            <div align="right">
                                <button type="button" style="width:100%" class="btn btn-warning" data-toggle="modal" data-target="#editMember"
                                        onclick="fillModalWindowByMember('${member.memberId}')"><fmt:message key="view" bundle="${localeAdminMembers}" /></button>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal edit member-->
<div class="modal fade" id="editMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form role="form" method="post" action="InvokerServlet">
                    <input type="hidden" name="command" value="edit_member_by_admin">
                    <input type="hidden" name="memberId" id="modalMemberId">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="form-group">
                                <div style="text-align: center;">
                                    <label><fmt:message key="image" bundle="${localeAdminMembers}" /></label>
                                    <br>
                                    <img src="" id="ModalMemberImage" alt="Member image" style="width: 30%; border-radius: 10%"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ModalMemberFio"><fmt:message key="fio" bundle="${localeAdminMembers}" /></label>
                                <input type="text" class="form-control" id="ModalMemberFio" placeholder="Fio" disabled>
                            </div>
                            <div class="form-group">
                                <label for="ModalMemberLogin"><fmt:message key="login" bundle="${localeAdminMembers}" /></label>
                                <input type="text" class="form-control" id="ModalMemberLogin" placeholder="Login" disabled>
                            </div>
                            <div class="form-group">
                                <label for="ModalMemberPhone"><fmt:message key="phone" bundle="${localeAdminMembers}" /></label>
                                <input type="text" class="form-control" id="ModalMemberPhone" placeholder="Phone" disabled>
                            </div>
                            <div class="form-group">
                                <label for="ModalMemberEmail"><fmt:message key="email" bundle="${localeAdminMembers}" /></label>
                                <input type="text" class="form-control" id="ModalMemberEmail" placeholder="Email" disabled>
                            </div>
                            <div class="form-group">
                                <label for="ModalMemberDiscount"><fmt:message key="discount" bundle="${localeAdminMembers}" /> (%)</label>
                                <input type="number" min="0" max="100" step="1" class="form-control" id="ModalMemberDiscount" name="discount" placeholder="Discount" required>
                            </div>
                            <label for="ModalRoles"><fmt:message key="role" bundle="${localeAdminMembers}" /></label>
                            <div class="form-group">
                                <select class="selectpicker show-tick form-control" data-live-search="true" data-style="btn-primary" id="ModalRoles" name="role">
                                    <c:forEach items="${roles}" var="role">
                                            <option value="${role.roleId}" ${role.roleId == 2 ? 'disabled' : 'enabled'}>${role.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <label for="ModalRoles"><fmt:message key="blocked" bundle="${localeAdminMembers}" /></label>
                            <div class="form-group">
                                <select class="selectpicker show-tick form-control" data-style="btn-danger" id="ModalBlocked" name="blocked">
                                    <option value="0"><fmt:message key="no" bundle="${localeAdminMembers}" /></option>
                                    <option value="1"><fmt:message key="yes" bundle="${localeAdminMembers}" /></option>
                                </select>
                            </div>
                            <input type="submit" class="btn btn-success" style="width: 100%" value="<fmt:message key="change" bundle="${localeAdminMembers}" />" />
                        </div>
                    </div>
                </form>
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 100%; margin-bottom: 2%"><fmt:message key="close" bundle="${localeAdminMembers}" /></button>
        </div>
    </div>
</div>
<footer>
    <c:import url="${pageContext.request.contextPath}/WEB-INF/pages/common/footer.jsp"/>
</footer>
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="http://momentjs.com/downloads/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/js/admin/members.js"></script>
</body>


</html>