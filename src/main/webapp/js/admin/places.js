$(document).ready(function() {
    $('#data_table').DataTable();
    $(".data_table").DataTable();
});

function fillModalWindowByPlace(id){
    var method = "GET";
    var action = "InvokerServlet";
    var command = "find_place_by_id";
    var data = "command="+ command +"&id=" + id;
    $.ajax({
        type: method,
        url:  action,
        data: data,
        success: function (data) {
            var place = JSON.parse(data);
            $( "#modalPlaceId" ).val(place.placeId);
            $( "#ModalPlaceName" ).val(place.name);
            $('#ModalPlaceAddress').val(place.address);
            $('#ModalMembers').val(place.member.memberId).change();
            $("#ModalPlaceImage").attr("src", "data:image/jpeg;base64," + place.image);
        }
    });
}

function showCreatePlaceBlock() {
    if(!$('#createPlaceBlock').is(':visible'))
    {
        $( "#createPlaceBlock" ).show("slow");
    }else{
        $( "#createPlaceBlock" ).hide("slow");
    }
}