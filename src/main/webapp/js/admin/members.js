$(document).ready(function() {
    $('#data_table').DataTable();
    $(".data_table").DataTable();
});

function fillModalWindowByMember(id){
    var method = "GET";
    var action = "InvokerServlet";
    var command = "find_member_by_id";
    var data = "command="+ command +"&id=" + id;
    $.ajax({
        type: method,
        url:  action,
        data: data,
        success: function (data) {
            var member = JSON.parse(data);
            $( "#modalMemberId" ).val(member.memberId);
            $( "#ModalMemberFio" ).val(member.firstname + " " + member.lastname);
            $( "#ModalMemberLogin" ).val(member.login);
            $('#ModalMemberPhone').val(member.phone);
            $('#ModalMemberEmail').val(member.email);
            $('#ModalMemberDiscount').val(member.discount);
            $('#ModalRoles').val(member.role.roleId).change();
            $('#ModalBlocked').val(member.blocked ? "1" : "0").change();
            $("#ModalMemberImage").attr("src", "data:image/jpeg;base64," + member.image);
        }
    });
}