$(document).ready(function() {
    $('#data_table').DataTable();
    $(".data_table").DataTable();
});

function deleteBicycleById(id) {
    if (confirm('Вы действительно хотите удалить велосипед?')) {
        var method = "GET";
        var action = "InvokerServlet";
        var command = "delete_bicycle_by_id";
        var data = "command="+ command +"&id=" + id;
        $.ajax({
            type: method,
            url:  action,
            data: data,
            success: function (data) {
                alert("Велосипед удален!");
                window.location.href = "/InvokerServlet?command=fill_admin_bicycles";
            }
        });
    }
}

function fillModalWindowByBicycle(id){
    var method = "GET";
    var action = "InvokerServlet";
    var command = "find_bicycle_by_id";
    var data = "command="+ command +"&id=" + id;
    $.ajax({
        type: method,
        url:  action,
        data: data,
        success: function (data) {
            var bicycle = JSON.parse(data);
            $( "#ModalBicycleMaker" ).val(bicycle.maker);
            $( "#ModalBicycleModel" ).val(bicycle.model);
            $( "#ModalBicycleColor" ).val(bicycle.color);
            $( "#ModalBicycleSize" ).val(bicycle.size);
            $( "#ModalBicycleSerial" ).val(bicycle.serialNumber);
            $( "#ModalBicyclePrice" ).val(bicycle.price);
            $('#ModalPlaces').val(bicycle.place.placeId).change();
            $( "#modalBicycleId" ).val(bicycle.bicycleId);
            $("#ModalBicycleImage").attr("src", "data:image/jpeg;base64," + bicycle.image);
        }
    });
}

function showCreateBicycleBlock() {
    if(!$('#createBicycleBlock').is(':visible'))
    {
        $( "#createBicycleBlock" ).show("slow");
    }else{
        $( "#createBicycleBlock" ).hide("slow");
    }
}