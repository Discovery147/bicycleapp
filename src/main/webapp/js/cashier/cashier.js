var PATTERN;
const defaultPattern = {
    "en_US": "M/D/YYYY HH:mm:ss",
    "ru_RU": "DD.MM.YYYY HH:mm:ss",
    "by_BY": "D.M.YYYY HH:mm:ss"
} // Источник: https://gist.github.com/mlconnor/1887156

$(document).ready(function() {
    $('#data_table').DataTable();
    $(".data_table").DataTable();
    $('#AmountNewTransaction').val($('#bicycles :selected').val());
    PATTERN = $( "#locale" ).val();
    PATTERN = (PATTERN === "") ? defaultPattern["ru_RU"] : defaultPattern[PATTERN];
});

function fillExpectedAmountNewTransaction(){
    $('#AmountNewTransaction').val($('#bicycles :selected').val()*$('#period').val());
};

$('#period').on('input',function(e){
    var amount =  $('#period').val() *  $('#bicycles :selected').val();
    $('#AmountNewTransaction').val(amount.toFixed(2));
});

function showTransactionBlock() {
    if(!$('#transactionBlock').is(':visible'))
    {
        $( "#transactionBlock" ).show("slow");
    }else{
        $( "#transactionBlock" ).hide("slow");
    }
}

function checkRealAmount(expectedAmount, startTime, endTime) {
    var startDate = new Date(startTime);
    var endDate = new Date(endTime);
    var currentDate = new Date();
    if(currentDate.getTime() < endDate.getTime()){
        return 0;
    }
    var oldPeriod = endDate.getTime() - startDate.getTime();
    var realPeriod = currentDate.getTime() - startDate.getTime();
    var realAmount = realPeriod * expectedAmount / oldPeriod;
    return (realAmount-expectedAmount).toFixed(2);
}

function fillModalWindowByNotFinishedTransaction(id){
    var method = "GET";
    var action = "InvokerServlet";
    var command = "find_not_finished_transaction";
    var data = "command="+ command +"&id=" + id;
    $.ajax({
        type: method,
        url:  action,
        data: data,
        success: function (data) {
            var transaction = JSON.parse(data);
            var bicycle = transaction.bicycle.maker + " " + transaction.bicycle.model + " " + transaction.bicycle.color + " " + transaction.bicycle.size;
            var client = transaction.member.firstname + " " + transaction.member.lastname;
            var document = transaction.document.doctype.name + " №" + transaction.document.number;
            $( "#transactionId" ).val(transaction.transactionId);
            $( "#modalBicycle" ).val(bicycle);
            $( "#modalClient" ).val(client);
            $( "#modalDocument" ).val(document);
            $( "#modalStartTime" ).val(moment(new Date(transaction.startTime).getTime()).format(PATTERN));
            $( "#modalEndTime" ).val(moment(new Date(transaction.endTime).getTime()).format(PATTERN));
            $( "#modalExpectedAmount" ).val(transaction.expectedAmount);
            var realAmount = checkRealAmount(transaction.expectedAmount, transaction.startTime, transaction.endTime);
            $( "#modalAmount" ).val(realAmount);
        }
    });
}

function fillModalWindowByNotFinishedReservation(id){
    var method = "GET";
    var action = "InvokerServlet";
    var command = "find_reservation";
    var data = "command="+ command +"&id=" + id;
    $.ajax({
        type: method,
        url:  action,
        data: data,
        success: function (data) {
            var reservation = JSON.parse(data);
            var bicycle = reservation.bicycle.maker + " " + reservation.bicycle.model + " " + reservation.bicycle.color + " " + reservation.bicycle.size;
            var client = reservation.member.firstname + " " + reservation.member.lastname;
            $( "#ModalReservationId" ).val(reservation.reservationId);
            $( "#modalBicycleByReservation" ).val(bicycle);
            $( "#modalClientByReservation" ).val(client);
            var startTime = moment(new Date(reservation.startTime).getTime()).format(PATTERN);
            var endTime = moment(new Date(reservation.endTime).getTime()).format(PATTERN);
            $( "#modalStartTimeByReservation" ).val(startTime);
            $( "#modalEndTimeByReservation" ).val(endTime);
            $( "#modalExpectedAmountByReservation" ).val(reservation.amount);
            $( "#ModalmemberIdByReservation" ).val(reservation.member.memberId);
            $( "#ModalbicycleIdByReservation" ).val(reservation.bicycle.bicycleId);
        }
    });
}

function createTransactionByReservation() {
    var method = "POST";
    var action = "InvokerServlet";
    var command = "create_transaction_by_reservation";

    var docTypeId = $('#documentByReservation :selected').val(); // id docType
    var documentNumber =  $('#docNumber').val();
    var documentOther =  $('#docOther').val();

    var memberId = $('#ModalmemberIdByReservation').val();
    var bicycleId = $('#ModalbicycleIdByReservation').val();
    var reservationId = $('#ModalReservationId').val();

    var startTime = $('#modalStartTimeByReservation').val();

    var startTimeDate = new Date( $('#modalStartTimeByReservation').val());
    var endTimeDate = new Date( $('#modalEndTimeByReservation').val());

    var period = (endTimeDate.getTime() - startTimeDate.getTime())/1000/60/60;

    var expectedAmount = $('#modalExpectedAmountByReservation').val();

    var data = "command="+ command +"&memberId=" + memberId +"&bicycleId=" + bicycleId +"&startTime=" + startTime +"&period=" + period
        +"&expectedAmount=" + expectedAmount +"&docTypeId=" + docTypeId +"&documentNumber=" + documentNumber +"&documentOther=" + documentOther +"&reservationId=" + reservationId;
    $.ajax({
        type: method,
        url:  action,
        data: data,
        success: function (data) {
            if(data != "forward"){
                $("#modalReservationError").text(data);
                $("#modalReservationError").show("slow");
            }else{
                $("#newTransactionByReservationSuccess").show("slow");
                setTimeout(function() {
                    window.location.href = "/InvokerServlet?command=fill_cashier";
                }, 2000);
            }
        }
    });
}

function createTransaction() {
    var method = "POST";
    var action = "InvokerServlet";
    var command = "create_transaction";

    var bicycleId = $('#bicycles :selected').val(); // bicycleId
    var docTypeId = $('#doctypes :selected').val(); // doctypeId
    var documentNumber =  $('#docNumberNewTransaction').val(); //docNumber
    var documentOther =  $('#docOtherNewTransaction').val(); //docOther

    var startTime = moment(new Date().getTime()).format(PATTERN);
    var period = $('#period').val(); // period
    var expectedAmount = $('#AmountNewTransaction').val(); // expected_amount
    var data = "command="+ command +"&bicycleId=" + bicycleId + "&docTypeId=" + docTypeId + "&documentNumber=" + documentNumber +"&documentOther=" + documentOther
        +"&startTime=" + startTime +"&period=" + period +"&expectedAmount=" + expectedAmount;

    $.ajax({
        type: method,
        url:  action,
        data: data,
        success: function (data) {
            if(data != "forward"){
                $("#newTransactionError").text(data);
                $("#newTransactionError").show("slow");
            }else{
                $("#newTransactionSuccess").show("slow");
                setTimeout(function() {
                    window.location.href = "/InvokerServlet?command=fill_cashier";
                }, 2000);
            }
        }
    });
}