package test.com.sizonenko.bicycleapp.validator;

import com.sizonenko.bicycleapp.localization.Locale;
import com.sizonenko.bicycleapp.parser.TimeStampParser;
import com.sizonenko.bicycleapp.validator.PlaceWorkTimeValidator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.text.ParseException;

public class PlaceWorkTimeValidatorTest {

    private static PlaceWorkTimeValidator validator;

    @BeforeClass
    public void createValidator() {
        validator = new PlaceWorkTimeValidator();
    }

    @Test
    public void negativeValidateDateForOpenClosePlaceTest() throws ParseException {
        Timestamp timestamp = TimeStampParser.parseStringToDateByLocale("6/25/2018 8:00:00", Locale.en_US);
        boolean actual = validator.validateDateForOpenClosePlace(timestamp);
        assert !actual;
    }

    @Test
    public void validateDateForOpenClosePlaceTest() throws ParseException {
        Timestamp timestamp = TimeStampParser.parseStringToDateByLocale("6/25/2018 9:00:00", Locale.en_US);
        boolean actual = validator.validateDateForOpenClosePlace(timestamp);
        assert actual;
    }

    @AfterClass
    public void clearValidator() {
        validator = null;
    }
}
