package test.com.sizonenko.bicycleapp.validator;

import com.sizonenko.bicycleapp.entity.CreatorMember;
import com.sizonenko.bicycleapp.entity.Member;
import com.sizonenko.bicycleapp.validator.MemberValidator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MemberValidatorTest {

    private static Member member;
    private static MemberValidator validator;

    @BeforeClass
    public void createObjects() {
        member = new CreatorMember().create();
        validator = new MemberValidator();
    }

    @Test
    public void validateRegisterTest() {
        member.setFirstname("Ihar");
        member.setLastname("Blinoff");
        member.setLogin("Ihar42");
        member.setPassword("42blinov42");
        member.setPhone("375296664242");
        member.setEmail("blinov@gmail.com");
        boolean actual = validator.validateRegister(member);
        assert actual;
    }

    @Test
    public void negativeValidateRegisterTest() {
        member.setFirstname("Ihar42");
        member.setLastname("Blinoff");
        member.setLogin("Ihar42");
        member.setPassword("42blinov42");
        member.setPhone("+375296664242");
        member.setEmail("blinov@gmail,com");
        boolean actual = validator.validateRegister(member);
        assert !actual;
    }

    @AfterClass
    public void clearObject() {
        member = null;
        validator = null;
    }
}
