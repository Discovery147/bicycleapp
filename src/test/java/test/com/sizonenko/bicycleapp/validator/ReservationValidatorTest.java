package test.com.sizonenko.bicycleapp.validator;

import com.sizonenko.bicycleapp.entity.CreatorMember;
import com.sizonenko.bicycleapp.entity.Member;
import com.sizonenko.bicycleapp.localization.Locale;
import com.sizonenko.bicycleapp.validator.ReservationValidator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class ReservationValidatorTest {

    private static Member member;
    private static ReservationValidator validator;

    @BeforeClass
    public void createObjects() {
        member = new CreatorMember().create();
        validator = new ReservationValidator();
    }

    @Test
    public void negativeValidateReservationIsBlockedTest() {
        member.setBlocked(true);
        boolean actual = validator.validateReservation(null, null, null, null, member, null);
        assert !actual;
    }

    @Test
    public void negativeValidateReservationNoSessionTest() {
        boolean actual = validator.validateReservation(null, null, null, null, null, null);
        assert !actual;
    }

    @Test
    public void negativeValidateReservationNoMoneyTest() {
        member.setBlocked(false);
        member.setAmount(new BigDecimal(10));
        boolean actual = validator.validateReservation(null, null, null, "12", member, null);
        assert !actual;
    }

    @Test
    public void negativeValidateReservationIncorrectDatePatternTest() {
        member.setAmount(new BigDecimal(10));
        boolean actual = validator.validateReservation("1", "6/25/2018 13:00:00", null, "10", member, Locale.ru_RU);
        assert !actual;
    }

    @Test
    public void negativeValidateReservationIncorrectMinIntervalTest() {
        member.setAmount(new BigDecimal(10));
        boolean actual = validator.validateReservation("1", "6/25/2018 13:00:00", "6/25/2018 13:30:00", "10", member, Locale.en_US);
        assert !actual;
    }

    @Test
    public void validateReservationTest() {
        member.setAmount(new BigDecimal(10));
        boolean actual = validator.validateReservation("1", "6/25/2018 13:00:00", "6/25/2018 14:00:00", "10", member, Locale.en_US);
        assert actual;
    }

    @AfterClass
    public void clearObjects() {
        member = null;
        validator = null;
    }
}
