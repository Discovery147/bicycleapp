package test.com.sizonenko.bicycleapp.localization;

import com.sizonenko.bicycleapp.localization.DatePattern;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DatePatternTest {

    @Test
    public void getPattern() {
        String expected = "dd.MM.yyyy HH:mm:ss";
        String actual = DatePattern.getInstance().get(null);
        Assert.assertEquals(expected, actual);
    }
}
