package test.com.sizonenko.bicycleapp.parser;

import com.sizonenko.bicycleapp.entity.*;
import com.sizonenko.bicycleapp.parser.BicycleParser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Map;

public class BicycleParserTest {

    private static BicycleParser parser;

    @BeforeClass
    public void createParser() {
        parser = new BicycleParser();
    }

    @Test
    public void transformListToMapTest() {
        int expected = 1;
        ArrayList bicycles = new ArrayList();

        Bicycle bicycleFirst = new CreatorBicycle().create();
        bicycleFirst.setBicycleId(1);
        bicycleFirst.getPlace().setPlaceId(5);

        Bicycle bicycleSecond = new CreatorBicycle().create();
        bicycleSecond.setBicycleId(2);
        bicycleSecond.getPlace().setPlaceId(5);

        bicycles.add(bicycleFirst);
        bicycles.add(bicycleSecond);

        Map map = parser.transformListToMap(bicycles);
        int actual = map.size();
        Assert.assertEquals(expected, actual);
    }

    @AfterClass
    public void clearParser() {
        parser = null;
    }
}
