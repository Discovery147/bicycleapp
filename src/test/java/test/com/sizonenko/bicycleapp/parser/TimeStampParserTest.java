package test.com.sizonenko.bicycleapp.parser;

import com.sizonenko.bicycleapp.localization.Locale;
import com.sizonenko.bicycleapp.parser.TimeStampParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.text.ParseException;

public class TimeStampParserTest {

    private static final String DATE = "1/1/1993 8:00:00";

    @Test
    public void parseStringToDateByLocaleTest() throws ParseException {
        Timestamp timestamp = TimeStampParser.parseStringToDateByLocale(DATE, Locale.en_US);
        Assert.assertNotEquals(timestamp, null);
    }

    @Test(expectedExceptions = ParseException.class)
    public void negativeParseStringToDateByLocaleTest() throws ParseException {
        Timestamp timestamp = TimeStampParser.parseStringToDateByLocale(DATE, Locale.by_BY);
        Assert.assertEquals(timestamp, null);
    }
}
