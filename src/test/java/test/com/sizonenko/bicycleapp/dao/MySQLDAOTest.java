package test.com.sizonenko.bicycleapp.dao;

import com.sizonenko.bicycleapp.pool.ConnectionPool;
import com.sizonenko.bicycleapp.pool.DefaultConnectionConfig;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

public class MySQLDAOTest{

    private static ConnectionPool pool;
    private static final String URL = "jdbc:mysql://localhost:3306/test_bicycle_db";

    @BeforeClass
    public void init() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        DefaultConnectionConfig.setURL(URL);
        pool = ConnectionPool.getInstance();
    }

    @AfterClass
    public void destroy() throws SQLException {
        Connection connection;
        while (true) {
            connection = pool.getConnection();
            if (connection != null) {
                connection.close();
            } else {
                break;
            }
        }
        pool = null;
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
