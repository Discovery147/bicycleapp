package test.com.sizonenko.bicycleapp.dao.mysql;

import com.sizonenko.bicycleapp.dao.AbstractDAO;
import com.sizonenko.bicycleapp.dao.DAOFactory;
import com.sizonenko.bicycleapp.dao.Table;
import com.sizonenko.bicycleapp.dao.mysql.MySQLDAO;
import com.sizonenko.bicycleapp.entity.Bicycle;
import com.sizonenko.bicycleapp.entity.CreatorBicycle;
import com.sizonenko.bicycleapp.exception.DAOException;
import org.testng.annotations.Test;
import test.com.sizonenko.bicycleapp.dao.MySQLDAOTest;
import java.lang.Enum;

import java.math.BigDecimal;
import java.util.List;

public class MySQLBicycleDAOTest extends MySQLDAOTest implements MySQLDAO {

    private static final AbstractDAO dao = DAOFactory.getDAO(Table.BICYCLE);
    private static final String SERIAL_NUMBER = "123456789";

    @Test
    public void addBicycleTest() throws DAOException {
        Bicycle bicycle = new CreatorBicycle().create();
        bicycle.getPlace().setPlaceId(1);
        bicycle.setSerialNumber(SERIAL_NUMBER);
        bicycle.setMaker("Kross");
        bicycle.setModel("Level B4 2014");
        bicycle.setColor("green");
        bicycle.setSize("XL");
        bicycle.setPrice(new BigDecimal(5));
        assert dao.create(bicycle);
    }

    @Test
    public void checkAddTest() throws DAOException {
        List<Bicycle> bicycles = dao.findAll();
        boolean status = false;
        for(Bicycle bicycle: bicycles){
            if(bicycle.getSerialNumber().equals(SERIAL_NUMBER)){
                status = true;
            }
        }
        assert status;
    }

    @Test
    public void deleteBicycleTest() throws DAOException {
        List<Bicycle> bicycles = dao.findAll();
        Long id = null;
        for(Bicycle bicycle: bicycles){
            if(bicycle.getSerialNumber().equals(SERIAL_NUMBER)){
                id = bicycle.getBicycleId();
            }
        }
        assert dao.delete(id);
    }

    @Test
    public void checkDeleteTest() throws DAOException {
        List<Bicycle> bicycles = dao.findAll();
        boolean status = false;
        for(Bicycle bicycle: bicycles){
            if(bicycle.getSerialNumber().equals(SERIAL_NUMBER)){
                status = true;
            }
        }
        assert status;
    }
}
