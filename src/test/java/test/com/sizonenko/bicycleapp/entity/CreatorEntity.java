package test.com.sizonenko.bicycleapp.entity;

import com.sizonenko.bicycleapp.entity.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreatorEntity {

    @Test
    public void createBicycle() {
        Bicycle bicycle = new CreatorBicycle().create();
        Role role = bicycle.getPlace().getMember().getRole();
        Assert.assertNotEquals(role, null);
    }

    @Test
    public void createConfirm() {
        Confirm confirm = new CreatorConfirm().create();
        Assert.assertNotEquals(confirm, null);
    }

    @Test
    public void createDoctype() {
        Doctype doctype = new CreatorDoctype().create();
        Assert.assertNotEquals(doctype, null);
    }

    @Test
    public void createDocument() {
        Document document = new CreatorDocument().create();
        Doctype doctype = document.getDoctype();
        Assert.assertNotEquals(doctype, null);
    }

    @Test
    public void createMember() {
        Member member = new CreatorMember().create();
        Role role = member.getRole();
        Assert.assertNotEquals(role, null);
    }

    @Test
    public void createPlace() {
        Place place = new CreatorPlace().create();
        Role role = place.getMember().getRole();
        Assert.assertNotEquals(role, null);
    }

    @Test
    public void createReservation() {
        Reservation reservation = new CreatorReservation().create();
        Role role = reservation.getMember().getRole();
        Assert.assertNotEquals(role, null);
    }

    @Test
    public void createTransaction() {
        Transaction transaction = new CreatorTransaction().create();
        Doctype doctype = transaction.getDocument().getDoctype();
        Assert.assertNotEquals(doctype, null);
    }

    @Test
    public void createRole() {
        Role role = new CreatorRole().create();
        Assert.assertNotEquals(role, null);
    }
}
