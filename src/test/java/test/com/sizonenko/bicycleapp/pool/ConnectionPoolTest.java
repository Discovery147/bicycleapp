package test.com.sizonenko.bicycleapp.pool;

import com.sizonenko.bicycleapp.pool.ConnectionPool;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPoolTest {

    private static ConnectionPool pool;

    @BeforeClass
    public void initPool() {
        pool = ConnectionPool.getInstance();
    }

    @Test
    public void getConnectionTest() {
        String expected = "com.mysql.jdbc.JDBC4Connection";
        Connection connection = pool.getConnection();
        String actual = connection.getClass().getName();
        pool.returnConnection(connection);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getCurrentSizeTest() {
        int expected = 10;
        int actual = pool.getCurrentPoolSize();
        Assert.assertEquals(expected, actual);
    }

    @AfterClass
    public void clearPool() throws SQLException {
        Connection connection;
        while (true) {
            connection = pool.getConnection();
            if (connection != null) {
                connection.close();
            } else {
                break;
            }
        }
        pool = null;
    }
}
