package test.com.sizonenko.bicycleapp.api;

import com.sizonenko.bicycleapp.api.discount.Discount;
import com.sizonenko.bicycleapp.entity.CreatorMember;
import com.sizonenko.bicycleapp.entity.Member;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DiscountTest {

    private static Member member;

    @BeforeClass
    public void createMember() {
        member = new CreatorMember().create();
    }

    @Test
    public void checkDiscountWithIncrement() {
        byte expected = 15;
        byte discount = 10;
        member.setDiscount(discount);
        Discount.checkDiscount(member);
        byte actual = member.getDiscount();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void checkDiscountWithoutIncrement() {
        byte expected = 40;
        byte discount = 40;
        member.setDiscount(discount);
        Discount.checkDiscount(member);
        byte actual = member.getDiscount();
        Assert.assertEquals(expected, actual);
    }

    @AfterClass
    public void clearMember(){
        member = null;
    }
}
