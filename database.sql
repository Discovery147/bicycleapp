-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema bicycle_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bicycle_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bicycle_db` DEFAULT CHARACTER SET utf8 ;
USE `bicycle_db` ;

-- -----------------------------------------------------
-- Table `bicycle_db`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`role` (
  `role_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Номер роли',
  `name` VARCHAR(45) NOT NULL COMMENT 'Наименование роли\nпользователь, кассир, администратор и др.',
  PRIMARY KEY (`role_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COMMENT = 'Роли пользователя';


-- -----------------------------------------------------
-- Table `bicycle_db`.`member`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`member` (
  `member_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор пользователя',
  `role_id` INT(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Роль пользователя:\nКлиент, Кассир, Администратор, ...',
  `blocked` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Заблокирован ли пользователь',
  `first_name` VARCHAR(45) NOT NULL COMMENT 'Имя пользователя',
  `last_name` VARCHAR(45) NOT NULL COMMENT 'Фамилия пользователя',
  `phone` VARCHAR(45) NOT NULL COMMENT 'Телефон пользователя',
  `email` VARCHAR(45) NOT NULL COMMENT 'Почта пользователя',
  `login` VARCHAR(20) NOT NULL COMMENT 'Логин пользователя\nНе заменен на первичный ключ ввиду возможного большого размера\nУникальный.',
  `password` VARCHAR(45) NOT NULL COMMENT 'Пароль пользователя',
  `image` MEDIUMBLOB NULL DEFAULT NULL COMMENT 'Изображение пользователя.\nМожет отсутствовать.',
  `discount` TINYINT(2) UNSIGNED NOT NULL DEFAULT '10' COMMENT 'Скидка пользователя на бронирование велосипеда, по дефолту 10%',
  `amount` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT '20.00' COMMENT 'Кошелек пользователя, при бронировании 100% предоплата.',
  PRIMARY KEY (`member_id`),
  CONSTRAINT `fk_role_id_member`
    FOREIGN KEY (`role_id`)
    REFERENCES `bicycle_db`.`role` (`role_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 164
DEFAULT CHARACTER SET = utf8
COMMENT = 'Набор пользователей с различными ролями';

CREATE UNIQUE INDEX `login_UNIQUE` ON `bicycle_db`.`member` (`login` ASC);

CREATE UNIQUE INDEX `email_UNIQUE` ON `bicycle_db`.`member` (`email` ASC);

CREATE UNIQUE INDEX `phone_UNIQUE` ON `bicycle_db`.`member` (`phone` ASC);

CREATE INDEX `role_id_idx` ON `bicycle_db`.`member` (`role_id` ASC);

CREATE INDEX `login` ON `bicycle_db`.`member` (`login` ASC)  COMMENT 'При регистрации и авторизации постоянная сверка логина и пароля';

CREATE INDEX `password` ON `bicycle_db`.`member` (`password` ASC)  COMMENT 'При регистрации и авторизации постоянная сверка логина и пароля';


-- -----------------------------------------------------
-- Table `bicycle_db`.`pstatus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`pstatus` (
  `pstatus_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Номер статуса',
  `name` VARCHAR(45) NOT NULL COMMENT 'Наименование статуса: закрыт, открыт, ...',
  PRIMARY KEY (`pstatus_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COMMENT = 'Набор уникальных статусов пункта проката';


-- -----------------------------------------------------
-- Table `bicycle_db`.`place`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`place` (
  `place_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор пункта проката',
  `name` VARCHAR(45) NOT NULL COMMENT 'Название пункта проката',
  `address` VARCHAR(45) NOT NULL COMMENT 'Адрес пункта проката',
  `image` MEDIUMBLOB NULL DEFAULT NULL COMMENT 'Фото самого пункта (необязательно)',
  `member_id` INT(10) UNSIGNED NOT NULL COMMENT 'Ссылка на кассира\nСвязь 1:1 (он уникален)',
  `pstatus_id` INT(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Ссылка на статус.',
  PRIMARY KEY (`place_id`),
  CONSTRAINT `fk_member_id_place`
    FOREIGN KEY (`member_id`)
    REFERENCES `bicycle_db`.`member` (`member_id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pstatus_id_place`
    FOREIGN KEY (`pstatus_id`)
    REFERENCES `bicycle_db`.`pstatus` (`pstatus_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COMMENT = 'Пункт проката.\nЗа каждый пунктом закреплен кассир, он уникален, но его может и не быть (при открытии новой точки).\nСтатус точки: закрыт, открыт';

CREATE UNIQUE INDEX `member_id_UNIQUE` ON `bicycle_db`.`place` (`member_id` ASC);

CREATE INDEX `fk_pstatus_id_place_idx` ON `bicycle_db`.`place` (`pstatus_id` ASC);

CREATE INDEX `fk_member_id_place_idx` ON `bicycle_db`.`place` (`member_id` ASC);


-- -----------------------------------------------------
-- Table `bicycle_db`.`bstatus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`bstatus` (
  `bstatus_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Уникальный статус велосипеда',
  `name` VARCHAR(45) NOT NULL COMMENT 'Наименование статуса',
  PRIMARY KEY (`bstatus_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COMMENT = 'Набор уникальных статусов для велосипедов';


-- -----------------------------------------------------
-- Table `bicycle_db`.`bicycle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`bicycle` (
  `bicycle_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор велосипеда',
  `place_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Закрепление велосипеда за пунктом проката',
  `serial_number` VARCHAR(60) NOT NULL COMMENT 'Серийный номер велосипеда (для идентификации в случаях пропажи)\nНе входит в первичный ключ, потому что он длинный и используется редко.',
  `maker` VARCHAR(45) NOT NULL COMMENT 'Производитель',
  `model` VARCHAR(45) NOT NULL COMMENT 'Модель',
  `color` VARCHAR(45) NOT NULL COMMENT 'Цвет',
  `size` VARCHAR(45) NOT NULL COMMENT 'Размер (L,XL,XXL,...)',
  `image` MEDIUMBLOB NULL DEFAULT NULL COMMENT 'Одно изображение для каждого велосипеда, возможно расширение на несколько (доп таблица с изображениями)',
  `price` DECIMAL(10,0) UNSIGNED NOT NULL COMMENT 'Цена за час проката ',
  `bstatus` INT(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Статус 1: Свободен\nСтатус 2: Занят\nСтатус 3: В ремонте\nи т.д.',
  `deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Статус: удален ли велосипед. Фактическое удаление велосипеда не должно быть для сохранения всей информации о транзакциях.',
  PRIMARY KEY (`bicycle_id`),
  CONSTRAINT `fk _place_id_ bicycle`
    FOREIGN KEY (`place_id`)
    REFERENCES `bicycle_db`.`place` (`place_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bstatus_bicycle`
    FOREIGN KEY (`bstatus`)
    REFERENCES `bicycle_db`.`bstatus` (`bstatus_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 54
DEFAULT CHARACTER SET = utf8
COMMENT = 'Содержит велосипеды\nВелосипед может быть закреплен за пунктом проката\nСтатус: доступен\\не доступен\\ремонт и т.д.';

CREATE INDEX `place_id_idx` ON `bicycle_db`.`bicycle` (`place_id` ASC);

CREATE INDEX `fk_status_bicycle_idx` ON `bicycle_db`.`bicycle` (`bstatus` ASC);


-- -----------------------------------------------------
-- Table `bicycle_db`.`confirm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`confirm` (
  `login` VARCHAR(20) NOT NULL,
  `code` VARCHAR(120) NOT NULL,
  PRIMARY KEY (`login`),
  CONSTRAINT `fk_login_register_confirm`
    FOREIGN KEY (`login`)
    REFERENCES `bicycle_db`.`member` (`login`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bicycle_db`.`doctype`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`doctype` (
  `doctype_id` INT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `deleted` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`doctype_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bicycle_db`.`document`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`document` (
  `document_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(120) NOT NULL,
  `other` VARCHAR(120) NULL DEFAULT NULL,
  `doctype_id` INT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`document_id`),
  CONSTRAINT `fk_doctype_id_document`
    FOREIGN KEY (`doctype_id`)
    REFERENCES `bicycle_db`.`doctype` (`doctype_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8;

CREATE UNIQUE INDEX `number_UNIQUE` ON `bicycle_db`.`document` (`number` ASC);

CREATE INDEX `fk_doctype_id_document_idx` ON `bicycle_db`.`document` (`doctype_id` ASC);


-- -----------------------------------------------------
-- Table `bicycle_db`.`reservation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`reservation` (
  `request_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Номер брони',
  `member_id` INT(10) UNSIGNED NOT NULL COMMENT 'Тот, кто забронировал',
  `bicycle_id` INT(10) UNSIGNED NOT NULL COMMENT 'Номер велосипеда',
  `start_time` DATETIME NOT NULL COMMENT 'Начало поездки',
  `end_time` DATETIME NOT NULL COMMENT 'Окончание поездки',
  `amount` DECIMAL(10,2) UNSIGNED NOT NULL COMMENT 'Сумма за данный период (может отличаться от итоговой в transaction ввиду доп.времени)',
  PRIMARY KEY (`request_id`),
  CONSTRAINT `fk_bicycle_id_request`
    FOREIGN KEY (`bicycle_id`)
    REFERENCES `bicycle_db`.`bicycle` (`bicycle_id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_member_id_request`
    FOREIGN KEY (`member_id`)
    REFERENCES `bicycle_db`.`member` (`member_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = utf8
COMMENT = 'Бронирование велосипеда\nПри добавлении записи 100%-ная предоплата списывается со счета и бронь добавляется в календарь.';

CREATE INDEX `fk_member_id_request_idx` ON `bicycle_db`.`reservation` (`member_id` ASC);

CREATE INDEX `fk_bicycle_id_request_idx` ON `bicycle_db`.`reservation` (`bicycle_id` ASC);


-- -----------------------------------------------------
-- Table `bicycle_db`.`transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bicycle_db`.`transaction` (
  `transaction_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Номер транзакции',
  `member_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Ссылка на пользователя, который забронировал велосипед.\nЕго может не быть, если человек без брони взял велосипед в пункте проката.',
  `bicycle_id` INT(10) UNSIGNED NOT NULL COMMENT 'Ссылка на велосипед, который выдали в пункте проката',
  `start_time` DATETIME NOT NULL COMMENT 'Время выдачи велосипеда',
  `end_time` DATETIME NULL DEFAULT NULL COMMENT 'Время сдачи велосипеда, оно неизвестно, человек может прихать позже и доплатить.\nФиксируется на момент сдачи.',
  `period` FLOAT UNSIGNED NOT NULL COMMENT 'Записывается на момент выдачи велосипеда, чтобы сравнивать с моментом сдачи велосипеда.',
  `expected_amount` DECIMAL(10,2) NOT NULL COMMENT 'Ожидаемая сумма',
  `amount` DECIMAL(10,2) UNSIGNED NULL DEFAULT NULL COMMENT 'Конечная сумма',
  `document_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Документ, залог. Удаляется триггером после того, как поездка завершилась',
  PRIMARY KEY (`transaction_id`),
  CONSTRAINT `fk_bicycle_id_transaction`
    FOREIGN KEY (`bicycle_id`)
    REFERENCES `bicycle_db`.`bicycle` (`bicycle_id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_document_id_transaction`
    FOREIGN KEY (`document_id`)
    REFERENCES `bicycle_db`.`document` (`document_id`)
    ON DELETE SET NULL,
  CONSTRAINT `fk_member_id_transaction`
    FOREIGN KEY (`member_id`)
    REFERENCES `bicycle_db`.`member` (`member_id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8
COMMENT = 'Записи о выдаче велосипедов на месте пункта проката  и их приеме обратно';

CREATE INDEX `member_id_idx` ON `bicycle_db`.`transaction` (`member_id` ASC);

CREATE INDEX `fk _bicycle_id_ bicycle_idx` ON `bicycle_db`.`transaction` (`bicycle_id` ASC);

CREATE INDEX `fk_document_id_transaction_idx` ON `bicycle_db`.`transaction` (`document_id` ASC);

USE `bicycle_db`;

DELIMITER $$
USE `bicycle_db`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `bicycle_db`.`place_AFTER_INSERT`
AFTER INSERT ON `bicycle_db`.`place`
FOR EACH ROW
BEGIN
        UPDATE member SET role_id = 2 WHERE member_id = NEW.member_id;
END$$

USE `bicycle_db`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `bicycle_db`.`place_AFTER_UPDATE`
AFTER UPDATE ON `bicycle_db`.`place`
FOR EACH ROW
BEGIN
	UPDATE member SET role_id = 2 WHERE member_id = NEW.member_id;
END$$

USE `bicycle_db`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `bicycle_db`.`place_BEFORE_UPDATE`
BEFORE UPDATE ON `bicycle_db`.`place`
FOR EACH ROW
BEGIN
	UPDATE member SET role_id = 1 WHERE member_id = OLD.member_id;
END$$

USE `bicycle_db`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `bicycle_db`.`request_AFTER_INSERT`
AFTER INSERT ON `bicycle_db`.`reservation`
FOR EACH ROW
BEGIN
    UPDATE member SET amount = amount - NEW.amount WHERE member_id = NEW.member_id;
END$$

USE `bicycle_db`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `bicycle_db`.`transaction_AFTER_INSERT`
AFTER INSERT ON `bicycle_db`.`transaction`
FOR EACH ROW
BEGIN
	UPDATE bicycle SET bstatus = 2 WHERE bicycle_id = NEW.bicycle_id;    
END$$

USE `bicycle_db`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `bicycle_db`.`transaction_AFTER_UPDATE`
AFTER UPDATE ON `bicycle_db`.`transaction`
FOR EACH ROW
BEGIN
	IF NEW.end_time IS NOT NULL THEN
		UPDATE bicycle SET bstatus = 1 WHERE bicycle_id = NEW.bicycle_id;
    END IF;
    IF NEW.end_time IS NOT NULL THEN 
		DELETE FROM document WHERE document_id = NEW.document_id;
    END IF;
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
